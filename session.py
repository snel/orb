#!/usr/bin/env python2
# noinspection PyUnresolvedReferences
"""
Interactive Orb client session with optional GUI and console.

The session can perform a local simulation of the game that is kept in sync
with the game master over the network.  The console is an interactive Python
interpreter for sending API commands to the server.

This module also defines defines connection parameters for client scripts.
See `host`.

Example:
    First ensure the connection parameters are configured correctly for your
    team.  Then from the Orb project root directory, run the module like this:

    $ python session.py

    Then in the console, command the avatar like this:

    >>> ?  # Displays help.
    >>> api.fire('TorpedoGun')  # Fires a torpedo directly ahead.

    When finished, type `exit()` or press CTRL+D (in Linux) or CTRL+Z (in
    Windows).
"""

import logging
import os
from distutils.util import strtobool
from pprint import pprint

from tornado.ioloop import IOLoop

import orb.network.api
from orb.core.orb_console import GameConsole, OrbConsole
from orb.esper import Entity
from orb.network import json_encoding
from orb.network.api import Api
from orb.network.game_client import GameClient
from orb.util import log_config
from orb.util.threading_util import run_daemon

log = logging.getLogger(__file__)
log_config.config("logs/session.log")
orb.network.api.setup_logging()
banner = "Client session started."

# Team id is only used to select the correct host and certs below.
team_id = 1
# IP address or domain name of the game master.
host = 'https://localhost:200{}'.format(team_id)

cert_path = os.path.join(os.path.dirname(__file__), "certs")
creds = {
    "ca_certs": os.path.join(cert_path, 'server{}.crt'.format(team_id)),
    "client_cert": os.path.join(cert_path, 'client{}.crt'.format(team_id)),
    "client_key": os.path.join(cert_path, 'client{}.key'.format(team_id)),
}


def select_avatar(avatar=None):
    """Returns the selected avatar entity.

       Args:
            avatar (int): Avatar entity.  If `None`, the user must select an
                avatar in the console.
    """
    if not avatar:
        api = Api(avatar, host, creds, async=False)
        data = json_encoding.decode(api.hello().body)
        avatars = {
            'ships': sorted(data['ships']),
            'stations': sorted(data['stations']),
            'torpedoes': sorted(data['torpedoes']),
        }

        print("Avatars owned by your team:")
        pprint({str(k): v for k, v in avatars.items()})
        avatar = input("Select an avatar to control: ")

    return Entity(int(avatar))


def interact(avatar, sim=True, display=True, fullscreen=False, console=True):
    """Starts an interactive Orb client session.

       Args:
            avatar (int): Assumes control of the selected entity.
            sim (bool): Runs a local version of the simulation that is
                synchronized with that on the server.
            display (bool): Opens a graphics window iff this is `True`.
                If `True`, `sim` must also be set to `True` to have effect.
            fullscreen (bool): Starts the graphics window in full-screen mode
                iff this is `True`.
            console (bool): Opens a game console iff this is `True`.
    """
    # NOTE: orb.avatar and orb.api in the console namespace may not stay the
    # same as the avatar from GameClient when switching control from the GUI.
    # There doesn't appear to be an easy way to do this while keeping the
    # `avatar` variable as a variable and not a function.
    orb = OrbConsole()
    orb.avatar = avatar
    orb.api = Api(avatar, host, creds, async=False)

    if not sim:
        if console:
            orb.open_console(banner, True)
        return orb

    game = GameClient(avatar=avatar, host=host, creds=creds,
                      headless=not display, fullscreen=fullscreen)
    game.entity_factory.create_solar_system()  # Avoids having to create these later.
    game.client_processor.update_now()

    # Adds more functions to the orb object when running with sim.
    orb.__class__ = GameConsole
    orb.__init__(game)

    orb.take_control(avatar)
    if console:
        orb.open_console(banner=banner, ns=orb.api.get_methods())
    game.run()  # Runs the game loop. Must be run from the main thread.


def add_connect_args(parser, fun):
    """Decorator for entry points. Adds args for host, port, team_id and
    avatar.  Also runs an IOLoop thread which is required by `AsyncApi`."""
    def connect():
        """
        Args:
        host (str): Hostname or IP address of the server to connect to.
        """
    parser.add_argument('--host', '-ch', type=str, f=connect)
    parser.add_argument('--avatar', '-ca', type=str, f=select_avatar)

    def f(host, avatar, **kwargs):
        if host:
            globals()['host'] = host

        run_daemon(name='IOLoop', target=IOLoop.current().start)
        avatar = select_avatar(avatar)
        fun(avatar=avatar, **kwargs)
    return f


if __name__ == "__main__":
    from orb.util.argparse_util import arg_parser
    parser = arg_parser(interact, description=__doc__)
    main = add_connect_args(parser, interact)
    parser.add_argument('--sim', '-s', type=strtobool, f=interact)
    parser.add_argument('--display', '-d', type=strtobool, f=interact)
    parser.add_argument('--fullscreen', '-f', type=strtobool, f=interact)
    parser.add_argument('--console', '-c', type=strtobool, f=interact)
    parser.invoke(main)
