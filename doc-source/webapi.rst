.. index : webapi

Web API
=======

The Orb web API is a pseudo-RESTful API based on HTTP GET requests.
Players connecting to the API execute requests on behalf of their assigned teams.
See: :mod:`orb.network.server`.

Example
-------
Assume the server is hosted at "http://orb".  If we want to request
a scan, we can enter the following URL in a browser:
"http://orb/scan".

If we want entity 42 to turn right, we can enter something like:
"http://orb/turn/42?direction=[1, 0, 0]".

Note the direction parameter. This should work as-is in most browsers
that automatically URL-encode addresses (incl. Chrome and Postman).
But to perform the request from cURL we must indicate this, e.g.

.. code-block:: bash

    curl -G -v "http://orb/turn/42" --data-urlencode "direction=[1, 0, 0]"

.. .. exec::
..    from orb.network.server import api_doc
..    print api_doc
