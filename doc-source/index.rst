.. EFS documentation master file, created by
   sphinx-quickstart on Mon Dec  5 06:07:58 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for Orb
=====================

.. only:: html

   .. include:: author.rst

.. toctree::
   
     intro
     pyapi
     webapi
     .. modules


.. only:: html
   
   .. include:: aux.rst

