.. intro

Introduction
============

Orb was specially written for MWR South Africa's 2017 HackFu challenge
and completed within 3 months.
Thank you to the organisers, especially Travis for his support and Tyrone
for allowing me to spend some time on this during regular office hours. ;)

Objectives
----------
The game takes place in a fictional star system with a number of factions
vying for control. Players are divided into teams (factions).
The objective is for your team to earn the highest score by colonising planets
and killing players from opposing teams within the allocated time (about 4 hours).
Your team will have lost if all your ships and stations are destroyed.

Entities
--------
Objects in the game world are each assigned an integer that uniquely identifies
that object.  For example, a ship might be entity ``42`` and a station might be
entity ``19``.  Entities have a set of *components* associated with them.
Components contain data about the entity, such as its position and velocity,
whether it is a ship, station or a planet.

Avatars
-------
Each team has control over a number of `avatars`.  An avatar is a controllable
game entity, such as a ship or station.  Everyone on a team has full control
over any avatar belonging to their team.  Multiple players can even control the
same avatar at the same time.

Controlling avatars
-------------------
There are 3 different ways to control avatars.

3D display
^^^^^^^^^^
The simplest is by opening a Python *session* and playing the game from a
3D display.  Press **F1** to print a list of controls to the console.
Pro tip: try adding your own key bindings!

Default controls in 3D display
""""""""""""""""""""""""""""""
 * **ESC**: quit
 * **arrow keys** *or* **hold right-mouse & drag**: look around
 * **mouse wheel**: zoom
 * **left-mouse**: select target
 * **TAB**: cycle targets

.. figure:: images/screen.png

     Display layout

In the top-left corner of the screen is information about your avatar and in
the top-right corner of the screen is information about your the selected target.
Note that `r`, `theta` and `phi` are spherical coordinates to your target.

Objects have green circle icons to help spot them in the vastness of space.
The target is shown by a selection box.  The ship's heading is shown by the
crosshairs and the ship's velocity direction is indicated by the circle
with a dot in the middle.  In this case it is white, which means this is the
direction **opposite** to the direction of movement.  If we turned around
180 degrees, we would see a green circle with a dot showing us the direction
of movement.  Other icons similarly turn white when the object is behind us.


Python console
^^^^^^^^^^^^^^
Another important way of playing the game is by entering commands in the
console.  The console is an interactive
`IPython <http://ipython.readthedocs.io/en/5.x/interactive/tutorial.html>`_
interpreter.
The console provides greater control and flexibility than the 3D display.
For example, you can configure power distribution to systems on your ship,
transfer fuel between avatars or build new stations.  See: :doc:`pyapi`.

To run the game with 3D display and console, enter the following:
``$ python session.py``

.. note::
    The 3D display is optional.  Use
    ``$ python session.py -d 0``
    to run a console session without 3D display.

.. note::
    Bear in mind that each open session will use up network bandwidth and
    your team may get rate-limited.  The API supports up to 100 calls per team
    per second.

Python scripts
^^^^^^^^^^^^^^
It is also possible to run existing Python scripts.  See the `scripts` directory
for examples.

Web API
^^^^^^^
Another way of playing the game is via its :doc:`webapi` that
you can access through a regular web browser, Postman or even cURL.

.. note::
    There are known issues with displaying scan results with Postman.
    This happens because standard JSON does not support non-finite number 
    representations so Postman may fail to pretty-print responses.
    Python and cURL should still work fine, regardless.

Gameplay
--------
The game takes place in space.  As such, Newtonian physics apply such as
conservation of momentum apply--if a ship is moving it will continue
moving in the same direction indefinitely, unless a resultant force is applied.

Systems
-------
Avatars have electrical systems on board that enable them to do things.
All systems drain power while activated, so by running out of fuel you run the
risk of cutting off power and killing everyone on board.
The amount of power to each system can be changed at any time.  Systems
are disabled when supplied with insufficient power;  some systems can even
be boosted past their regular operating level.

See: :func:`~orb.network.api.Api.power`.

.. note::
    Leaving systems enabled will drain fuel quickly.
    Shields and warp drives are disabled by default.

Colonising planets
------------------
Both planets and moons can be colonised.  To colonise means building a
space station in orbit around the planet.  Therefore, each planet or moon can
be colonised by only one team at a time.

To colonise a new planet, two ships from the same team need to be within
150km of the planet's surface.  Then one of the ships elects to build a new
station by issuing a colonise request.  Ideally, this ship should already
be in stable orbit, because thrusters will be disabled until construction is
complete, and the finished station will be placed in orbit at the same velocity
as the ship.  See: :func:`~orb.network.api.Api.colonise`.

Ships vs stations
-----------------
Ships are highly mobile, while stations are almost immobile.

Stations provide fuel and score income.  See :class:`~orb.core.config.Colony`.
Stations have only weak thrusters to correct orbital trajectory, but won't be
able to fly far away from their planet.
Players should ensure that stations remain in a stable orbit
to prevent them crashing into the planet!

Ships can colonise new planets.  Linear thrusters are not effective for interplanetary
travel.  Ships also have warp drives to travel at faster-than-light
speeds (up to 300 times the speed of light).  See: :func:`~orb.network.api.Api.warp`.

.. note::
    When warping to distant planets, bear in mind that your entry velocity
    is conserved when leaving warp!  This means that you will have to adjust
    your velocity either before or after warping to enter a stable orbit
    at the destination.

Destroying avatars and respawning
---------------------------------
Avatars can be destroyed by weapons fire, crashing into things or failure
to provide power to life support systems.
Destroyed ships will respawn after a few minutes at the oldest surviving station,
with the same entity id as before.
Destroyed stations do not respawn, but must be manually rebuilt by colonising
the planet again.

