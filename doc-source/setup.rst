.. intro

Setup
=====

You should have been issued a live-boot flash disk.
It contains Fedora 25 and an Orb setup script.

# Reboot and in the boot-up options, select the flash stick to boot into it.
# Once Fedora boots, open a network connection.
# Open a terminal.
    # Enter `sudo su`.
    # Enter `orb-get`.  Setup should take a few minutes to complete.
    # Change directory to `orb-hackfu`.
    # Play the game by entering `python session.py`.
