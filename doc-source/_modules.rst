orb
====

.. toctree::
   :maxdepth: 4
   

.. autosummary::
   :toctree: _autosummary

    session
    master
    orb

    orb.components
    orb.components.attitude_thrusters
    orb.components.avatar
    orb.components.blocks_sensor
    orb.components.body
    orb.components.bubble
    orb.components.circuit
    orb.components.colonisable
    orb.components.colony
    orb.components.component_mappers
    orb.components.components
    orb.components.construction
    orb.components.explosion
    orb.components.fuel_tank
    orb.components.generator
    orb.components.hitpoints
    orb.components.kinetic_orbiter
    orb.components.laser_gun
    orb.components.laser
    orb.components.life_support
    orb.components.linear_thrusters
    orb.components.observer
    orb.components.planet
    orb.components.projectile
    orb.components.respawn
    orb.components.self_destruct
    orb.components.sensor
    orb.components.shields
    orb.components.ship
    orb.components.star
    orb.components.station
    orb.components.team
    orb.components.torpedo_gun
    orb.components.torpedo
    orb.components.transform
    orb.components.warp_drive

    orb.components.subcomponents
    orb.components.subcomponents.appliance
    orb.components.subcomponents.capacitor
    orb.components.subcomponents.countdown
    orb.components.subcomponents.engine
    orb.components.subcomponents.ghost
    orb.components.subcomponents.projectile_weapon
    orb.components.subcomponents.relation
    orb.components.subcomponents.resistor

    orb.core
    orb.core.astro_processor
    orb.core.avatar_controller
    orb.core.bubble_processor
    orb.core.circuit_processor
    orb.core.config
    orb.core.construction_processor
    orb.core.countdown_processor
    orb.core.damage_processor
    orb.core.display_processor
    orb.core.entity_factory
    orb.core.game_master
    orb.core.game
    orb.core.gravity_processor
    orb.core.input_processor
    orb.core.mouse_control
    orb.core.orb_help
    orb.core.orb_console
    orb.core.sensor_processor
    orb.core.solar_system
    orb.core.team_processor
    orb.core.thruster_processor


    orb.network
    orb.network.api
    orb.network.client_processor
    orb.network.game_client
    orb.network.json_encoding
    orb.network.master_processor
    orb.network.server

    orb.esper
    orb.esper.component
    orb.esper.entity
    orb.esper.entity_world
    orb.esper.processor
    orb.util
    orb.util.argparse_util
    orb.util.banner
    orb.util.colours
    orb.util.decoration
    orb.util.dic
    orb.util.field_path
    orb.util.formatter
    orb.util.grid
    orb.util.log_config
    orb.util.math_util
    orb.util.roman
    orb.util.serializable
    orb.util.threading_util
    orb.util.type_casting
    orb.util.versioned

    scripts
    scripts.fire_at
    scripts.fire
    scripts.scan
    scripts.turn

