.. index : pyapi

Python API reference
====================

An `api` object is available in the console to issue commands to your avatar.
The api object's methods are imported to the global namespace by default,
so that entering ``fire('lasergun')`` is equivalent to ``api.fire('lasergun')``.

.. code-block:: python
    :caption: console use of `api` object

    api?       # Displays brief summary.
    api??      # Displays source code.
    help(api)  # Displays documentation.

    fire('lasergun')       # Fires a laser.
    turn(direction.port)   # Turns to port (left).

.. autoclass:: orb.network.api.Api
    :members:

