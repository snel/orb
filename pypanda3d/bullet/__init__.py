import typing

from pypanda3d.core import BoundingVolume

if not typing.TYPE_CHECKING:
    from panda3d.bullet import *
else:
    import pypanda3d.core as __core
    import panda3d.bullet as __bullet

    # noinspection PyMethodOverriding
    class BulletBodyNode(__bullet.BulletBodyNode):
        def set_kinematic(self, value):
            # type: (bool) -> ()
            pass

        def set_static(self, value):
            # type: (bool) -> ()
            pass

        def get_shape_bounds(self):
            # type: () -> __core.BoundingVolume
            pass

    # noinspection PyMethodOverriding
    class BulletRigidBodyNode(BulletBodyNode, __bullet.BulletRigidBodyNode):
        def set_linear_velocity(self, velocity):
            # type: (__core.LVecBase3) -> ()
            pass

        def get_linear_velocity(self):
            # type: () -> __core.LVecBase3
            pass

        def set_linear_velocity(self):
            # type: () -> __core.LVecBase3
            pass

        def get_angular_velocity(self):
            # type: () -> __core.LVecBase3
            pass

        def set_angular_velocity(self, value):
            # type: (__core.Vec3) -> ()
            pass

        def set_linear_damping(self, value):
            # type: (float) -> ()
            pass

        def set_angular_damping(self, value):
            # type: (float) -> ()
            pass

        def get_mass(self):
            # type: () -> float
            pass

        def apply_central_force(self, force):
            # type: (__core.LVecBase3) -> ()
            pass

        def apply_central_impulse(self, force):
            # type: (__core.LVecBase3) -> ()
            pass

        def apply_torque(self, torque):
            # type: (__core.LVecBase3) -> ()
            pass

        def is_kinematic(self):
            # type: () -> bool
            pass

        def get_bounds(self):
            # type: () -> BoundingVolume
            pass

        def get_total_force(self):
            # type: () -> __core.LVecBase3
            pass

        def get_total_torque(self):
            # type: () -> __core.LVecBase3
            pass

        def set_active(self, active=True, force=True):
            # type: (bool, bool) -> ()
            pass

        def clear_forces(self):
            # type: () -> ()
            pass

        def get_shape(self, i):
            # type: (int) -> BulletShape
            pass

    # noinspection PyMethodOverriding
    class BulletWorld(__bullet.BulletWorld):
        def remove(self, obj):
            # type: (__core.TypedObject) -> ()
            pass

        def attach(self, obj):
            # type: (__core.TypedObject) -> ()
            pass
