import typing

if not typing.TYPE_CHECKING:
    from panda3d.core import *
else:
    from typing import AnyStr as __AnyStr, Union, AnyStr, Tuple, List
    import panda3d.core as __core

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class VBase3F(__core.VBase3F):
        x = 0.
        y = 0.
        z = 0.

        def normalize(self):
            # type: () -> bool
            pass

        def normalized(self):
            # type: () -> VBase3F
            pass

        def dot(self, other):
            # type: (LVecBase3i) -> VBase3F
            pass

        def cross(self, other):
            # type: (LVecBase3i) -> VBase3F
            pass

        def length(self):
            # type: () -> float
            pass

        def length_squared(self):
            # type: () -> float
            pass

        def set(self, float_x, float_y, float_z):
            # type: (float, float, float) -> ()
            pass

        def normalize(self):
            # type: () -> ()
            pass

        def __add__(self, y):
            # type: (float) -> VBase3F
            """ x.__add__(y) <==> x+y """
            pass

        def __sub__(self, y):
            # type: (float) -> VBase3F
            """ x.__sub__(y) <==> x-y """
            pass

        def __mul__(self, y):
            # type: (float) -> VBase3F
            """ x.__mul__(y) <==> x*y """
            pass

        def __div__(self, y):
            # type: (float) -> VBase3F
            """ x.__div__(y) <==> x/y """
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class Vec3F(VBase3F, __core.Vec3F):
        @staticmethod
        def zero():
            # type: () -> Vec3F
            pass

        @staticmethod
        def up():
            # type: () -> Vec3F
            pass

        @staticmethod
        def down():
            # type: () -> Vec3F
            pass

        @staticmethod
        def forward():
            # type: () -> Vec3F
            pass

        @staticmethod
        def back():
            # type: () -> Vec3F
            pass

        @staticmethod
        def left():
            # type: () -> Vec3F
            pass

        @staticmethod
        def right():
            # type: () -> Vec3F
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class VBase3D(__core.VBase3D):
        x = 0.
        y = 0.
        z = 0.

        def normalize(self):
            # type: () -> bool
            pass

        def normalized(self):
            # type: () -> VBase3D
            pass

        def dot(self, other):
            # type: (LVecBase3i) -> VBase3D
            pass

        def cross(self, other):
            # type: (LVecBase3i) -> VBase3D
            pass

        def length(self):
            # type: () -> float
            pass

        def length_squared(self):
            # type: () -> float
            pass

        def __copy__(self):
            # type: () -> VBase3D
            pass

        def __add__(self, y):
            # type: (float) -> VBase3D
            """ x.__add__(y) <==> x+y """
            pass

        def __sub__(self, y):
            # type: (float) -> VBase3D
            """ x.__sub__(y) <==> x-y """
            pass

        def __mul__(self, y):
            # type: (float) -> VBase3D
            """ x.__mul__(y) <==> x*y """
            pass

        def __div__(self, y):
            # type: (float) -> VBase3D
            """ x.__div__(y) <==> x/y """
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class Vec3D(VBase3D, __core.Vec3D):
        x = 0.
        y = 0.
        z = 0.

        def normalize(self):
            # type: () -> bool
            pass

        def normalized(self):
            # type: () -> Vec3D
            pass

        @staticmethod
        def zero():
            # type: () -> Vec3D
            pass

        @staticmethod
        def up():
            # type: () -> Vec3D
            pass

        @staticmethod
        def down():
            # type: () -> Vec3D
            pass

        @staticmethod
        def forward():
            # type: () -> Vec3D
            pass

        @staticmethod
        def back():
            # type: () -> Vec3D
            pass

        @staticmethod
        def left():
            # type: () -> Vec3D
            pass

        @staticmethod
        def right():
            # type: () -> Vec3D
            pass

        def __copy__(self):
            # type: () -> Vec3D
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class VBase4F(__core.VBase4F):
        # x = 0.
        # y = 0.
        # z = 0.
        # w = 0.

        def length(self):
            # type: () -> VBase4F
            pass

        def length_squared(self):
            # type: () -> VBase4F
            pass

        def __add__(self, y):
            # type: (float) -> VBase4F
            """ x.__add__(y) <==> x+y """
            pass

        def __sub__(self, y):
            # type: (float) -> VBase4F
            """ x.__sub__(y) <==> x-y """
            pass

        def __mul__(self, y):
            # type: (float) -> VBase4F
            """ x.__mul__(y) <==> x*y """
            pass

        def __div__(self, y):
            # type: (float) -> VBase4F
            """ x.__div__(y) <==> x/y """
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class Vec4F(VBase4F, __core.Vec4F):
        pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class QuatF(__core.QuatF):
        @staticmethod
        def ident_quat(self):
            # type: () -> QuatF
            pass

        def set_from_axis_angle(self, angle_degrees, axis):
            # type: (float, LVector3f) -> ()
            pass

        def xform(self, v, dummy=None):
            # type: (LVecBase3) -> Vec3F
            pass

        def normalize(self):
            # type: () -> bool
            pass

        def __copy__(self):
            # type: () -> QuatF
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class QuatD(__core.QuatD):
        @staticmethod
        def ident_quat():
            # type: () -> QuatD
            pass

        def set_from_axis_angle(self, angle_degrees, axis):
            # type: (float, LVector3f) -> ()
            pass

        def xform(self, v, dummy=None):
            # type: (LVecBase3) -> Vec3D
            pass

        def normalize(self):
            # type: () -> bool
            pass

        def __copy__(self):
            # type: () -> QuatD
            pass

    # noinspection PyMethodOverriding,PyPep8Naming,PyMissingOrEmptyDocstring
    class GeomNode(__core.GeomNode):
        @staticmethod
        def getDefaultCollideMask():
            # type: () -> BitMask32
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class PandaNode(__core.PandaNode):
        def get_python_tag(self, key):
            # type: (__AnyStr) -> object
            pass

        def set_python_tag(self, key, value):
            # type: (__AnyStr, object) -> ()
            pass

        def copy_children(self, other):
            # type: (PandaNode) ->()
            pass

        def copy_all_properties(self, other):
            # type: (PandaNode) ->()
            pass

    # noinspection PyMethodOverriding,SpellCheckingInspection,PyMissingOrEmptyDocstring
    class NodePath(__core.NodePath):
        def node(self):
            # type: () -> PandaNode
            pass

        def attach_new_node(self, node):
            # type: (Union[AnyStr, PandaNode]) -> NodePath
            pass

        def reparent_to(self, np):
            # type: (NodePath) -> ()
            pass

        def get_pos(self):
            # type: () -> LVecBase3
            pass

        def get_hpr(self):
            # type: () -> LVecBase3
            pass

        def get_bounds(self):
            # type: () -> BoundingVolume
            pass

        def get_parent(self):
            # type: () -> NodePath
            pass

        def set_pos(self, x):
            # type: (LVecBase4) -> ()
            pass

        def set_quat(self, q):
            # type: (LQuaternionf) -> ()
            pass

        def set_pos_quat(self, x, q):
            # type: (LVecBase4, LQuaternionf) -> ()
            pass

        def set_fluid_pos(self, x):
            # type: (LVecBase4) -> ()
            pass

        def set_hpr(self, hpr):
            # type: (LVecBase3) -> ()
            pass

        def set_h(self, h):
            # type: (float) -> ()
            pass

        def set_name(self, name):
            # type: (__AnyStr) -> ()
            pass

        def get_name(self):
            # type: () -> str
            pass

        def get_relative_vector(self, other, v):
            # type: (NodePath, LVecBase3) -> LVecBase3
            pass

        def show(self):
            # type: () -> ()
            pass

        def hide(self):
            # type: () -> ()
            pass

        def get_python_tag(self, key):
            # type: (__AnyStr) -> object
            pass

        def set_python_tag(self, key, value):
            # type: (__AnyStr, object) -> ()
            pass

        def set_two_sided(self, bool_two_sided, int_priority):
            # type: (bool, int) -> ()
            pass

        def set_color(self, v):
            # type: (LVecBase4) -> ()
            pass

        def set_color_scale(self, v):
            # type: (LVecBase4) -> ()
            pass

        def set_light_off(self):
            # type: () -> ()
            pass

        def set_depth_write(self, value):
            # type: (bool) -> ()
            pass

        def set_compass(self):
            # type: () -> ()
            pass

        def set_bin(self, bin_name, draw_order, priority=0):
            # type: (str, int, int) -> ()
            pass

        def set_scale(self, value):
            # type: (object) -> ()
            """value can be either a vector or scalar.
            There are also other overrides available."""
            pass

        def set_material(self, material, priority=0):
            # type: (Material, int) -> ()
            pass

        def set_two_sided(self, two_sided, priority=0):
            # type: (bool, int) -> ()
            pass

        def get_child(self, i):
            # type: (int) -> NodePath
            pass

        def get_quat(self):
            # type: () -> QuatF
            pass

        def find(self, str):
            # type: (str) -> NodePath
            pass

        def node(self):
            # type: () -> PandaNode
            pass

        def find_all_materials(self):
            # type: () -> List[Material]
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class Material(__core.Material):
        def set_emission(self, value):
            # type: (Union[Tuple[float, float, float, float], LVecBase4]) -> ()
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class PointLight(__core.PointLight):
        def set_attenuation(self, attenuation):
            # type: (Union[Tuple[float, float, float], LVecBase3]) -> ()
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class ClockObject(__core.ClockObject):
        def get_long_time(self):
            # type: (int) -> ()
            pass

        @staticmethod
        def getGlobalClock():
            # type: () -> ClockObject
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class Lens(__core.Lens):
        def set_fov(self, fov):
            # type: (float) -> ()
            pass

        def get_fov(self):
            # type: () -> LVecBase3f
            pass

        def set_near_far(self, near, far):
            # type: (float, float) -> ()
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class LineSegs(__core.LineSegs):
        def create(self):
            # type: () -> ()
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class CollisionTraverser(__core.CollisionTraverser):
        def add_collider(self, collider, handler):
            # type: (NodePath, CollisionHandler) -> ()
            pass

        def traverse(self, root):
            # type: (NodePath) -> ()
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class CollisionHandlerQueue(__core.CollisionHandlerQueue):
        def get_num_entries(self):
            # type: () -> int
            pass

        def sort_entries(self):
            # type: () -> ()
            pass

    # noinspection PyMethodOverriding,PyMissingOrEmptyDocstring
    class BillboardEffect(__core.BillboardEffect):

        def makePointEye(self):
            # type: () -> ()
            pass

    Vec2 = __core.Vec2F
    Vec3 = Vec3F
    Vec4 = __core.Vec4F
    LVecBase3 = VBase3F
    LVecBase4 = __core.VBase4F
    # Vec4 = Vec4F
