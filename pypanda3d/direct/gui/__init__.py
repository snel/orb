from direct.gui.OnscreenText import OnscreenText as __OnscreenText

from pypanda3d.core import PandaNode, NodePath


# noinspection PyMethodOverriding
class OnscreenText(__OnscreenText):
    def node(self):
        # type: () -> PandaNode
        return NodePath.node(self)
