"""
Panda3D methods ported from C++ have invalid signatures--the first parameter `self` must always be omitted.
This module replaces them with corrected ones at compile-time, but with no overhead at run-time.
As an added bonus, the replaced methods have added documentation and type hints.

For this package to work, clients must not import from `panda3d` or `direct` directly, but always import from
`pypanda3d` instead. Otherwise the regular panda3d modules will override the pypanda3d ones.
Note that it is not possible to actually modify the panda3d classes at all--it would cause a runtime error.
"""
