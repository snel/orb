for i in {1..8}
do
    # Generate Server Original Key
    echo "Generate Server Original Key"
    openssl genrsa -passout pass:4XmFUTr7GC -des3 -out server$i.orig.key 2048 &> /dev/null

    # Generate Server Key
    echo "Generate Server Key"
    openssl rsa -in server$i.orig.key -passin pass:4XmFUTr7GC -out server$i.key &> /dev/null

    # PKCS#10 X.509 Certificate Signing Request (CSR) Management
    echo "Request Certificate Signing"
    openssl req -new -key server$i.key -out server$i.csr -subj '/CN=TeamNumber' &> /dev/null

    # X.509 Certificate Data Management 
    echo "Sign Certificate"
    openssl x509 -req -days 365 -in server$i.csr -signkey server$i.key -out server$i.crt &> /dev/null

   echo "Creating Certificates for team $i"
   openssl rsa -in server$i.orig.key -passin pass:4XmFUTr7GC -out "client$i.key" &> /dev/null
   openssl req -new -key "client$i.key" -out "client$i.csr" -subj "/CN=Team$i" &> /dev/null
   openssl x509 -req -days 365 -in "client$i.csr" -signkey "client$i.key" -out "client$i.crt" &> /dev/null
done
