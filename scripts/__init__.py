"""
Example scripts to demonstrate how to interact with game avatars through the
API.

Note that because these scripts are in a subpackage, they need to be called as
follows:
    $ python -m examples.fire
"""
