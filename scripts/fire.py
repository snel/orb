"""
Fires a torpedo directly ahead.
"""

from orb.core.orb_console import OrbConsole
from orb.network.api import Api
from session import add_connect_args, host, creds


def fire(avatar, weapon='TorpedoGun'):
    """Fires the selected weapon directly ahead.

    Args:
        avatar (int): Assumes control of the selected entity.
        weapon (str): Weapon name.
    """
    api = Api(avatar, host, creds, async=False)
    orb = OrbConsole()
    projectile = api.fire(weapon, orb.direction.forward).body
    print "Fired {}.".format(projectile)


if __name__ == "__main__":
    from orb.util.argparse_util import arg_parser
    parser = arg_parser(fire, description=__doc__)
    main = add_connect_args(parser, fire)
    parser.add_argument('--weapon', '-w', type=str)
    parser.invoke(main)
