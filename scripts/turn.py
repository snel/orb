"""
Turns the avatar.
"""

from time import sleep

from orb.core.orb_console import OrbConsole
from orb.network.api import Api
from session import add_connect_args, host, creds


def turn(avatar, throttle=1, direction="starboard", time=2.18):
    """Turns the avatar by applying thrust in the given direction for `time`
    seconds.  Then thrust is applied in the opposite direction to stop
    rotation.

    If the avatar is a ship, and assuming the ship is stationary, the default
    parameters turn it about 45 degrees to starboard.

    Args:
        avatar (int): Assumes control of the selected entity.
        throttle (float): Throttle setting in [0, 1].
        direction (str): Direction to turn:
            either "up", "down", "left" or "right".
        time (float): Time in seconds to apply thrust.
    """
    api = Api(avatar, host, creds, async=True)
    orb = OrbConsole()

    y = orb.direction.__dict__[direction]
    api.turn(y, throttle, time)
    sleep(time)
    api.turn([-y[0], -y[1], -y[2]], throttle, time)
    sleep(time)
    api.stabilise()
    print "Finished."


if __name__ == "__main__":
    from orb.util.argparse_util import arg_parser
    parser = arg_parser(turn, description=__doc__)
    main = add_connect_args(parser, turn)
    parser.add_argument('--direction', '-d', type=str)
    parser.add_argument('--throttle', '-f', type=str)
    parser.add_argument('--time', '-t', type=float)
    parser.invoke(main)
