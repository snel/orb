"""
Fires at a possibly moving target entity.
"""
# In this module we use the convention that `x` is a scalar and 
# `x_` is a vector. Variables named `q_` are quaternions.
# Quaternions are 4-dimensional vectors, but are not homogeneous coordinates.
# They are used to transform vectors the same as 3x3 rotation matrices would,
# except that they are more efficient and avoid gimbal lock.

from time import time

import numpy as np
from orb.network.api import Api
from orb.network.json_encoding import parse_scan
from session import add_connect_args, host, creds
from orb.util.type_casting import cast_quat, cast_vec3
from pypanda3d.core import Quat


def projectile_velocity(u, x_0_, v_0_, x_1_, v_1_):
    """Returns the velocity to fire the projectile so that it hits the target.

    Assumes that the relative velocity between the avatar and target remains
    constant. If both are in a similar orbit, the firing angle should still be
    accurate, because projectiles experience the same gravitational force as
    both the avatar and target.

    Args:
        u: Speed of the projectile leaving the avatar (scalar).
        x_0_ (np.ndarray): Avatar's position vector.
        v_0_ (np.ndarray): Avatar's velocity vector.
        x_1_ (np.ndarray): Target's position vector.
        v_1_ (np.ndarray): Target's velocity vector.

    Returns:
        np.ndarray: Velocity of projectile, including its momentum gained from
            the avatar it is fired from.

    If the relative velocity is zero, we fire directly at the target (`x_10_`).
    Otherwise we solve for the direction as follows:
        a) Consider the triangle with edges at `x_0_`, `x_1_` and `x_2_` for
            the avatar, target and intersection point, respectively.
        b) Let `i_` be the direction from `x_0_` to `x_1_`.
        c) Let `j_` be perpendicular to `i_`, to the same side as as the
            intersection point.  d) For the projectile to meet the target, both
            their velocities must have equal components along the
            `j_` axis. Let the `v_j` be the length of the target's velocity
            along the `j_` axis.
        e) Since `u` is the length of the projectile's velocity from `x_0_` to
            `x_2_`, we can solve for the hypotenuse
            using Pythagoras' rule:
                `u^2 = (v_i)^2 + (v_j)^2 => (v_i)^2 = u^2 - (v_j)^2`.
        f) Now we have the components of the projectile's velocity (`u_`) in
            the `i_` and `j_` axes, and the result follows.
    """
    # Computes relative velocity (v_t_) and translation (x_10_) between avatar
    # (x_0_) and target (x_1_).  x_10 and v_t are the scalar lengths of each
    # vector, respectively.
    v_t_ = v_1_ - v_0_
    x_10_ = x_1_ - x_0_
    x_10 = np.linalg.norm(x_10_)
    v_t = np.linalg.norm(v_t_)

    if not x_10:
        raise ValueError("invalid target")

    i_ = x_10_ / np.linalg.norm(x_10_)
    if not v_t:
        return u * i_
    else:
        j_ = v_t_ - v_t_.dot(i_) * i_
        j_ = j_ / np.linalg.norm(j_)

        v_j = v_t_.dot(j_)
        if u < v_j:
            raise ValueError("out of range")
        else:
            v_i = np.sqrt(u ** 2 - v_j ** 2)  # We discard the negative root.
            return v_i * i_ + v_j * j_


def inv_transform(q_, v_):
    # type: (np.ndarray, np.ndarray) -> np.ndarray
    """Returns the inverse transform of vector `v`, relative to quaternion `q`.

    Args:
        q_ (np.ndarray): Avatar's rotation quaternion (4-dimensional).
        v_ (np.ndarray): Vector to transform (3-dimensional).

    Returns:
        np.ndarray: Transformed vector.

    Although Numpy is convenient for vector operations, it doesn't include
    quaternion functions.  Panda3d does, so instead of reinventing the wheel it
    is convenient to use for that here.
    """
    q_ = cast_quat(q_)
    v_ = cast_vec3(v_)
    q_inv = Quat.ident_quat().__copy__()
    q_inv.invert_from(q_)
    y_ = q_inv.xform(v_)
    return np.array([y_.x, y_.y, y_.z])


def fire_at(avatar, target=None, weapon='TorpedoGun'):
    """Fires at the entity's predicted location, given that its velocity
    remains constant.

    Args:
        avatar (int): Assumes control of the selected entity.
        target (int): Target entity to fire at.
        weapon (str): Weapon name.
    """
    if not target:
        raise ValueError("no target selected")

    api = Api(avatar, host, creds, async=False)

    # Gets the current state of the game.
    # (t_1 - t_0) is the time taken for a round trip from sending a message to
    # receiving its response.
    t_0 = time()
    scan_data = parse_scan(api.scan().body)
    t_1 = time()
    avatar_data = scan_data[avatar]
    target_data = scan_data[target]

    u = avatar_data[weapon]['launch_speed']
    x_0_ = np.array(avatar_data['Transform']['x'])
    v_0_ = np.array(avatar_data['Body']['v'])
    x_1_ = np.array(target_data['Transform']['x'])
    v_1_ = np.array(target_data['Body']['v'])

    # Here we revise u_ by accounting for time lag. This includes:
    #     i)   time taken by this script to compute since getting the state
    #           (`time() - t_1`); and
    #     ii)  time to send message to server (`(t_1 - t_0) / 2`).
    # Residual error is due to:
    #     i)   difference between estimated and true time lag;
    #     ii)  numerical inaccuracy; and
    #     iii) the bullet being fired some distance from the ship and not from
    #       its centre.
    dt = (time() - t_1) + (t_1 - t_0) / 2.
    u_ = projectile_velocity(u, x_0_ + v_0_ * dt, v_0_, x_1_ + v_1_ * dt, v_1_)

    # Rotates the result to be relative to the avatar's current bearing and
    # normalizes to get its direction.  The avatar's heading quaternion is a
    # transformation from world coordinate system to the avatar's coordinate
    # system, so we take the inverse transformation to get back from world
    # coordinates to the avatar's.
    y_ = inv_transform(avatar_data['Transform']['q'], u_)
    y_ = y_ / np.linalg.norm(y_)

    # And we're done.
    projectile = api.fire(weapon, y_).body
    print "Fired {}.".format(projectile)


if __name__ == "__main__":
    from orb.util.argparse_util import arg_parser
    parser = arg_parser(fire_at, description=__doc__)
    main = add_connect_args(parser, fire_at)
    parser.add_argument('--target', '-t', type=int)
    parser.add_argument('--weapon', '-w', type=str)
    parser.invoke(main)
