"""
Prints the state of an entity.
"""

from pprint import pprint

from orb.network.api import Api
from orb.network.json_encoding import parse_scan
from session import add_connect_args, host, creds


def scan(avatar, target=None):
    """Prints the state of the target entity.

    Args:
        avatar (int): Assumes control of the selected entity.
        target (int): Target entity. Default is the avatar.
    """
    api = Api(avatar, host, creds, async=False)
    if target is None:
        target = avatar

    data = parse_scan(api.scan().body)
    print "Scan results for {}:".format(target)
    pprint(data[target])


if __name__ == "__main__":
    from orb.util.argparse_util import arg_parser
    parser = arg_parser(scan, description=__doc__)
    main = add_connect_args(parser, scan)
    parser.add_argument('--target', '-t', type=int)
    parser.invoke(main)
