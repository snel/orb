#!/usr/bin/env python2
# noinspection PyUnresolvedReferences
"""
Orb master (server) with optional GUI and console.

Players can use this for to create their own test environments, but to join an existing game,
see "session.py" or the client scripts in "./scripts/` instead.

Example:
    From the Orb project root directory, run the module like this:

    $ python master.py

    Then in the interactive interpreter, enter commands like this:

    >>> help  # Displays help.
    >>> from orb.core.network.api import Api
    >>> api = Api(1)  # Selects entity 1.
    >>> api.fire('TorpedoGun', [0, 1, 0])  # Fires a torpedo directly ahead.

    When finished, type `exit()` or press CTRL+D (in Linux) or CTRL+Z (in
    Windows). If run from the Python REPL, the graphics window will remain
    visible but inactive until the REPL is closed (CTRL+D).
"""

from __future__ import absolute_import, division, unicode_literals

import logging
from distutils.util import strtobool

import orb.util.log_config as log_config
from orb.components import Transform
from orb.components.components import Components, Ship
from orb.core import config
from orb.core.game_master import GameMaster
from orb.core.orb_console import GameConsole
from orb.network.server import Server, setup_logging as setup_server_logging
from pypanda3d.core import Vec3D

log = logging.getLogger(__file__)
log_config.config("logs/master.log")
setup_server_logging()
banner = "Master session started."


def create_observer(game, home):
    w = game.entity_world
    h = Components(w, home)
    x1 = h.Transform.get_relative_vector(Vec3D.forward())
    x = h.Transform.x + x1 * 8e3 * config.Ship.scale * (1. / 175.)

    observer = game.entity_factory.create_observer(w.create_entity(), x)
    s = Components(w, observer)
    s.Transform.q.set_from_axis_angle(180, Vec3D.up())
    s.Body.v = h.Body.v

    # We reset q, but the BubbleProcessor doesn't know about it unless we
    # call update_component().
    w.update_component((observer,), Transform)
    return observer


def main(num_teams=8, avatar=None, target=None, display=False,
         fullscreen=False, server=True, console=True, gravity=True,
         line_up=False, load_game=None):
    """
    Starts an Orb master session.

    Args:
        num_teams (int): Number of teams.
        avatar (str): Assumes control of the selected entity. This can be an
            entity id e.g. '23', or the name of an astronomical body, e.g. 'sol'.
            If not specified, an "observer" entity is created that other players
            won't be able to see.
        target (int): Selected target entity.
        display (bool): Opens a graphics window iff this is `True`.
        fullscreen (bool): Starts the graphics window in full-screen mode iff
            this is `True`.
        server (bool): Starts a game server iff this is `True`.
        console (bool): Opens a game console iff this is `True`.
        gravity (bool): Enables gravity and orbits iff this is `True`.
        line_up (bool): Places all the ships in a line for player 1 to shoot
            at, nearby player 1's station.  This is intended for testing purposes.
        load_game (str): Path to previously saved game.
    """
    game = GameMaster(
        headless=not display, fullscreen=fullscreen, gravity=gravity)
    if load_game:
        game = GameMaster.load(load_game)

    game.entity_factory.create_world(num_teams)
    orb = GameConsole(game)

    ships = list(game.entity_world.components[Ship])
    if line_up:
        game.entity_factory.line_up(ships[0], ships[1:])

    if not avatar:
        avatar = orb.observer = create_observer(game, ships[0])
    orb.avatar = avatar = orb.entity_by_name(avatar)

    a = Components(game.entity_world, avatar)
    if a.Avatar:
        orb.take_control(avatar)
        orb.api = game.input_processor.controller
    elif display:
        orb.reset_camera(avatar)

    if target:
        orb.target = target = orb.entity_by_name(target)

    if display:
        game.input_processor.set_target(target)

    if server:
        orb.server = Server(game)

    if console:
        orb.open_console(banner)

    game.run()  # Runs the game loop. Must be run from the main thread.


if __name__ == "__main__":
    from orb.util.argparse_util import arg_parser
    parser = arg_parser(main, description=__doc__)
    parser.add_argument('--num_teams', '-n', type=int)
    parser.add_argument('--avatar', '-a', type=str)
    parser.add_argument('--target', '-t', type=int)
    parser.add_argument('--display', '-d', type=strtobool)
    parser.add_argument('--fullscreen', '-f', type=strtobool)
    parser.add_argument('--server', '-s', type=strtobool)
    parser.add_argument('--console', '-c', type=strtobool)
    parser.add_argument('--gravity', '-g', type=strtobool)
    parser.add_argument('--line_up', '-l', type=strtobool)
    parser.add_argument('--load_game', '-o', type=str)
    parser.invoke(main)
