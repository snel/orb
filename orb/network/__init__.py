"""
Client- and server-side network protocols.
"""


# Untested code to handle timeouts.
# @contextlib.contextmanager
# def auto_timeout(self, timeout=2):  # Seconds
#     """
#     See:
#         https://stackoverflow.com/questions/25327455/right-way-to-timeout-a-request-in-tornado
#     """
#
#     handle = IOLoop.instance().add_timeout(time() + timeout, self.timed_out)
#     try:
#         yield handle
#     except Exception as e:
#         log.warn(e, exc_info=1)
#     finally:
#         IOLoop.instance().remove_timeout(handle)
#         if not self._timed_out:
#             self.finish()
#         else:
#             raise Exception("Request timed out")
