import logging
import time

import typing
from six import viewkeys, viewitems
from tornado.httpclient import HTTPError

from orb.components import (
    AttitudeThrusters, Avatar, Body, Colony, Construction, Explosion, FuelTank,
    Generator, Hitpoints,
    KineticOrbiter, LaserGun, LifeSupport, LinearThrusters, Planet, Sensor,
    Shields, Star, Team, TorpedoGun, Transform, WarpDrive, Respawn,
    SelfDestruct)
from orb.components.component_mappers import ComponentMappers
from orb.esper import Entity
from orb.esper.processor import Processor
from orb.network import json_encoding
from orb.network.api import HookedApi
from orb.util import type_casting
from orb.util.threading_util import run_daemon
from pypanda3d.core import Vec3D

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)

# To prevent entity id conflicts, don't create any entities directly on the
# client. We could do a mapping, but this might just be confusing.

synced_components = {
    AttitudeThrusters, Avatar, Body, Explosion, FuelTank, Generator, Hitpoints,
    KineticOrbiter, LaserGun, TorpedoGun, LifeSupport, LinearThrusters, Planet,
    Sensor, Shields, Star, Team, WarpDrive}


def with_appliance(c, json_data):
    x = c.appliance
    x.min_current = json_data['min_current']
    x.max_current = json_data['max_current']
    x.current_available = json_data['current_available']
    x.resistance = json_data['resistance']


def with_capacitor(c, json_data):
    x = c.capacitor
    x.capacitance = json_data['capacitance']
    x.charge = json_data['charge']


def with_countdown(c, json_data):
    x = c.countdown
    x.time_left = json_data['time_left']


def with_engine(c, json_data):
    x = c.engine
    x.throttle = json_data['throttle']
    x.efficiency = json_data['efficiency']


# def with_resistor(c, json_data):
#     x = c.resistor
#     x.resistance = json_data['resistance']


def with_ghost(c, json_data):
    x = c.ghost
    x.range = json_data['range']


def with_projectile(c, json_data):
    c.fired_by = json_data['fired_by']
    c.damage = json_data['damage']


def with_projectile_weapon(c, json_data):
    with_appliance(c, json_data)
    with_countdown(c, json_data)
    c.launch_speed = json_data['launch_speed']


def with_relation(c, json_data):
    x = c.relation
    x.actor = json_data['actor']
    x.target = json_data['target']


class ClientProcessor(Processor):
    """Synchronises state between the local session and the master server."""

    def __init__(self, game, entity, host, creds):
        # type: (Game, Entity) -> ()
        super(ClientProcessor, self).__init__()
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers
        self._ef = game.entity_factory
        self._entity = entity
        self._creds = creds
        self._frequency = .5
        self._last_update = 0
        self._scan_update_response = None
        self._team_update_response = None
        self._updating = True
        self.api = HookedApi(lambda: self._game.avatar.value, host, creds, async=False)

        # It would seem better to create a PeriodicCallback on the IOLoop to
        # update state, instead of doing it manually from another thread as
        # we do here.  But there seems to be a race condition in Tornado that
        # manifests when we try that.  In tornado/concurrent.py", line 270,
        # we get 'NoneType' object has no attribute 'append'.
        run_daemon(name='update_client', target=self._update)

    # Reference assignment is atomic, so no race condition.  See _process().
    def _update_once(self, api, t):
        self._last_update = t

        team = api.hello()
        self._team_update_response = json_encoding.decode(team.body)

        try:
            scan = api.scan()
            # Reference assignment is atomic, so no race condition.
            self._scan_update_response = json_encoding.parse_scan(scan.body)
        except HTTPError:
            # self._updating = False
            self._last_update = t + 30
            self._scan_update_response = {}

    def _update(self):
        # Only updates at regular intervals to prevent throttling by the
        # server.
        while self._updating:
            t = self._game.global_clock.get_long_time()
            if (self._scan_update_response is not None or
                        t < self._last_update + self._frequency):
                time.sleep(self._frequency)
                continue
            self._update_once(self.api, t)

    def update_now(self):
        t = self._game.global_clock.get_long_time()
        self._update_once(self.api, t)
        self._process(0)

    def _process(self, dt):
        # type: (float) -> ()
        """Called from :class:`orb.esper.EntityWorld`."""

        # Reference assignment is atomic, so no race condition.  See
        # _update_once(). If properties is is an empty dict, the avatar has
        # been destroyed or the user is not authorised.

        team_response = self._team_update_response
        if team_response is not None:
            self._team_update_response = None
            self._game.team_processor.scores = {
                int(k): v for k, v in viewitems(team_response['scores'])}

        properties = self._scan_update_response
        if properties is None:
            return
        self._scan_update_response = None

        entities = viewkeys(properties)
        deleted_entities = [
            e for e in self.entity_world.entities if e not in entities]
        added_entities = (
            e for e in entities if e not in self.entity_world.entities)

        for e in deleted_entities:
            self._game.delete(e)

        ew = self.entity_world
        for e in added_entities:
            # Simplifies the job of entity iteration in display_processor.
            ew._next_entity_id = max(e.guid + 1, ew._next_entity_id)

            p = properties[e]
            if not p:
                continue
            transform = p.get('Transform')
            if not transform:
                continue

            x = transform['x']
            x = Vec3D(x[0], x[1], x[2])

            explosion = p.get('Explosion')
            if explosion is not None:
                self._ef.create_explosion(
                    e, x, explosion['radius'])

            torpedo = p.get('Torpedo')
            if torpedo is not None:
                self._ef.create_torpedo(
                    e, p['Projectile']['fired_by'],
                    type_casting.cast_vec3d(x), 1)

            laser = p.get('Laser')
            if laser is not None:
                self._ef.create_laser(
                    e, p['Projectile']['fired_by'],
                    type_casting.cast_vec3d(x), 1)

            ship = p.get('Ship')
            if ship is not None:
                team_id = p['Team']['id']
                self._ef.create_ship(e, team_id, x)

            station = p.get('Station')
            if station is not None:
                team_id = p['Team']['id']
                self._ef.create_station(e, team_id, station.home, x)

        for e in entities:
            props = properties[e]
            if props is None:
                props = {}
                # continue

            # Removes missing components.
            for c in synced_components.intersection(
                    viewkeys(self.entity_world._entities.get(e, {}))):
                if c.__name__ not in props:
                    self.entity_world.remove_component(e, c)

            for k, v in viewitems(props):
                c = self._cm.component_by_name(k)(e)
                isnew = not c
                if isnew:
                    # Adds missing components.  For non-synced components, or
                    # those that need further initialisation, we must do so
                    # above, in the section for added_entities.
                    tp = self._cm.component_type_by_name(k)
                    if not hasattr(tp, 'new'):
                        log.warn("Component type '%s' not initialised " +
                                 "for %s: dump: \n%s.", tp, e, props)
                        continue

                    c = tp.new()
                    # self.entity_world.add_component(e, c)
                else:
                    tp = type(c)

                if tp is AttitudeThrusters:
                    with_appliance(c, v)
                    with_engine(c, v)
                    c.torque = v.torque
                    c.direction = type_casting.cast_vec3d(v.direction)
                    c.time = v.time
                # elif tp is Avatar:  # nothing to do
                # elif tp is BlocksSensor:  # implicit
                elif tp is Body:
                    # c.mass = v.mass  # remains constant
                    c.v = type_casting.cast_vec3d(v.v)
                    c.w = type_casting.cast_vec3d(v.w)
                    # For `BubbleProcessor`.
                    self.entity_world.update_component((e,), Body)
                # elif tp is Circuit:  # implicit
                # elif tp is Colonisable:  # implicit
                elif tp is Colony:
                    with_relation(c, v)
                elif tp is Construction:
                    with_relation(c, v)
                    c.progress = v.progress
                elif tp is Explosion:
                    c.radius = v.radius
                elif tp is FuelTank:
                    c.fuel = v.fuel
                    c.capacity = v.capacity
                elif tp is Generator:  # aka Reactor
                    c.voltage = v.voltage
                    c.efficiency = v.efficiency
                elif tp is Hitpoints:
                    c.hp = v.hp
                elif tp is KineticOrbiter:
                    c.barycenter = v.barycenter
                    c.theta = v.theta
                    c.period = v.period
                    # c.axis = type_casting.cast_vec3d(v.axis)
                    c.semi_major = v.semi_major
                    c.semi_minor = v.semi_minor
                # elif tp is Projectile:  # implicit
                # elif tp is Laser:  # implicit
                # elif tp is Torpedo:  # implicit
                elif tp is LaserGun:
                    with_projectile_weapon(c, v)
                elif tp is TorpedoGun:
                    with_projectile_weapon(c, v)
                # elif tp is ProjectileWeapon:  # irrelevant
                elif tp is LifeSupport:
                    with_appliance(c, v)
                elif tp is Respawn:
                    with_countdown(c, v)
                elif tp is SelfDestruct:
                    with_countdown(c, v)
                elif tp is LinearThrusters:
                    with_appliance(c, v)
                    with_engine(c, v)
                    c.thrust = v.thrust
                    c.direction = type_casting.cast_vec3d(v.direction)
                    c.time = v.time
                # elif tp is Observer:  # master only
                elif tp is Planet:
                    c.name = v.name
                    c.radius = v.radius
                    c.is_moon = v.is_moon
                elif tp is Sensor:
                    with_appliance(c, v)
                elif tp is Shields:
                    with_appliance(c, v)
                    with_capacitor(c, v)
                # elif tp is Ship:  # implicit
                elif tp is Star:
                    c.name = v.name
                # elif tp is Station:  # implicit
                elif tp is Team:
                    c.id = v.id
                elif tp is Transform:
                    c.x = type_casting.cast_vec3d(v.x)
                    c.q = type_casting.cast_quatd(v.q)
                    # For `BubbleProcessor`.
                    self.entity_world.update_component((e,), Transform)
                elif tp is WarpDrive:
                    with_appliance(c, v)
                    with_engine(c, v)
                    with_countdown(c, v)
                    c.speed = v.speed
                    # c.entry_velocity = v.entry_velocity  # master only

                if isnew:
                    self.entity_world.add_component(e, c)
