"""
JSON serialisation protocol.
"""

import json
from json import JSONEncoder
from six import viewitems

try:
    from types import SimpleNamespace as Namespace
except ImportError:
    # Python 2.x fallback
    from argparse import Namespace

from orb.esper import Entity
from orb.util import type_casting
from pypanda3d.core import LVecBase3d, LVecBase3f, QuatF, QuatD

vectypes = (LVecBase3f, LVecBase3d)
quattypes = (QuatF, QuatD)


# Hacky way to get a namespace that is also a dict.
# Convenient for client scripts.  TODO Should evaluate its
# performance; probably better to just define dict methods instead.
class DictNamespace(dict, Namespace):
    def __init__(self, data):
        Namespace.__init__(self, **data)
        dict.__init__(self, **data)


class OrbJsonEncoder(JSONEncoder):
    """JSON encoder for Python types used by Orb."""

    # noinspection PyProtectedMember
    def default(self, obj):
        if isinstance(obj, Entity):
            return obj.guid
        if isinstance(obj, vectypes):
            return type_casting.cast_tuple3(obj)
        if isinstance(obj, quattypes):
            return type_casting.cast_tuple4(obj)
        if hasattr(obj, '__dict__'):
            return obj.__dict__
        if hasattr(obj, '__iter__'):
            return list(obj)  # JSON doesn't support sets, only dicts and lists.
        return JSONEncoder.default(self, obj)

_encoder = OrbJsonEncoder()


def encode(obj):
    """Serializes the Python object as a JSON string."""
    return _encoder.encode(obj)


def decode(json_msg):
    """Deserializes the JSON string as a Python object."""
    # Parse JSON into an object with attributes corresponding to dict keys.
    return json.loads(json_msg, object_hook=DictNamespace)


def parse_entity(string):
    """Returns an Entity parsed from the string."""
    data = decode(string)
    return Entity(int(data))


def parse_scan(response_body):
    """Parses the scan response string and returns a dictionary."""
    data = decode(response_body)
    return {Entity(int(e)): v for e, v in viewitems(data)}
