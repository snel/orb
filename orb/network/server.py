import logging
import ssl

import os
import traceback
from pprint import pformat
from time import time

from six import viewitems
import tornado
from tornado.escape import url_unescape
from tornado.gen import coroutine
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import Application, RequestHandler, Finish

from orb.components import Ship, Station, Torpedo
from orb.core.avatar_controller import AvatarController, CountdownError, \
    AvatarError
from orb.esper import Entity
from orb.network import json_encoding
from orb.network.json_encoding import parse_entity
from orb.util import log_config
from orb.util.threading_util import run_daemon

log = logging.getLogger(__file__)

api_doc = """\
The Orb web API is a pseudo-RESTful API based on HTTP GET requests.
Players connecting to the API execute requests on behalf of their assigned
teams.  Most requests are made with reference to an avatar entity, which
is a unique integer identifying a controllable game object.
    
.. note::
    The server now requires SSL authentication.  So to send requests via cURL,
    you must specify the certificates to use.  And due to the fact that the
    game is often hosted on arbitrary hosts, the certificates may not correspond
    to the actual hostname; in that case "--insecure" will fix the problem.
    For example, playing as team 1 we would use:
    
    .. codeblock:: bash
        curl --insecure --cacert certs/server1.crt --cert certs/client1.crt --key certs/client1.key -G -v https://localhost:2001/hello
    
    The examples below assume the certificate parameters are added as 
    appropriate.    

Example
-------
Assume the server is hosted at "http://orb".  If we want to request
a scan, we can enter the following URL in a browser:
"http://orb/scan".

If we want entity 42 to turn right, we can enter something like:
"http://orb/turn/42?direction=[1, 0, 0]".

Note the direction parameter. This should work as-is in most browsers
that automatically URL-encode addresses (incl. Chrome and Postman).
But to perform the request from cURL we must indicate this, e.g.

.. codeblock:: bash
    curl -G -v "http://orb/turn/42" --data-urlencode "direction=[1, 0, 0]"

Hello
-----
    "/" or "/hello"
    
    Returns current game scores and lists of entities owned by your team.

Events
-----
    "/events"
    
    Returns a list of important past events that affected your team.

Fire
-----
    "/fire/AVATAR?weapon=WEAPON&direction=DIRECTION"
    
    {AvatarController.fire.__doc__}

Power
-----
    "/power/AVATAR?system=SYSTEM&power_setting=POWER_SETTING"
    
    {AvatarController.power.__doc__}

Scan
-----
    "/scan" or "/scan/AVATAR"
    
    Also accepts an optional 'e' parameter to filter the scan for one or 
    more entities.  This must be given as a JSON list.  For example, 
    "/scan?e=[42, 50]" only returns data on entities 42 and 50. 
    
    {AvatarController.scan.__doc__}

Self-destruct
-----
    "/self_destruct/AVATAR"
    
    {AvatarController.self_destruct.__doc__}

Stabilise
-----
    "/stabilise/AVATAR"
    
    {AvatarController.stabilise.__doc__}

Thrust
-----
    "/thrust/AVATAR?direction=DIRECTION&throttle=THROTTLE"
    
    {AvatarController.thrust.__doc__}

Transfer
-----
    "/transfer/AVATAR?target=TARGET&fuel=FUEL"
    
    {AvatarController.transfer.__doc__}

Turn
-----
    "/turn/AVATAR?direction=DIRECTION&throttle=THROTTLE"
    
    {AvatarController.turn.__doc__}

Warp
-----
    "/warp/AVATAR/?throttle=THROTTLE"
    
    {AvatarController.warp.__doc__}
""".format(**globals())
__doc__ = api_doc

errors400 = (AvatarError, ValueError, TypeError)
errors503 = CountdownError


def setup_logging():
    """Overrides global logging config with logging to a separate log file."""
    hdlr = log_config.rotation_handler('logs/server.log')
    for logger in (
        log,
        logging.getLogger('tornado.access'),
        logging.getLogger('tornado.application'),
        logging.getLogger('tornado.general'),
    ):
        logger.handlers = [hdlr]
        logger.propagate = 0


class RateLimiter(object):
    """Provides a ``update()`` method that blocks asynchronously if the number of
    requests exceeds ``request_total`` within the ``request_window``."""
    # Alternatively we could return "429 Too Many Requests".

    # Time window for rate limiting, in seconds.
    request_window = 1
    # Number of requests allowed during the time window.
    #   100*8/1 = 800 requests per second in total.
    request_budget = 100

    def __init__(self, team_id):
        self.request_count = 0
        self.update_time = 0
        self.team_id = team_id

    @coroutine
    def update(self):
        self.request_count += 1
        t = time()
        dt = t - self.update_time
        if dt > self.request_window:
            self.update_time = t
            self.request_count = max(0, self.request_count - self.request_budget)
        if self.request_count > self.request_budget:
            log.debug("Rate limited team %s.", self.team_id)
            yield tornado.gen.sleep(dt - self.request_window)


class ApiHandler(RequestHandler):
    """Base class for Orb request handlers."""

    # noinspection PyMethodOverriding,PyAttributeOutsideInit
    def initialize(self, game, team_id, rate_limiter, control):
        self.team_id = team_id
        self.game = game
        self.rate_limiter = rate_limiter
        self.control = control

    def data_received(self, chunk):
        pass

    @coroutine
    def prepare(self):
        yield self.rate_limiter.update()

    def parse_avatar(self, avatar):
        """Validates that the entity exists and is owned by the team."""
        avatar = parse_entity(avatar)
        with self.game.lock:
            entities = self.game.team_processor.entities.get(self.team_id, ())
        if avatar not in entities:
            msg = "unauthorised: team owns {} but not {}".format(
                entities, avatar)
            log.debug("(team %s) %s", self.team_id, msg)
            self.set_header('Content-Type', 'text/plain')
            self.set_status(403, msg)
            raise Finish()
        return avatar

    def write_json(self, data):
        self.set_header('Content-Type', 'application/json; charset=UTF-8')
        self.write(json_encoding.encode(data))

    def log_exception(self, typ, e, tb):
        if isinstance(e, errors503):
            self.set_status(503, str(e))
            self.set_header('Content-Type', 'text/plain')
            self.finish()
        elif isinstance(e, errors400):
            log.debug("(team %s) %s", self.team_id, e, exc_info=True)
            self.set_status(400, str(e))
            self.set_header('Content-Type', 'text/plain')
            self.write(traceback.format_exc())
            self.finish()
        else:
            super(ApiHandler, self).log_exception(typ, e, tb)


class Handlers(object):
    class Colonise(ApiHandler):
        def get(self, avatar):
            avatar = self.parse_avatar(avatar)
            self.control.set_avatar(avatar)
            world = parse_entity(self.get_argument('world'))
            with self.game.lock:
                station = self.control.colonise(world)
            self.write(str(station.guid) if station else None)

    class Fire(ApiHandler):
        default_direction = [0, 1, 0]

        def get(self, avatar, *args, **kwargs):
            avatar = self.parse_avatar(avatar)
            self.control.set_avatar(avatar)
            weapon = self.get_argument('weapon', 'LaserGun')
            direction = url_unescape(
                self.get_argument('direction', None))
            direction = json_encoding.decode(direction) \
                if direction else self.default_direction
            with self.game.lock:
                projectile = self.control.fire(weapon, direction)
            if projectile:
                self.write(str(projectile.guid))
            else:
                self.set_header('Content-Type', 'text/plain')
                self.set_status(403, "cooldown in effect")
                raise Finish()

    class Power(ApiHandler):
        def get(self, avatar, *args, **kwargs):
            avatar = self.parse_avatar(avatar)
            self.control.set_avatar(avatar)
            system = self.get_argument('system')
            power_setting = float(self.get_argument('power_setting', 1))
            with self.game.lock:
                self.control.power(system, power_setting)

    class Scan(ApiHandler):
        def get(self, avatar=None, *args, **kwargs):
            filtered = self.get_argument('e', None)
            if filtered:
                filtered = [Entity(int(e))
                            for e in json_encoding.decode(filtered)]

            if avatar:
                avatar = self.parse_avatar(avatar)
                self.control.set_avatar(avatar)

                with self.game.lock:
                    sensor = self.control.c.Sensor
                    if not sensor: raise ValueError("no sensors available")
                    entities = set.intersection(sensor.entities, filtered) \
                        if filtered else sensor.entities
                    data = self.game.master_processor.scan(entities)
            else:
                with self.game.lock:
                    sensors = (
                        self.game.component_mappers.Sensor(e)
                        for e in self.game.team_processor.entities.get(
                            self.team_id, ()))
                    entities = [sensor.entities for sensor in sensors if sensor]
                    if entities:
                        entities = set.union(*entities)
                        entities = set.intersection(entities, filtered) \
                            if filtered else entities
                    data = self.game.master_processor.scan(entities)

            # noinspection PyUnboundLocalVariable
            self.write_json(data)

    class Stabilise(ApiHandler):
        def get(self, avatar, *args, **kwargs):
            avatar = self.parse_avatar(avatar)
            self.control.set_avatar(avatar)
            with self.game.lock:
                self.control.stabilise()

    class SelfDestruct(ApiHandler):
        def get(self, avatar, *args, **kwargs):
            avatar = self.parse_avatar(avatar)
            self.control.set_avatar(avatar)
            with self.game.lock:
                self.control.self_destruct()

    class Turn(ApiHandler):
        default_direction = [0, 1, 0]

        def get(self, avatar, *args, **kwargs):
            avatar = self.parse_avatar(avatar)
            self.control.set_avatar(avatar)
            direction = url_unescape(
                self.get_argument('direction', None))
            direction = json_encoding.decode(direction) \
                if direction else self.default_direction
            throttle = float(self.get_argument('throttle', 1.))
            t = float(self.get_argument('t', 1.))
            with self.game.lock:
                self.control.turn(direction, throttle, t)

    class Thrust(ApiHandler):
        default_direction = [0, 1, 0]

        def get(self, avatar, *args, **kwargs):
            avatar = self.parse_avatar(avatar)
            self.control.set_avatar(avatar)
            direction = url_unescape(
                self.get_argument('direction', None))
            direction = json_encoding.decode(direction) \
                if direction else self.default_direction
            throttle = float(self.get_argument('throttle', 1.))
            t = float(self.get_argument('t', 1.))
            with self.game.lock:
                self.control.thrust(direction, throttle, t)

    class Transfer(ApiHandler):
        def get(self, avatar, *args, **kwargs):
            avatar = self.parse_avatar(avatar)
            self.control.set_avatar(avatar)
            fuel = float(self.get_argument('fuel', float('inf')))
            target = parse_entity(self.get_argument('target'))
            with self.game.lock:
                self.control.transfer(target, fuel)

    class Warp(ApiHandler):
        def get(self, avatar, *args, **kwargs):
            avatar = self.parse_avatar(avatar)
            self.control.set_avatar(avatar)
            throttle = float(self.get_argument('throttle', 1.))
            with self.game.lock:
                self.control.warp(throttle)

    class Events(ApiHandler):
        def get(self, *args, **kwargs):
            with self.game.lock:
                journal = self.game.team_processor.events.get(self.team_id)
            self.write_json(journal)

    class Hello(ApiHandler):
        def get(self, *args, **kwargs):
            tp = self.game.team_processor
            team_id = self.team_id
            with self.game.lock:
                data = {
                    'ships': tp.of_type(team_id, Ship),
                    'stations': tp.of_type(team_id, Station),
                    'torpedoes': tp.of_type(team_id, Torpedo),
                    'scores': {team_id: tp.scores.get(team_id, 0)
                               for team_id in tp.entities},
                    'gravity': self.game.gravity,
                }
            self.write_json(data)


class ServerApi(object):
    """Orb API server."""

    def __init__(self, game, team_id, port):
        self.game = game
        self.team_id = team_id
        self.port = port
        args = dict(game=game, team_id=team_id,
                    rate_limiter=RateLimiter(team_id),
                    control=AvatarController(game, 0))
        cert_path = os.path.join(os.path.dirname(__file__), "../../certs")
        ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        ssl_context.load_cert_chain(
            certfile=os.path.join(cert_path, 'server{}.crt'.format(team_id)),
            keyfile=os.path.join(cert_path, 'server{}.key'.format(team_id)))
        ssl_context.load_verify_locations(
            os.path.join(cert_path, 'client{}.crt'.format(team_id)))

        # Note: HTTPServer can place constraints on header or body size.
        self.app = HTTPServer(Application([
            # Avatar API, bound to an entity.
            (r'/colonise/(\d+)/?', Handlers.Colonise, args),
            (r'/fire/(\d+)/?', Handlers.Fire, args),
            (r'/power/(\d+)/?', Handlers.Power, args),
            (r'/scan(?:/(\d+))?/?', Handlers.Scan, args),
            (r'/self_destruct/(\d+)/?', Handlers.SelfDestruct, args),
            (r'/stabilise/(\d+)/?', Handlers.Stabilise, args),
            (r'/thrust/(\d+)/?', Handlers.Thrust, args),
            (r'/transfer/(\d+)/?', Handlers.Transfer, args),
            (r'/turn/(\d+)/?', Handlers.Turn, args),
            (r'/warp/(\d+)/?', Handlers.Warp, args),

            # General team API, not bound to an entity.
            (r'/events/?', Handlers.Events, args),
            (r'(/?$)|(/hello/?)', Handlers.Hello, args),
        ], serve_traceback=True), ssl_options=ssl_context)
        self.app.listen(self.port)


class Server(object):
    """Orb server.

    Manages independent HTTP server sockets for each team.
    """

    def __init__(self, game, start_port=2001):
        """Constructor. Creates an HTTP server socket for each team.

        Args:
            game: Game instance.
            start_port: Port number assigned to the first team. The second team
                will get ``start_port+1``, and so on.
        """
        #ports = [4597, 9253, 5674, 9853, 7093, 8376, 4935, 2958]
        #self.teams = {team_id: ServerApi(game, team_id, ports[i])
        #              for i, team_id in enumerate(game.team_processor.team_ids)}

        self.teams = {team_id: ServerApi(game, team_id, start_port + i)
                      for i, team_id in enumerate(game.team_processor.team_ids)}
        run_daemon(name='IOLoop', target=IOLoop.current().start)

        log.info("Opened server sockets:\n%s", pformat(
            ["port {} -> team {}".format(v.port, k)
             for k, v in viewitems(self.teams)]))
