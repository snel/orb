"""
Client-side API allowing a team to control their avatar remotely.
"""

import logging
from functools import wraps

import sys
from tornado import escape
from tornado.httpclient import HTTPClient, AsyncHTTPClient, HTTPError
from typing import Union, List, Callable

from orb.core.avatar_controller import AvatarController
from orb.esper.entity import Entity
from orb.util import log_config
from orb.util.argparse_util import docstr
from orb.util.decoration import class_decorator, get_methods

log = logging.getLogger(__file__)


preamble = __doc__


def setup_logging():
    """Overrides global logging config with logging to a separate log file."""
    hdlr = log_config.rotation_handler('logs/client.log')
    for logger in (
        log,
        logging.getLogger('tornado.access'),
        logging.getLogger('tornado.application'),
        logging.getLogger('tornado.general'),
    ):
        logger.handlers = [hdlr]
        logger.propagate = 0


def avatar_doc(f):
    """Decorator that copies the docstrings from AvatarMaster."""
    d = getattr(AvatarController, f.__name__).__doc__
    # if f.__name__ == "scan":
    d = d.replace("pprint(scan())", "pprint(scan().body)")
    return docstr(before=d)(f)


def http_error_handler(f):
    """Decorator that logs API error info as returned by server and the local
    trace stack."""
    def render_traceback_():
        return [str(sys.last_value)]  # Just returns the message, no traceback.

    @wraps(f)
    def g(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except HTTPError as e:
            log.error(e.response.body, exc_info=0)
            # Disables IPython's traceback.
            e._render_traceback_ = render_traceback_
            raise

    return g


def rate_limited(f):
    """Decorator that rate limits outgoing requests to avoid getting
    rate-limited by the server.  This makes for smoother gameplay both for this
    session and everyone else on the team.  Rate limited functions return
    ``None``."""
    request_count = [0]
    request_limit = 3

    @wraps(f)
    def g(*args, **kwargs):
        if request_count[0] > request_limit: return
        callback = kwargs.get('callback')

        def h(x):
            request_count[0] -= 1
            if callback: callback(x)

        request_count[0] += 1
        kwargs['callback'] = h
        return f(*args, **kwargs)

    return g


@class_decorator(http_error_handler)
class Api(object):
    __doc__ = preamble + """\

    Remember that data returned by the server is contained in the response body,
    in string format.  See ``json_encoding`` for ways to decode responses.

    Synchronous example:
        >>> from orb.network.json_encoding import parse_scan
        >>> api = Api(42, async=False)
        >>> scan_data = parse_scan(api.scan().body)  # type: dict

    Asynchronous example:
        >>> from orb.network.json_encoding import parse_scan
        >>> def f(response):
        >>>     print parse_scan(response.body)
        >>> api = Api(42, async=True)
        >>> api.scan(callback=f)

    .. seealso:: :class:`orb.core.avatar_controller.AvatarController`
    """

    def __init__(self, avatar, host, creds, async=True):
        # type: (Union[int, Entity], str) -> ()
        """Constructor.

        Args:
            avatar (int): Avatar entity to control.
            host (str): URL to the server.
            creds (dict): Dict of certificates, with keys for "client_key",
                "client_cert" and "ca_certs".
            async (bool): Performs synchronous requests iff ``True``, otherwise
                asynchronous.  Asynchronous by default.
        """
        if isinstance(avatar, Entity):
            avatar = avatar.guid

        self.avatar = avatar
        self.host = host
        self.client = AsyncHTTPClient() if async else HTTPClient()
        self.creds = creds

    def events(self, callback=None):
        # type: () -> List[str]
        """Returns a list of events that occurred to the avatar's team,
        e.g. ships destroyed, whether the game was won, etc.

        Returns:
            ``list`` of event strings.

        Example:
            >>> print(events().body)  # Prints the event logs.
        """
        return self.client.fetch(
            "{self.host}/events"
            .format(**locals()), callback=callback, **self.creds)

    def hello(self, callback=None):
        """Returns current scores for all teams and lists of avatars available
        to your team."""
        return self.client.fetch(
            "{self.host}/"
            .format(**locals()), callback=callback, **self.creds)

    @avatar_doc
    def colonise(self, world, callback=None):
        return self.client.fetch(
            "{self.host}/colonise/{self.avatar}"
            "?world={world}"
            .format(**locals()), callback=callback, **self.creds)

    @avatar_doc
    def fire(self, weapon, direction=None, callback=None):
        if direction is None:
            direction = ''
        elif not isinstance(direction, str):
            direction = escape.url_escape(
                '[{}, {}, {}]'.format(direction[0], direction[1], direction[2]))

        return self.client.fetch(
            "{self.host}/fire/{self.avatar}"
            "?weapon={weapon}&direction={direction}"
            .format(**locals()), callback=callback, **self.creds)

    @avatar_doc
    def power(self, system, power_setting, callback=None):
        return self.client.fetch(
            "{self.host}/power/{self.avatar}"
            "?system={system}&power_setting={power_setting}"
            .format(**locals()), callback=callback, **self.creds)

    @avatar_doc
    def scan(self, whole_team=False, callback=None):
        if whole_team:
            return self.client.fetch(
                "{self.host}/scan"
                .format(**locals()), callback=callback, **self.creds)
        else:
            return self.client.fetch(
                "{self.host}/scan/{self.avatar}"
                .format(**locals()), callback=callback, **self.creds)

    @avatar_doc
    def self_destruct(self, callback=None):
        return self.client.fetch(
            "{self.host}/self_destruct/{self.avatar}"
            .format(**locals()), callback=callback, **self.creds)

    @avatar_doc
    def stabilise(self, callback=None):
        return self.client.fetch(
            "{self.host}/stabilise/{self.avatar}"
            .format(**locals()), callback=callback, **self.creds)

    @avatar_doc
    def thrust(self, direction, throttle=1, t=.1, callback=None):
        if direction is None:
            direction = ''
        elif not isinstance(direction, str):
            direction = escape.url_escape(
                '[{}, {}, {}]'.format(direction[0], direction[1], direction[2]))

        return self.client.fetch(
            "{self.host}/thrust/{self.avatar}"
            "?direction={direction}&throttle={throttle}&t={t}"
            .format(**locals()), callback=callback, **self.creds)

    @avatar_doc
    def transfer(self, target, fuel=float('inf'), callback=None):
        return self.client.fetch(
            "{self.host}/transfer/{self.avatar}"
            "?target={target}&fuel={fuel}"
            .format(**locals()), callback=callback, **self.creds)

    @avatar_doc
    def turn(self, direction, throttle=1, t=.1, callback=None):
        if direction is None:
            direction = ''
        elif not isinstance(direction, str):
            direction = escape.url_escape(
                '[{}, {}, {}]'.format(direction[0], direction[1], direction[2]))

        return self.client.fetch(
            "{self.host}/turn/{self.avatar}"
            "?direction={direction}&throttle={throttle}&t={t}"
            .format(**locals()), callback=callback, **self.creds)

    @avatar_doc
    def warp(self, throttle, callback=None):
        return self.client.fetch(
            "{self.host}/warp/{self.avatar}"
            "?throttle={throttle}"
            .format(**locals()), callback=callback, **self.creds)

Api.get_methods = get_methods


class HookedApi(Api):
    """Hooks the API entity to a function."""

    def __init__(self, avatar_fun, host, creds, async=False):
        # type: (Callable, str) -> ()
        Api.__init__(self, -1, host, creds, async)
        self.avatar_fun = avatar_fun

    @property
    def avatar(self):
        e = self.avatar_fun()
        if isinstance(e, int):
            return e
        return e.guid if e else None

    @avatar.setter
    def avatar(self, value):
        return


class GameApi(HookedApi):
    __doc__ = preamble + Api.__doc__ + """\

    This class also updates the local game session for smoother gameplay, and 
    keeps the avatar in sync with the one selected in-game.
    """

    def __init__(self, control, host, creds, async=False):
        # type: (Callable, str) -> ()
        HookedApi.__init__(self, lambda: control.avatar, host, creds, async)
        self.control = control  # type: AvatarController

    @avatar_doc
    @rate_limited
    def fire(self, weapon, direction=None, callback=None):
        # TODO: create the torpedo/laser entity with callback
        # so it appears on-screen ASAP.
        # self.avatar_master.fire(weapon, direction)
        return super(GameApi, self).fire(weapon, direction, callback)

    @avatar_doc
    def stabilise(self, callback=None):
        self.control.stabilise()
        return super(GameApi, self).stabilise()

    @avatar_doc
    @rate_limited
    def turn(self, direction, throttle=1, t=1, callback=None):
        self.control.turn(direction, throttle, t)
        return super(GameApi, self).turn(direction, throttle, t, callback)

    @avatar_doc
    @rate_limited
    def thrust(self, direction, throttle=1, t=1, callback=None):
        self.control.thrust(direction, throttle, t)
        return super(GameApi, self).thrust(direction, throttle, t, callback)

    @avatar_doc
    def warp(self, throttle, callback=None):
        self.control.warp(throttle)
        return super(GameApi, self).warp(throttle, callback)
