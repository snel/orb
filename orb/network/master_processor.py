import logging

import typing
from six import viewitems

from orb.components import (
    Avatar, Body, Colony, Construction, Explosion,
    FuelTank, Hitpoints, KineticOrbiter, Planet, Generator, Projectile, Sensor,
    Shields, Ship, Station, Star, Team, Torpedo, Transform, Laser,
    AttitudeThrusters, LaserGun, LifeSupport, LinearThrusters, TorpedoGun,
    WarpDrive, Respawn, SelfDestruct)
from orb.components.component_mappers import ComponentMappers
from orb.esper import Processor
from orb.util.dic import merge

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game


log = logging.getLogger(__file__)


def with_appliance(c):
    x = c.appliance
    return {
        'min_current': x.min_current,
        'max_current': x.max_current,
        'current_available': x.current_available,
        'resistance': x.resistance,
    }


def with_capacitor(c):
    x = c.capacitor
    return {
        'capacitance': x.capacitance,
        'charge': x.charge,
    }


def with_countdown(c):
    x = c.countdown
    return {
        'time_left': x.time_left,
    }


def with_engine(c):
    x = c.engine
    return {
        'throttle': x.throttle,
        'efficiency': x.efficiency,
    }


# def with_resistor(c):
#     x = c.resistor
#     return {
#         'resistance': x.resistance,
#     }


def with_ghost(c):
    x = c.ghost
    return {
        'range': x.range,
    }


def with_projectile(c):
    return {
        'fired_by': c.fired_by,
        'damage': c.damage,
    }


def with_projectile_weapon(c):
    return merge(with_appliance(c), with_countdown(c), {
        'launch_speed': c.launch_speed,
    })


def with_relation(c):
    x = c.relation
    return {
        'actor': x.actor,
        'target': x.target,
    }


class MasterProcessor(Processor):
    """Updates the set of properties (i.e. components) associated with each
    entity, for network transmission. This implements the server-side protocol
    of the scan API.

    Scanning, and in particular `inspecting` is a performance hotspot.
    So we do it once, for everyone, every frame.
    """

    def __init__(self, game):
        # type: (Game) -> ()
        super(MasterProcessor, self).__init__()
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers
        self._ew = game.entity_world
        self._inspections = {}
        self._updated_components = {}

    def scan(self, entities_in_range):
        # JSON requires that dict keys be strings.
        return {str(e.guid): v for e, v in (
            (e, self._inspections.get(e)) for e in entities_in_range)
            if v is not None}

    # noinspection PyProtectedMember
    def _process(self, dt):
        # type: (float) -> ()
        # Must refresh all dynamic components here.
        # That is, components with data that must be sync'ed over the network
        # but do not have updated() called when they are updated.
        # For example:
        # for component_type in (Body, Transform):
        #    self._update(component_type)

        for component_type, entities in viewitems(self._updated_components):
            self._update_inspections(component_type, entities)
            entities.clear()

    def _update(self, component_type):
        entities = self._game.entity_world._components.get(
            component_type, set())
        self._update_inspections(component_type, entities)

    def _added(self, component_type, entities):
        self._update_inspections(component_type, entities)

    def _removed(self, component_type, entities):
        for e in entities:
            x = self._inspections.get(e, ())
            if x:
                x.pop(component_type.__name__, None)
            if not x:
                self._inspections.pop(e, None)

    def _updated(self, component_type, entities):
        # This delays updating inspections until the next _process step. This
        # avoids cache misses and updating the same components multiple times.
        bag = self._updated_components.get(component_type, None)
        if bag is None:
              bag = self._updated_components[component_type] = set()
        bag.update(entities)

    def _update_inspections(self, component_type, entities):
        for e in entities:
            data = self._inspections.get(e)
            if data is None:
                data = self._inspections[e] = {}

            c = self._ew._entities.get(e, {}).get(component_type)
            if not c or self._cm.Observer(e):
                data.pop(component_type.__name__, None)
                continue

            if component_type is AttitudeThrusters:
                x = merge(with_appliance(c), with_engine(c), {
                    'torque': c.torque,
                    'direction': c.direction,
                    'time': c.time,
                })
            elif component_type is Avatar:
                x = {}
            # elif component_type is BlocksSensor:  # implicitly added on startup
            elif component_type is Body:
                x = {
                    'v': c.v,
                    'w': c.w,
                    # 'mass': c.mass,  # remains constant
                }
            # elif component_type is Bubble:  # singleton
            # elif component_type is Circuit:  # implicitly added to new avatars
            # elif component_type is Colonisable:  # implicitly added on startup
            elif component_type is Colony:
                x = with_relation(c)
            elif component_type is Construction:
                x = merge(with_relation(c), {
                    'progress': c.progress,
                })
            elif component_type is Explosion:
                x = {
                    'radius': c.radius,
                }
            elif component_type is FuelTank:
                x = {
                    'fuel': c.fuel,
                    'capacity': c.capacity,
                }
            elif component_type is Generator:  # aka Reactor
                x = {
                    'voltage': c.voltage,
                    'efficiency': c.efficiency,
                }
            elif component_type is Hitpoints:
                x = {
                    'hp': c.hp,
                }
            elif component_type is KineticOrbiter:
                x = {
                    'barycenter': c.barycenter,
                    'theta': c.theta,
                    'period': c.period,
                    'semi_major': c.semi_major,
                    'semi_minor': c.semi_minor,
                }
            elif component_type is Projectile:
                x = {
                    'fired_by': c.fired_by,
                    'damage': c.damage,
                }
            elif component_type is Laser:
                x = {}
            elif component_type is Torpedo:
                x = {}
            elif component_type is LaserGun:
                x = with_projectile_weapon(c)
            elif component_type is TorpedoGun:
                x = with_projectile_weapon(c)
            # elif component_type is ProjectileWeapon:  # irrelevant
            elif component_type is LifeSupport:
                x = with_appliance(c)
            elif component_type is LinearThrusters:
                x = merge(with_appliance(c), with_engine(c), {
                    'thrust': c.thrust,
                    'direction': c.direction,
                    'time': c.time,
                })
            # elif component_type is Observer:  # master only
            elif component_type is Planet:
                x = {
                    'name': c.name,
                    'radius': c.radius,
                    'is_moon': c.is_moon,
                }
            elif component_type is Respawn:
                x = with_countdown(c)
            elif component_type is SelfDestruct:
                x = with_countdown(c)
            elif component_type is Sensor:
                x = with_appliance(c)
            elif component_type is Shields:
                x = merge(with_appliance(c), with_capacitor(c))
            elif component_type is Ship:
                x = {}
            elif component_type is Star:
                x = {
                    'name': c.name,
                }
            elif component_type is Station:
                x = {
                    'home': c.home,
                }
            elif component_type is Team:
                x = {
                    'id': c.id,
                }
            elif component_type is Transform:
                x = merge({
                    'x': c.x,
                    'q': c.q,
                })
            elif component_type is WarpDrive:
                x = merge(with_appliance(c), with_engine(c), with_countdown(c), {
                    'speed': c.speed,
                    # 'entry_velocity: c.entry_velocity,  # master only
                })
            else:
                continue

            data[component_type.__name__] = x
