from orb.network import json_encoding
from orb.network.client_processor import ClientProcessor

from orb.core.game import Game
from orb.network.api import Api


class GameClient(Game):
    """Game type that runs on endpoints (i.e. local sessions but not on the
    master). """

    def __init__(self, avatar, host, creds, *args, **kwargs):
        api = Api(avatar, host, creds, async=False)
        data = json_encoding.decode(api.hello().body)
        gravity = data['gravity']
        Game.__init__(self, is_client=True, gravity=gravity, avatar=avatar,
                      *args, **kwargs)

        self.input_processor.hook_api(host, creds)
        self.client_processor = ClientProcessor(self, avatar, host, creds)
        self.entity_world.add_processor(self.client_processor)

    def _collision(self, e0, e1):
        pass
