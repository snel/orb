# noinspection PyClassHasNoInit
class Component(object):
    """Class from which all components should derive."""


__doc__ = Component.__doc__
