from typing import Dict, Iterator, List, Sequence, Set, Type

from orb.esper import Component, Entity, Processor
from orb.util.serializable import serializable


@serializable(ignore=['_processors'])
# noinspection PyTypeChecker
class EntityWorld(object):
    def __init__(self):
        # type: () -> ()
        """A World object keeps track of all Entities, Components and
        Processors.

        A World contains a database of all Entity/Component assignments.  It
        also handles calling the process method on any Processors assigned to
        it.
        """
        self._processors = []  # type: List[Processor]
        self._next_entity_id = 0
        # TODO: factor out direct external use of these dicts.
        self._components = {}  # type: Dict[Type[Component], Set[Entity]
        self._entities = {}  # type: Dict[Entity, Dict[Type[Component], Component]]

    @property
    def entities(self):
        return self._entities

    @property
    def components(self):
        return self._components

    def clear_database(self):
        # type: () -> ()
        """Removes all entities and components from the world."""
        self._components.clear()
        self._entities.clear()
        self._next_entity_id = 0

    def add_processor(self, processor_instance, priority=0):
        # type: (Processor, int) -> ()
        """Adds a Processor instance to the world.

        Args:
            processor_instance: A Processor instance.
            priority: Processors with higher priority are processed first.
        """
        assert issubclass(processor_instance.__class__, Processor)
        processor_instance.priority = priority
        processor_instance.entity_world = self
        self._processors.append(processor_instance)
        self._processors.sort(key=lambda processor: -processor.priority)

    def remove_processor(self, processor_type):
        # type: (Type[Processor]) -> ()
        """Removes a Processor from the world, by type.

        Args:
            processor_type: Class type of the Processor to remove.
        """
        for processor in self._processors:
            if type(processor) == processor_type:
                processor.entity_world = None
                self._processors.remove(processor)

    def create_entity(self):
        # type: () -> Entity
        """Returns a new Entity.

        Returns:
            An Entity with the next entity id in the sequence.
        """
        self._next_entity_id += 1
        return Entity(self._next_entity_id)

    def delete_entity(self, entity):
        # type: (Entity) -> ()
        """Deletes an Entity from the EntityWorld.

        This will also delete any Component instances that are assigned to the
        Entity.

        Raises a KeyError if the given entity does not exist in the database.

        Args:
            entity: The Entity to delete.
        """
        e = (entity,)
        for component_type in self._entities[entity]:
            for processor in self._processors:
                # noinspection PyProtectedMember
                processor._removed(component_type, e)

            self._components[component_type].discard(entity)

            if not self._components[component_type]:
                del self._components[component_type]

        del self._entities[entity]

    # TODO: rename these functions.
    def component_for_entity(self, entity, component_type):
        # type: (Entity, Type[Component]) -> Component
        """Retrieve a specific Component instance for an Entity.

        Args:
            entity: The Entity to retrieve the Component for.
            component_type: The Component instance you wish to retrieve.

        Returns:
            A Component instance, if it exists for the Entity, otherwise None.
        """
        return self._entities.get(entity, {}).get(component_type)

    def add_component(self, entity, component_instance):
        # type: (Entity, Component) -> ()
        """Adds a new Component instance to an Entity.

        If a Component of the same type is already assigned to the Entity,
        it will be replaced with the new one.

        Args:
            entity: The Entity to associate the Component with.
            component_instance: A Component instance.
        """
        if not isinstance(entity, Entity): entity = Entity(entity)

        component_type = type(component_instance)

        if component_type not in self._components:
            self._components[component_type] = set()

        self._components[component_type].add(entity)

        if entity not in self._entities:
            self._entities[entity] = {}

        self._entities[entity][component_type] = component_instance

        e = (entity,)
        for processor in self._processors:
            # noinspection PyProtectedMember
            processor._added(component_type, e)

    def remove_component(self, entity, component_type):
        # type: (Entity, Type[Component]) -> ()
        """Removes a Component instance from an Entity, by type.

        Has no effect if either the given entity or Component type does not
        exist in the database.

        Args:
            entity: The Entity to remove the Component from.
            component_type: The type of the Component to remove.

        Example:
            >>> # Removes the Transform component from entity 1.
            >>> remove_component(1, Transform)
        """
        for processor in self._processors:
            # noinspection PyProtectedMember
            processor._removed(component_type, (entity,))

        cd = self._components.get(component_type)
        if cd:
            cd.discard(entity)
        if not cd:
            del self._components[component_type]

        ed = self._entities.get(entity)
        if ed:
            ed.pop(component_type, None)
        if not ed:
            del self._components[entity]

        return entity

    # TODO: make all these functions able to accept either one or many entities.
    def update_component(self, entities, component_type, ignore_processor=None):
        # type: (Sequence[Entity], Type[Component]) -> ()
        """Updates Component instances, by type.

        Args:
            entities: List of entities to update.
            component_type: The type of Component to update.

        Example:
            >>> update_component((1,), Transform)
        """
        for processor in self._processors:
            if processor == ignore_processor:
                continue
            # noinspection PyProtectedMember
            processor._updated(component_type, entities)

    def get_component(self, component_type):
        # type: (Type[Component]) -> Iterator[(Entity, Component)]
        """Gets an iterator for Entity, Component pairs.

        Args:
            component_type: The Component type to retrieve.

        Returns:
            An iterator for (Entity, Component) tuples.
        """
        entity_db = self._entities
        for entity in self._components.get(component_type, ()):
            yield entity, entity_db[entity][component_type]

    def get_components(self, *component_types):
        # type: (Sequence[Component]) -> Iterator[(Entity, List[Component])]
        """Gets an iterator for Entity and multiple Component sets.

        Args:
            component_types: Two or more Component types.

        Returns:
            An iterator for (Entity, Component1, Component2, etc) tuples.
        """
        entity_db = self._entities
        comp_db = self._components

        try:
            entity_set = set.intersection(*[comp_db[ct] for ct in component_types])
            for entity in entity_set:
                yield entity, [entity_db[entity][ct] for ct in component_types]
        except KeyError:
            pass

    def process(self, *args):
        """Processes all Processors, in order of their priority."""
        for processor in self._processors:
            # noinspection PyProtectedMember
            processor._process(*args)


__doc__ = EntityWorld.__doc__
