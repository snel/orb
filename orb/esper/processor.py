import typing
from typing import Sequence, Type

from orb.esper import Entity, Component

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.esper.entity_world import EntityWorld


# noinspection PyMethodMayBeStatic,PyUnusedLocal
class Processor(object):
    def __init__(self, *args, **kwargs):
        # type: () -> ()
        self.entity_world = None  # type: EntityWorld

    def _process(self, dt):
        # type: (float) -> ()
        # Be careful when iterating over lists/dicts in the update loop; if
        # inside the loop we somehow change the list during iteration.  This
        # could happen if we iterate over e.g. ships and the loop creates a new
        # ship.
        pass

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['entity_world']
        return state

    # def __setstate__(self, state):
    #     self.__dict__.update(state)

    # Components should not be added/updated/removed from within these methods;
    # otherwise we run the risk of infinite recursive loops.

    def _added(self, component_type, entities):
        # type: (Type[Component], Sequence[Entity]) -> ()
        """Called after the component type is added to each entity in the
        list."""
        pass

    def _updated(self, component_type, entities):
        # type: (Type[Component], Sequence[Entity]) -> ()
        """Called after the component type is updated for each entity in the
        list."""
        pass

    def _removed(self, component_type, entities):
        # type: (Type[Component], Sequence[Entity]) -> ()
        """Called before the component type is removed from each entity in the
        list."""
        pass


__doc__ = Processor.__doc__
