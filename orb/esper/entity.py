class Entity(object):
    """Encapsulation of a GUID to use in the entity database."""

    def __init__(self, guid):
        # (int) -> ()
        """
        Constructor.

        Args:
            guid (int): Globally unique identifier.
        """
        self.guid = guid

    def __repr__(self):
        return 'e_{}'.format(self.guid)

    def __hash__(self):
        return self.guid

    def __eq__(self, other):
        if isinstance(other, Entity):
            return self.guid == other.guid
        else:
            return self.guid == other

    # Python 3.x
    def __bool__(self):
        return bool(self.guid)

    # Python 2.x
    def __nonzero__(self):
        return bool(self.guid)

    # NB!
    def __ne__(self, other):
        return not self == other


__doc__ = Entity.__doc__
