ENTITY_TAG = 'entity'
"""An ``Entity`` id associated with the node. Useful during collision testing."""
