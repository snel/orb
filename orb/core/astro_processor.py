import logging

import typing
from typing import Dict

from orb.components import Star, Planet
from orb.components.component_mappers import ComponentMappers
from orb.esper import Entity, Processor

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game


log = logging.getLogger(__file__)


class AstroProcessor(Processor):
    """Provides access to stars, planets and moons by name."""

    def __init__(self, game):
        # type: (Game) -> ()
        super(AstroProcessor, self).__init__()
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers
        self.stars = {}  # type: Dict[str, Entity]
        self.planets = {}  # type: Dict[str, Entity]
        self.moons = {}  # type: Dict[str, Entity]

    def _added(self, component_type, entities):
        if component_type is Star:
            for e in entities:
                star = self._cm.Star(e)
                self.stars[star.name] = e

        if component_type is Planet:
            for e in entities:
                planet = self._cm.Planet(e)
                if planet.is_moon:
                    self.moons[planet.name] = e
                else:
                    self.planets[planet.name] = e

    # We don't bother checking for removed entities, since they are never
    # removed.  TODO (@Sean): They can be when the game master reloads the game.
