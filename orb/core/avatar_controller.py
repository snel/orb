import logging
import math

import typing
from six import viewitems
from typing import Dict, Optional, Union, Tuple

from orb.components import (
    LaserGun, TorpedoGun, FuelTank, AttitudeThrusters, LinearThrusters,
    WarpDrive, SelfDestruct)
from orb.components.components import Components
from orb.core import config
from orb.esper import Entity
from orb.util.math_util import look_at
from pypanda3d.core import Vec3, Vec3D, LVecBase3d

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)


def _validate_vec3(y):
    # type: (Vec3) -> bool
    """Returns ``True`` iff the vector is finite."""
    return all(not math.isinf(x) and not math.isnan(x)
               for x in (y.x, y.x, y.z))


# Transforms coordinates from z-forward to y-forward.
turn_q = look_at(Vec3D.right(), Vec3D.back())

LaserGun_name = LaserGun.__name__.lower()
TorpedoGun_name = TorpedoGun.__name__.lower()


class AvatarError(Exception):
    """Raised when an avatar received an unsupported request."""
    pass


class CountdownError(Exception):
    """Raised if a system is not ready to perform an operation yet."""
    pass


class AvatarController(object):
    """Controls an avatar entity.

    Note that this class controls a game instance directly.  To control the
    avatar in multi-player, players should use :class:`~orb.network.api.Api`.

    NB: this class is not thread-safe. Method calls must be synchronized with
    the render thread.
    """
    # We need to ensure the game is robust against "very large" inputs that are
    # finite, but that cause computations to overflow. That could cause the
    # game to crash!
    # Also, to guard against NaN, use:
    #   >>> if not 0. <= throttle <= 1.: good()
    # instead of
    #   >>> if throttle >= 0 and throttle <= 1: bad().

    def __init__(self, game, avatar):
        # type: (Game, Entity) -> ()
        """Constructor.

        Args:
            game: ``Game`` instance.
            avatar: Avatar entity to attach to. Must have all the following
                components: ``Avatar``, ``Transform``, ``Body``.
        """
        self._game = game  # type: Game
        self._cm = game.component_mappers
        self.avatar = avatar  # type: Entity
        """Avatar to control.  This is exposed to conform to the same interface
        as :class:`~orb.network.api.Api`, but should not be set directly.
        Use ``~set_avatar()`` instead."""
        self.c = Components(self._game.entity_world, self.avatar)
        """Avatar components."""

    def colonise(self, world):
        # type: (Entity) -> Entity
        """Begins colonisation of a planet by starting construction of a
        space station or aborts construction in progress.

        The request will fail if the avatar is not a ship, if the target is
        not colonisable or if the planet is already colonised (i.e. already
        has a completed station orbiting it).  An additional requirement is
        that another ship from your team must also be in close orbit to the
        planet for colonisation to begin.  They are free to leave afterwards,
        but your avatar will remain immobile until construction is complete.

        If other teams also try to colonise the world at the same time,
        the first one to complete their station wins, and the other stations
        will be destroyed.

        Args:
            world (Entity): Planet to colonise.  If ``0`` or ``None`` and the avatar
                was already busy colonising, then the process will be cancelled
                and your avatar will be free to move again.

        Returns:
            The new station entity under construction or ``None`` if cancelled.

        Example:
            >>> colonise(13)    # Colonises planet 13.
        """
        self._check_alive()
        if not self.c.Ship:
            raise AvatarError("avatar not a ship")
        if not world:
            return self._game.construction_processor.abort(self.avatar)
        return self._game.construction_processor.build_station(self.avatar, world)

    def fire(self, weapon, direction=None):
        # type: (str, Union[Tuple[float, float, float], Vec3D]) -> Optional[Entity]
        """Fires the avatar's weapon named at direction, relative to the
        avatar's heading.

        A cooldown timer takes effect between shots.  The request will fail if
        the weapon is not available, or is not supplied with enough electrical
        power or fuel.  Firing weapons is disallowed while the warp drive is
        active.  If the request fails, no weapons are fired.

        Weapons available on ships and stations:
            "TorpedoGun",
            "LaserGun"

        Args:
            weapon (str): Weapon name (case insensitive).
            direction (Tuple[float, float, float]): Direction vector.
                Default is ``[0, 1, 0]`` (forwards).

        Returns:
            ``Entity``, if the weapon fired a projectile. Otherwise ``None``.

        Example:
            >>> fire('TorpedoGun', [0, 1, 0])  # Fires a torpedo directly ahead.
        """
        self._check_alive()
        w = self.arsenal().get(weapon.lower())
        if not w:
            raise ValueError("no such weapon available: '{}'".format(weapon))

        if direction is None:
            direction = Vec3D.forward()
        if not isinstance(direction, LVecBase3d):
            direction = Vec3D(direction[0], direction[1], direction[2])
        if not _validate_vec3(direction):
            raise ValueError("direction must be finite vector")
        if not direction.normalize():
            raise ValueError("zero-length direction vector")

        if isinstance(w, LaserGun):
            fuel = config.LaserGun.fuel
            cooldown = config.LaserGun.cooldown
        elif isinstance(w, TorpedoGun):
            fuel = config.TorpedoGun.fuel
            cooldown = config.TorpedoGun.cooldown
        else:
            raise Exception("not implemented: \"{}\"".format(weapon))

        self._check_fuel(fuel)
        self._check_power(w.appliance)

        warp_drive = self.c.WarpDrive
        if warp_drive and warp_drive.engine.throttle:
            raise AvatarError("unable to fire while warp drive is active")

        time_left = w.countdown.time_left
        if time_left > 0.:
            raise CountdownError(
                "cooldown: {:.2f} seconds remaining until ready".format(time_left))

        # Legacy code: although we reset the position in ``fire_projectile``, we
        # must set the position x at creation-time to ensure a good owner
        # bubble is selected for the projectile.
        x = self.c.Transform.x
        factory = self._game.entity_factory
        world = self._game.entity_world
        boost = (w.appliance.current_available /
                 w.appliance.min_current)
        if isinstance(w, LaserGun):
            projectile = factory.create_laser(
                world.create_entity(), self.avatar, x, min(1, boost))
            factory.fire_projectile(
                self.avatar, projectile, w.launch_speed, direction)
        elif isinstance(w, TorpedoGun):
            projectile = factory.create_torpedo(
                world.create_entity(), self.avatar, x, min(1, boost))
            factory.fire_projectile(
                self.avatar, projectile, w.launch_speed, direction)
        else:
            raise Exception("not implemented: \"{}\"".format(weapon))

        fuel_tank = self.c.FuelTank
        fuel_tank.fuel -= fuel
        w.countdown.time_left = cooldown

        self._game.entity_world.update_component((self.avatar,), type(w))
        self._game.entity_world.update_component((self.avatar,), FuelTank)
        return projectile

    def power(self, system, power_setting):
        # type: (str, float) -> ()
        """Sets the power distribution of avatar systems, including engines,
        shields, weapons and sensors.

        The request may fail if no circuit/generator exists, if the system does
        not exist or if the power setting is outside the permissible range.  If
        the request fails, the power setting remains the same as it was before.

        Effect on systems:
            Supported systems include:
                - ``"AttitudeThrusters"``
                - ``"LaserGun"``
                - ``"LifeSupport"``
                - ``"LinearThrusters"``
                - ``"Sensor"``
                - ``"Shields"``
                - ``"TorpedoGun"``
                - ``"WarpDrive"``

            Some systems can function with power set below nominal:
                - ``"Sensors"``: reduces range.
                - ``"Shields"``: reduces maximum charge and recharge rate.

            Other systems are disabled when power is below nominal
            (``I_min``).  In particular, **without power to life support,
            the avatar suffers damage over time**.

            On the other hand, certain systems can be boosted by setting power
            above nominal:
                - ``"Sensors"``: increases range.
                - ``"Shields"``: increases shield effectiveness against damage.
                - ``"TorpedoGun"``: increases torpedo range.
                - ``"LaserGun"``: increases laser damage.
                - makes no difference to thrusters or the warp drive.

        Computations:
            The supplied power setting is used to determine the resistance to
            set on the system. For example, ``0`` disables the system,
            ``1`` enables it, and values above ``1`` boost the system.

            Let :math:`I_{min}` and :math:`I_{max}` be the nominal and maximum
            current drawn by the system.
            The power setting ``k`` is interpreted as:
                | :math:`k = I/I_{min} \in [0, I_{max}/I_{min}]`.

            This is used to compute the resistance to set on the system:
                | :math:`R = V/I`     [definition of resistance]

            The resultant power use is:
                | :math:`P = I V`     [definition of electrical power]

            See :class:`~orb.core.circuit_processor.CircuitProcessor` for
            details on fuel usage.

        Args:
            system (str): Name of system to adjust, e.g. ``"Shields"``, ``"Sensor"``.
                Case insensitive.
            power_setting (float): Power setting as a fraction of nominal
                power required.

        Returns:
            ``None``.

        Example:
            >>> power('Shields', 0)    # Sets shield power to 0%.
            >>> power('Shields', 1)    # Sets shield power to 100%.
            >>> power('Shields', 1.2)  # Sets shield power to 120%.
        """
        self._check_alive()
        circuit = self.c.Circuit
        if not circuit:
            raise AvatarError("no circuit available")
        generator = self.c.Generator
        if not generator:
            raise AvatarError("no generator available")
        sm = self._cm.component_by_name(
            system, case_sensitive=False)(self.avatar)
        if not sm or not hasattr(sm, 'appliance'):
            raise ValueError("no such system available: '{}'".format(system))

        k = float(power_setting)
        r, i = self.power_setting(generator.voltage, sm.appliance.min_current, k)
        if not i <= sm.appliance.max_current:
            raise ValueError(
                "power_setting exceeds maximum value: {:.2E}".format(
                    sm.appliance.max_current / sm.appliance.min_current))

        sm.appliance.resistance = r
        self._game.entity_world.update_component((self.avatar,), type(system))

    def scan(self):
        # type: () -> Dict[Entity, object]
        """Returns all entities in sensor range and their properties.
        This includes stars, planets and the avatar itself, regardless of sensor
        range.

        The request will fail if the avatar has no sensors available.

        Returns:
            ``dict`` of entity -> properties.

        Example:
            >>> from pprint import pprint
            >>> pprint(scan())  # Pretty-prints results from a scan.
        """
        self._check_alive()
        sensor = self.c.Sensor
        if not sensor:
            raise AvatarError("no sensors available")

        return self._game.master_processor.scan(self.avatar, sensor.entities)

    def self_destruct(self):
        # type: () -> None
        """Starts the self-destruct sequence.

        The avatar will be irreversibly destroyed after
        :py:const:`~orb.core.config.Avatar.self_destruct_delay` seconds.

        Returns:
            ``None``.

        Example:
            >>> self_destruct()
        """
        self._check_alive()
        sd = self.c.SelfDestruct
        if sd:
            raise AvatarError("already self-destructing")

        self._game.entity_world.add_component(self.avatar, SelfDestruct(
            config.Avatar.self_destruct_delay))
        log.warn("Self-destruct sequence activated for {} in {} seconds!".format(
            self.avatar, config.Avatar.self_destruct_delay))

    def stabilise(self):
        # type: () -> ()
        """Enters attitude-stabilisation mode.

        The avatar will engage attitude thrusters to cancel rotational moments.
        In other words, it will stop spinning.  Overrides any previous turn
        commands.  This mode will remain selected until the next ``turn``
        command is issued.

        Returns:
            ``None``.

        Example:
            >>> stabilise()
        """
        self._check_alive()
        attitude_thrusters = self.c.AttitudeThrusters
        if not attitude_thrusters:
            raise AvatarError("no attitude thrusters available")
        attitude_thrusters.stabilise = True

    def thrust(self, direction, throttle=1, t=.1):
        # type: (Union[Tuple[float, float, float], float, Vec3D], float) -> ()
        """Sets the throttle on linear thrusters to accelerate in the given
        direction.

        Overrides any previous thrust commands.  The request will fail if no
        thrusters are available, no more fuel remains or if thrusters are not
        supplied with enough electrical power.  If the request fails, throttle
        settings will remain unchanged.

        Args:
            direction (Tuple[float, float, float]): Direction vector, relative
                to the avatar's heading.
            throttle (float): Throttle setting, in :math:`[0, 1]`.
                Default is ``1``.
            t (float): Duration of time to apply thrust for, in seconds.
                Default is ``0.1`` seconds.

        Returns:
            ``None``.

        Example:
            >>> thrust([0, 1, 0])  # Accelerates directly ahead.
        """
        self._check_alive()
        linear_thrusters = self.c.LinearThrusters
        if not linear_thrusters:
            raise AvatarError("no linear thrusters available")

        throttle = float(throttle)
        if not 0. <= throttle <= 1.:
            raise ValueError("throttle must be in [0, 1]")

        t = float(t)
        if not t >= 0.:
            raise ValueError("time must be positive")

        if not isinstance(direction, LVecBase3d):
            direction = Vec3D(direction[0], direction[1], direction[2])
        if not _validate_vec3(direction):
            raise ValueError("direction must be a finite vector")
        if not direction.normalize():
            # If the throttle is cut, the direction doesn't matter.
            if throttle:
                raise ValueError("zero-length direction vector")
            else:
                direction = Vec3D.up()

        if throttle:
            self._check_fuel()
            self._check_power(linear_thrusters.appliance)

        linear_thrusters.engine.throttle = throttle
        linear_thrusters.direction = direction
        linear_thrusters.time = t
        self._game.entity_world.update_component((self.avatar,), LinearThrusters)

    # Some players had problems using the `fuel` parameter from cURL.
    # Adding the "-G" option seems to fix the problem, e.g.:
    #   curl -G "http://localhost:2001/transfer/19?target=27&fuel=100"
    def transfer(self, target, fuel=float('inf')):
        # type: (Entity, float) -> None
        """Transfers the specified amount of fuel to the target entity.

        Transfers are instantaneous and can be made to anyone, anywhere
        within :py:const:`~orb.core.config.Team.max_transfer_distance` metres.

        The request will fail if the avatar does not have enough fuel
        available or if the target is not a ship or station.

        Args:
            target (Entity): Ship or station entity to transfer fuel to.
            fuel (float): Amount of fuel to transfer, in kg.  Default is
                infinity, which means transfer everything we have.  The actual
                amount transferred may be less if the target's fuel tanks lack
                capacity.

        Returns:
            ``None``.

        Example:
            >>> transfer(42, 1e3)  # Transfers 1 ton of fuel to entity 42.
        """
        self._check_alive()

        transform0 = self.c.Transform
        transform1 = self._cm.Transform(target)
        y = (transform1.x - transform0.x).length() \
            if (transform0 and transform1) else None
        if y is None or y > config.Team.max_transfer_distance:
            raise AvatarError("target does not exist or is out of range")

        fuel_tank0 = self.c.FuelTank
        fuel_tank1 = self._cm.FuelTank(target)
        if not self._cm.Ship(target) and not self._cm.Station(target):
            raise AvatarError("target not a ship or station")
        if not fuel_tank0 or not fuel_tank1:
            raise AvatarError("no fuel tank available")

        fuel = float(fuel)
        if not fuel > 0.:
            # We might consider adding support for negative transactions, but
            # it might surprise players when their teammates accidentaly leach
            # from them this way.  Also, we would need to validate that the
            # target is on your team.
            raise ValueError("fuel transfer amount must be a positive number")

        fuel = min(fuel, max(0., fuel_tank1.capacity - fuel_tank1.fuel))
        self._check_fuel(fuel)

        fuel_tank0.fuel -= fuel
        fuel_tank1.fuel += fuel
        self._game.entity_world.update_component((self.avatar, target), FuelTank)

    def turn(self, direction, throttle=1, t=.1):
        # type: (Union[Tuple[float, float, float], float, Vec3D], float) -> ()
        """Sets the throttle on attitude thrusters to turn in the given direction.

        Overrides any previous turn commands.  The request will fail if no
        thrusters are available, no more fuel remains or if thrusters are not
        supplied with enough electrical power.  If the request fails, throttle
        settings will remain unchanged.

        Args:
            direction(Tuple[float, float, float]): Direction to turn, relative
                to the avatar's heading.  The norm of this vector must be
                non-zero unless throttle is ``0``.
            throttle (float): Throttle setting, in :math:`[0, 1]`.
                Default is ``1``.
            t (float): Duration of time to apply thrust for, in seconds.
                Default is ``0.1`` seconds.

        Returns:
            ``None``.

        Example:
            >>> turn([1, 0, 0])  # Turns to starboard (right).
        """
        self._check_alive()
        attitude_thrusters = self.c.AttitudeThrusters
        if not attitude_thrusters:
            raise AvatarError("no attitude thrusters available")

        throttle = float(throttle)
        if not 0. <= throttle <= 1.:
            raise ValueError("throttle must be in [0, 1]")

        t = float(t)
        if not t >= 0.:
            raise ValueError("time must be positive")

        if not isinstance(direction, LVecBase3d):
            direction = Vec3D(direction[0], direction[1], direction[2])
        if not _validate_vec3(direction):
            raise ValueError("direction must be a finite vector")
        if not direction.normalize():
            # If the throttle is cut, the direction doesn't matter.
            if throttle:
                raise ValueError("zero-length direction vector")
            else:
                direction = Vec3D.up()

        if throttle:
            self._check_fuel()
            self._check_power(attitude_thrusters.appliance)

        # Converts direction relative to forward direction to one relative to
        # up direction, to use as torque direction.
        direction = turn_q.xform(direction)

        attitude_thrusters.engine.throttle = throttle
        attitude_thrusters.direction = direction
        attitude_thrusters.time = t
        attitude_thrusters.stabilise = False
        self._game.entity_world.update_component((self.avatar,), AttitudeThrusters)

    def warp(self, throttle):
        # type: (float) -> ()
        """Engages or disengages the warp drive, which accelerates the avatar
        directly ahead.

        When exiting warp, the avatar's previous linear velocity is conserved.
        Overrides any previous thrust commands.  A cooldown timer takes effect
        between activations.  The request will fail if no warp drive is
        available, is not ready to engage, no more fuel remains or the warp
        drive is not supplied with enough electrical power.  If the request
        fails, the throttle setting will remain unchanged.

        Args:
            throttle (float): Throttle setting, in :math:`[0, 1]`.
                Engages the warp drive if non-zero, and disengages otherwise.

        Returns:
            ``None``.

        Example:
            >>> warp(1)  # Engages warp drive.
            >>> warp(0)  # Disengages warp drive.
        """
        self._check_alive()
        warp_drive = self.c.WarpDrive
        if not warp_drive:
            raise AvatarError("no warp drive available")

        throttle = float(throttle)
        if not 0. <= throttle <= 1:
            raise ValueError("throttle must be in [0, 1]")

        if throttle:
            self._check_fuel()
            self._check_power(warp_drive.appliance)

        if not warp_drive.engine.throttle and throttle:
            time_left = warp_drive.countdown.time_left
            if time_left > 0.:
                raise CountdownError(
                    "cooldown: {:.2f} seconds remaining until ready".format(time_left))

            warp_drive.countdown.time_left = config.WarpDrive.cooldown
            warp_drive.entry_velocity = self.c.Body.v

        warp_drive.engine.throttle = throttle
        self._game.entity_world.update_component((self.avatar,), WarpDrive)

    def set_avatar(self, avatar):
        # type: (Entity) -> ()
        """Resets the avatar entity to control."""
        if avatar is not None and not isinstance(avatar, int) and not isinstance(avatar, Entity):
            raise TypeError()
        self.avatar = avatar  # type: Entity
        self.c.entity = avatar

    def arsenal(self):
        # type: () -> dict
        """Returns a dict of weapons available."""
        return {k: v for k, v in viewitems({
            LaserGun_name: self.c.LaserGun,
            TorpedoGun_name: self.c.TorpedoGun,
        }) if v is not None}

    def _check_fuel(self, min_fuel=0.):
        """Verifies that there is enough fuel remaining."""
        fuel_tank = self.c.FuelTank
        if not fuel_tank:
            raise AvatarError("no fuel tank available")
        if fuel_tank.fuel <= min_fuel:
            if min_fuel <= 0:
                raise AvatarError("out of fuel")
            else:
                raise AvatarError("insufficient fuel")

    @staticmethod
    def _check_power(appliance):
        """Verifies that there is enough power for the appliance."""
        if appliance.current_available < appliance.min_current:
            raise AvatarError("system disabled; adjust power setting to enable")
        if appliance.current_available > appliance.max_current:
            raise AvatarError("circuit breaker tripped; reduce power setting")

    def _check_alive(self):
        """Verifies that the avatar exists and is not in the process of
        respawning."""
        if not self.c.Avatar or not self.c.Transform or not self.c.Body:
            respawn = self.c.Respawn
            if respawn:
                time_left = respawn.countdown.time_left
                raise CountdownError(
                    "respawning: {:.2f} seconds remaining until ready".format(time_left))
            else:
                raise AvatarError("entity destroyed or not an avatar")

    @staticmethod
    def power_setting(emf, min_current, power_setting):
        """Computes the resistance setting for the given power setting.

        Args:
            emf: Generator EMF, in Volts.  Must be non-negative.
            min_current: Current induced at power setting ``1``, in Coulomb.
                Must be positive.
            power_setting: Fraction of ``min_current`` Coulombs desired.
                Must be non-negative.

        Returns:
            (resistance, induced current)
        """
        if not emf >= 0:
            raise ValueError("emf must be non-negative")
        if not min_current > 0:
            raise ValueError("power setting must be positive")
        if not power_setting >= 0:
            raise ValueError("power setting must be non-negative")

        if power_setting == 0:
            i = 0
            r = float('inf')
        else:
            i = power_setting * min_current
            r = emf / i
        return r, i
