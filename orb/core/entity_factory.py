import itertools
import logging
import math
import random
from pprint import pformat

import typing
from direct.task import Task
from six import viewitems
from typing import Dict, List

from orb import ENTITY_TAG
from orb.components import (
    AttitudeThrusters, Avatar, BlocksSensor, Body, Circuit, Colonisable, Colony,
    Explosion, FuelTank, Generator, Hitpoints, Laser, LaserGun, LifeSupport,
    LinearThrusters, Observer, Planet, Projectile, Sensor, Shields, Ship, Star,
    Station, Team, Torpedo, TorpedoGun, Transform, WarpDrive, SelfDestruct)
from orb.components.component_mappers import ComponentMappers
from orb.components.components import Components
from orb.core import config
from orb.esper import Entity, EntityWorld
from orb.util import type_casting, math_util
from orb.util.math_util import look_at, quatd
from orb.util.dic import merge
from orb.util.roman import int_to_roman
from pypanda3d.bullet import (BulletGhostNode, BulletRigidBodyNode, BulletShape,
                              BulletSphereShape)
from pypanda3d.core import (
    BillboardEffect, BitMask32, Material, NodePath, PointLight, QuatD, Vec3,
    Vec3D, Vec4)

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)
UNIT_SCALE = Vec3(1.0, 1.0, 1.0)
random.seed(config.seed)


# Note: we can get the collision shape from a mesh like this:
#   shape_np = BulletHelper.fromCollisionSolids(model_np, True)
# But this is less efficient and robust than using simple shapes.
class EntityFactory(object):
    """Utility for creating entities."""

    def __init__(self, game):
        # type: (Game) -> ()
        self._game = game  # type: Game
        self._ew = game.entity_world  # type: EntityWorld
        self._cm = game.component_mappers  # type: ComponentMappers
        self._loader = self._game.base.loader

    def _add_component(self, e, c):
        self._ew.add_component(e, c)
        return c

    def _add_components(self, e, components):
        for c in components:
            self._ew.add_component(e, c)

    def _filter_sol_tree(self, tree):
        """Returns a transformed version of the star system tree for
        printing."""
        if not hasattr(tree, '__iter__'):
            return tree
        return [(k['entity'], k['name'], self._filter_sol_tree(v))
                if v else (k['entity'], k['name'])
                for k, v in tree]

    def create_world(self, num_teams, stations_per_team=1, ships_per_team=4):
        # type: (int) -> (Entity, Dict[str, Entity])
        """Creates the game world, including team avatars, and returns a dict
        of the solar system."""
        # Creates the environment.
        sol_list, sol_tree = self.create_solar_system()
        log.info("Created solar system: \n%s", pformat(
            self._filter_sol_tree(sol_tree), indent=4))

        # Assigns a home planet to each team.
        unclaimed = iter(sorted(self._game.astro_processor.planets.values(),
                                key=lambda x: x.guid))
        team_worlds = {t + 1: [unclaimed.next()
                               for i in xrange(stations_per_team)]
                       for t in xrange(num_teams)}

        # Creates one station for each team, in orbit around its own planet.
        team_stations = {team_id: [self.create_station(
            self._ew.create_entity(), team_id, home, Vec3D.zero())
            for home in worlds]
            for team_id, worlds in viewitems(team_worlds)
        }
        for team_id, stations in viewitems(team_stations):
            for station in stations:
                world = self._cm.Station(station).home
                self._cm.Transform(station).x = (
                    self._cm.Transform(world).x +
                    Vec3D.back() * (self._cm.Planet(world).radius +
                                    config.Colony.max_distance * .9))
                self.setup_station(station)
                self.set_in_orbit(station, world, Vec3D.up())

                self._add_component(station, Colony(station, world))
                self._add_component(world, Colony(station, world))

        # We set x, but the BubbleProcessor doesn't know about it unless we
        # call update_component()
        for stations in team_stations.values():
            self._ew.update_component(stations, Transform)
            self._ew.update_component(stations, Body)

        # Creates some ships per team, placed close to their station.
        team_ships = {
            team_id: [self.spawn_ship(team_id)
                      for _ in xrange(ships_per_team)]
            for team_id in team_worlds}

        for team_id, ships in viewitems(team_ships):
            self.line_up(team_stations[team_id][0], ships)

        # Logs results.
        teams = [(t, team_worlds[t], team_stations[t], team_ships[t])
                 for t in sorted(team_worlds)]
        world_names = {e: x['name'] for e, x in viewitems(sol_list)}
        s = ["team {} => "
             "{{worlds: {:6} ({:15}), stations: {}, ships: {}}}".format(
                team_id,
                str(worlds),
                str([world_names.get(world, str(world)) for world in worlds]),
                stations,
                ships) for team_id, worlds, stations, ships in teams]
        log.info("Created teams: \n%s", pformat(s, indent=4, width=-1))

        return sol_list, sol_tree

    def setup_station(self, entity):
        # home = self._cm.Station(entity).home
        self._cm.Body(entity).w = (
            Vec3D.forward() * 1e2 * config.scale_force)

    def spawn_ship(self, team_id, station=None, entity=None):
        """Spawns a new ship for the given team.

        If ``station`` is specified, the ship will respawn nearby that station.
        The actual location is randomised to prevent new ships from spawning
        on top of each other only to immediately crash into each other and
        respawn again.
        """
        stations = self._game.team_processor.of_type(team_id, Station)
        if not stations:
            raise Exception('cannot respawn, no stations remaining')

        if station and station not in stations:
            raise Exception('station not found')
        else:
            # Selects the oldest surviving station.
            station = sorted(stations, cmp=lambda x, y: -cmp(x, y))[0]

        if not entity:
            entity = self._ew.create_entity()

        ship = self._game.entity_factory.create_ship(
            entity, team_id, Vec3D.zero())
        s = Components(self._game.entity_world, ship)

        t = Components(self._game.entity_world, station)
        y = t.Body.radius + s.Body.radius
        s.Transform.x = math_util.somewhere_nearby(t.Transform.x, y * 2, y * 3)
        s.Transform.q = look_at(s.Transform.x - t.Transform.x, Vec3.up())

        s.Body.v = t.Body.v
        # We reset v, but the BubbleProcessor doesn't know about it unless we
        # call update_component().
        self._game.entity_world.update_component((ship,), Transform)
        self._game.entity_world.update_component((ship,), Body)
        return ship

    def line_up(self, captain, soldiers,
                distance=3e1*config.Ship.scale, spacing=0.5):
        # type: (Entity, List[Entity]) -> ()
        """Arranges the 'soldiers' in a line in front of the 'captain'."""
        for i, soldier in enumerate(soldiers):
            cpt = Components(self._ew, captain)
            sdr = Components(self._ew, soldier)

            # Faces towards the captain.
            q = cpt.Transform.q.__copy__()
            q.invert_in_place()

            x0 = cpt.Transform.x
            x1 = cpt.Transform.get_relative_vector(Vec3D.forward())
            x2 = cpt.Transform.get_relative_vector(Vec3D.right())
            sdr.Transform.x = x0 + (x1 + x2 * spacing * i) * distance
            sdr.Transform.q = q
            sdr.Body.v = cpt.Body.v

            # We reset x and v, but the BubbleProcessor doesn't know about it
            # unless we call update_component().
            self._ew.update_component((soldier,), Transform)
            self._ew.update_component((soldier,), Body)

    def create_sol(self):
        # type: () -> Dict[str, Entity]
        """Creates the solar system and returns a dict of Sol and its planets,
        mapping names to entity ids.  This solar system resembles the real
        one."""
        from solar_system import (
            Sol, Mercury, Venus, Earth, Luna, Mars, Deimos, Phobos, Jupiter,
            Io, Europa, Ganymede, Callisto, Saturn, Neptune, Uranus)
        from config import scale_astro_radius, scale_astro_dist, \
            scale_astro_mass

        _tex_path = config.Display.tex_path
        texture = {
            Sol.name: _tex_path + "2k/2k_sun.jpg",
            Mercury.name: _tex_path + "2k/2k_mercury.jpg",
            Venus.name: _tex_path + "2k/2k_venus_atmosphere.jpg",
            Earth.name: _tex_path + "land_ocean_ice_cloud_2048.jpg",
            Luna.name: _tex_path + "2k/2k_moon.jpg",
            Mars.name: _tex_path + "2k/2k_mars.jpg",
            Deimos.name: _tex_path + "1k/deimos_1k_tex.jpg",
            Phobos.name: _tex_path + "1k/phobos_1k_tex.jpg",
            Jupiter.name: _tex_path + "2k/2k_jupiter.jpg",
            Io.name: _tex_path + "2k/2k_makemake_fictional.jpg",
            Europa.name: _tex_path + "2k/2k_eris_fictional.jpg",
            Ganymede.name: _tex_path + "2k/2k_haumea_fictional.jpg",
            Callisto.name: _tex_path + "2k/2k_ceres_fictional.jpg",
            Saturn.name: _tex_path + "2k/2k_saturn.jpg",
            Uranus.name: _tex_path + "2k/2k_uranus.jpg",
            Neptune.name: _tex_path + "2k/2k_neptune.jpg",
            # Pluto.name: _tex_path + "2k/2k_pluto.jpg",
        }

        # Planet radius scaling.
        _radius = merge(
            {Sol.name: 2e-2 * Sol.radius * scale_astro_radius},
            {x.name: x.radius * scale_astro_radius for x in Sol.planets},
            {x.name: x.radius * scale_astro_radius for y in Sol.planets
             for x in y.moons},
        )
        _scale_big_planet = 2e-1
        for x in (Jupiter, Saturn, Uranus, Neptune):
            _radius[x.name] *= _scale_big_planet
        for x in itertools.chain(Jupiter.moons):
            _radius[x.name] *= 2 * _scale_big_planet

        # Planet distance scaling.
        _dist = merge(
            {x.name: x.distance * scale_astro_dist for x in Sol.planets},
            {x.name: (4e1 * x.distance + x.radius + y.radius) * scale_astro_dist
             for y in Sol.planets for x in y.moons},
        )
        _dist[Jupiter.name] *= .7 ** 2
        _dist[Saturn.name] *= .7 ** 3
        _dist[Uranus.name] *= .7 ** 4 * .8
        _dist[Neptune.name] *= .7 ** 5 * .9
        for x in itertools.chain(Jupiter.moons):
            _dist[x.name] *= 200

        # Planet mass scaling.
        _mass = merge(
            {Sol.name: 1e-3 * Sol.radius * scale_astro_mass},
            {x.name: x.mass * scale_astro_mass for x in Sol.planets},
            {x.name: x.mass * scale_astro_mass for y in Sol.planets
             for x in y.moons},
        )

        star = {
            'mass': _mass[Sol.name],
            'radius': _radius[Sol.name],
            'texture': texture[Sol.name],
            'planets': [
                (planet.name, {
                    'mass': _mass[planet.name],
                    'radius': _radius[planet.name],
                    'distance': _dist[planet.name],
                    'texture': texture[planet.name],
                    'moons': [
                        (moon.name, {
                            'mass': _mass[moon.name],
                            'radius': _radius[moon.name],
                            'distance': _dist[moon.name],
                            'texture': texture[moon.name],
                        }) for moon in planet.moons
                    ]
                }) for planet in Sol.planets
            ]
        }

        return self._create_star_system(star)

    def create_solar_system(self, num_planets=12):
        # type: () -> Dict[str, Entity]
        """Creates a solar system and returns a dict of the star and its planets,
        mapping names to entity ids.  This system is fictional."""
        from solar_system import (
            Sol, Mercury, Earth, Luna, Jupiter, Uranus)
        from config import scale_astro_radius, scale_astro_dist, \
            scale_astro_mass

        _tex_path = config.Display.tex_path
        small_planet_textures = [
            _tex_path + "2k/2k_mercury.jpg",
            _tex_path + "2k/2k_venus_atmosphere.jpg",
            _tex_path + "land_ocean_ice_cloud_2048.jpg",
            _tex_path + "2k/2k_mars.jpg",
            _tex_path + "1k/deimos_1k_tex.jpg",
            _tex_path + "1k/phobos_1k_tex.jpg",
            _tex_path + "2k/2k_makemake_fictional.jpg",
            _tex_path + "2k/2k_eris_fictional.jpg",
            _tex_path + "2k/2k_haumea_fictional.jpg",
            _tex_path + "2k/2k_ceres_fictional.jpg",
            # _tex_path + "2k/2k_pluto.jpg",
        ]
        big_planet_textures = [
            _tex_path + "2k/2k_jupiter.jpg",
            _tex_path + "2k/2k_saturn.jpg",
            _tex_path + "2k/2k_uranus.jpg",
            _tex_path + "2k/2k_neptune.jpg",
        ]
        planet_names = list(
            {"Albertus", "Alauda", "JayLark", "Rakhat", "Caleo", "Gor",
             "Aurelius", "Amara", "Jed", "Tiber", "Heinlein", "Tanna", "Fiore",
             "Utakt", "Seiryuki", "Alina", "Sagan", "Zaidant", "Pele",
             "Liberte", "Sultana", "Alderaan", "Hirshorn", "Rogue",
             "Infinitium", "Roddenberry", "Asimov", "Goddard"})

        random.shuffle(planet_names)
        planet_names = iter(planet_names)
        a = Mercury.distance  # Minimum separation distance.
        b = Uranus.distance - Jupiter.distance  # Maximum separation distance.

        # We want to successive planets and moons to be placed in a
        # monotonically increasing sequence of distances from the star.
        # Not the most elegant code, but it works.
        pdist = [0]
        mdist = [0]

        def next(tmp, x, i=0):
            if not i: tmp[0] = 0
            tmp[0] += x
            return tmp[0]

        moon_types = [
            lambda planet_name, i: {
                'name': "{} {}".format(planet_name, int_to_roman(i + 1)),
                'texture': small_planet_textures[
                    int(random.uniform(0, len(small_planet_textures)))],
                'radius': Luna.radius * scale_astro_radius * random.uniform(.3,
                                                                            3),
                'distance': next(mdist, random.uniform(a, b),
                                 i) * scale_astro_dist,
                'mass': Luna.mass * scale_astro_mass * random.uniform(.3, 3),
            }
        ]
        planet_types = [
            # Rocky world
            lambda name, i: {
                'name': name,
                'texture': small_planet_textures[
                    int(random.uniform(0, len(small_planet_textures)))],
                'radius': Earth.radius * scale_astro_radius * random.uniform(.3,
                                                                             3),
                'distance': next(pdist,
                                 random.uniform(a, b)) * scale_astro_dist,
                'mass': Earth.mass * scale_astro_mass * random.uniform(.3, 3),
                'moons': [
                    moon_types[int(random.uniform(0, len(moon_types)))](name, j)
                    for j in xrange(min(1, int(random.expovariate(2))))]
            },
            # Gas giant
            lambda name, i: {
                'name': name,
                'texture': big_planet_textures[
                    int(random.uniform(0, len(big_planet_textures)))],
                'radius': Jupiter.radius * 2e-1 * scale_astro_radius * random.uniform(
                    .3, 1),
                'distance': next(pdist,
                                 random.uniform(a, b)) * scale_astro_dist,
                'mass': Uranus.mass * scale_astro_mass * random.uniform(.3, 3),
                'moons': [
                    moon_types[int(random.uniform(0, len(moon_types)))](name, j)
                    for j in xrange(min(4, int(random.expovariate(.5))))]
            }
        ]
        star = {
            'name': "Star",
            'radius': Sol.radius * 2e-2 * scale_astro_radius,
            'mass': Sol.mass * scale_astro_mass,
            'texture': _tex_path + "2k/2k_sun.jpg",
            'planets': [planet_types[int(random.uniform(0, len(planet_types)))](
                planet_names.next(), i) for i in
                xrange(num_planets)],
        }

        return self._create_star_system(star)

    def _create_star_system(self, star):
        entities = {}  # type: Dict[Entity, dict]
        for star in [star]:
            star_x = Vec3D.zero()
            star['entity'] = self.create_star(
                entity=self._ew.create_entity(), name=star['name'], x=star_x,
                mass=star['mass'], radius=star['radius'],
                texture=star['texture'])
            entities[star['entity']] = star

            for planet in star.get('planets', {}):
                axis = Vec3D.up() + Vec3D.forward() * random.uniform(-1e-2,
                                                                     1e-2)
                planet.update(merge(planet, {
                    'name': planet['name'],
                    'entity': self._ew.create_entity(),
                    'barycenter': star['entity'],
                    'moon': False,
                    'axis': QuatD(1, axis.normalized()),
                    'theta': random.uniform(0, 2. * math.pi),
                    'semi_major': planet['distance'],
                    'semi_minor': planet['distance'],
                }))
                self.create_planet(**planet)
                entities[planet['entity']] = planet

                for moon in planet.get('moons', {}):
                    axis = Vec3D.up() + Vec3D.forward() * random.uniform(-1e-1,
                                                                         1e-1)
                    moon.update(merge(moon, {
                        'name': moon['name'],
                        'entity': self._ew.create_entity(),
                        'barycenter': planet['entity'],
                        'moon': True,
                        'axis': QuatD(1, axis.normalized()),
                        'theta': random.uniform(0, 2. * math.pi),
                        'semi_major': moon['distance'],
                        'semi_minor': moon['distance'],
                    }))
                    self.create_planet(**moon)
                    entities[moon['entity']] = moon

        tree = [
            (star, [
                (planet, [(moon, ()) for moon in planet['moons']])
                for planet in star['planets']
            ])
        ]
        return entities, tree

    def create_star(self, entity, name, x, mass, radius, texture):
        # type: (Entity, str, Vec3D, float, float, str) -> Entity
        """Creates a star."""
        body_np = self.load_rigidbody(
            entity=entity, name="{}({})".format(name, entity.guid),
            shape=BulletSphereShape(radius), mass=mass, kinematic=True,
            static=True)
        body_np.setLightOff(True)
        texture = self._loader.loadTexture(texture)
        mat = Material()
        mat.set_emission((1, 1, 1, 1))

        model = self._load_planet_model(radius)
        model.reparent_to(body_np)
        model.set_texture(texture, 1)
        model.set_material(mat)
        # model.set_color(Vec4F(255, 230, 230, 1.0))

        # Arranges lighting.

        sunlight = PointLight('sunlight')
        sunlight.setColor((1.0, 0.8, 0.8, 1))
        sunlight_np = body_np.attachNewNode(sunlight)
        sunlight.set_attenuation((1, 0, 1.5e-10 * config.scale_astro_dist))

        # TODO (@Sean): We may want to arrange 2 spotlights with 180 deg
        # lenses, to cast shadows.
        # Then we should remove the point light.

        # slight = Spotlight('slight')
        # slight.setColor(VBase4(1, 1, 1, 1))
        # lens = PerspectiveLens()
        # lens.set_fov(180)
        # slight.setLens(lens)
        # for x in [1, -1]:
        #     slnp = self._game.world_np.attachNewNode(slight)  # type: NodePath
        #     slnp.setPos(0, 3, 0)
        #     slnp.lookAt(slget_pos() + Vec3(x, 0, 0))
        #     self._game.base.render.setLight(slnp)

        # # Use a 512x512 resolution shadow map
        # light.setShadowCaster(True, 512, 512)
        # # Enable the shader generator for the receiving nodes
        # render.setShaderAuto()

        self._add_components(entity, [
            Star(name, radius, sunlight_np),
            BlocksSensor(),
            Transform(x),
            Body(body_np),
        ])
        return entity

    # noinspection PyUnusedLocal
    def create_planet(self, entity, name, barycenter, mass, radius, moon=False,
                      axis=None, theta=0, period=1. / 10, semi_major=None,
                      semi_minor=None, texture=None, *args, **kwargs):
        """Creates an orbiting planet.

        Additional ``args`` and ``kwargs`` are ignored.  These are included so that
        passing additional parameters don't raise errors.

        Args:
            entity (Entity): Planet/moon entity.
            name (str): Name of the planet/moon.
            barycenter (Entity): Entity this planet/moon orbits around.
            mass (float): Mass of planet/moon.
            radius (float): Radius of planet/moon.
            moon (bool): ``True`` iff this is a planet's moon.
            axis (QuatD): Orbit axis.
                This is not currently in use.
            theta: Orbit angle in radians, relative to ``axis``.
                This is not currently in use.
            period (float): Orbital period.
                This is not currently in use.
            semi_major (float): Magnitude of semi-major axis.
                This is not currently in use.
            semi_minor (float): Magnitude of semi-minor axis.
                This is not currently in use.
            texture (str): Path to texture.

        Returns:
            Entity
        """
        np = self.load_rigidbody(
            entity=entity, name="{}({})".format(name, entity.guid),
            shape=BulletSphereShape(radius), mass=mass, kinematic=True,
            static=True)
        model = self._load_planet_model(radius)
        model.reparent_to(np)

        tex = self._loader.loadTexture(texture)
        model.set_texture(tex, 0)

        if barycenter:
            if semi_major and semi_minor:
                q1 = quatd()
                q1.set_from_axis_angle_rad(theta, Vec3D.up())
                q = axis * q1
                x = q.xform(Vec3D(0, semi_major, 0))
                x += self._cm.Transform(barycenter).x
            else:
                raise ValueError('semi_major or semi_minor not set')
        else:
            x = Vec3D.zero()

        # if semi_major and semi_minor:
        #    self._add_component(entity, KineticOrbiter(
        #        barycenter, axis, theta, period, semi_major, semi_minor))

        self._add_components(entity, [
            Planet(name, radius, texture, moon),
            Colonisable(),
            BlocksSensor(),
            Transform(x),
            Body(np),
        ])
        return entity

    def create_observer(self, entity, x):
        # type: (Entity, Vec3D) -> Entity
        """Creates a non-player observer."""
        scale = config.Observer.scale
        np = self.load_rigidbody(
            entity=entity, mass=config.Observer.mass,
            shape=BulletSphereShape(1 / 0.2 * scale),
            name="{}({})".format(Observer.__name__, entity.guid))
        self.disable_collisions(entity, np)
        np.node().set_angular_damping(8e-1)

        model = self._load_model('models/camera.egg', UNIT_SCALE * scale)
        model.set_name('observer_model')
        model.reparent_to(np)
        mat = Material()
        mat.set_emission(Vec4(.1, .1, .1, 1))
        model.set_material(mat)

        self._add_components(entity, [
            Observer(),
            Avatar(),
            Transform(x),
            Body(np),
            FuelTank(float('inf')),
            Circuit(),
            Generator(config.Generator.voltage),
            AttitudeThrusters(config.Observer.torque),
            LinearThrusters(config.Observer.thrust),
            WarpDrive(resistance=
                      config.Generator.voltage / config.WarpDrive.min_current),
        ])
        return entity

    def create_ship(self, entity, team_id, x):
        # type: (Entity, int, Vec3D) -> Entity
        """Creates a ship."""
        # The real world battleship USS Missouri weighs 4.5e7 kg and is 175 m
        # in length.
        scale = config.Ship.scale
        shape = BulletSphereShape(scale)
        model_path = 'assets/models/scifi_fighter/scifi_fighter.bam'

        np = self.load_rigidbody(
            entity=entity, mass=config.Ship.mass, shape=shape,
            name="{}({})".format(
                Ship.__name__, entity.guid, team_id, Team.__name__))

        model = self._load_model(model_path, UNIT_SCALE * scale)
        model.reparent_to(np)
        for mat in model.find_all_materials():
            mat.set_emission((.3, .3, .3, 1))  # Brightens the model in shadow.

        # Models like scifi_fighter require this in order not to be transparent.
        model.set_two_sided(True)

        self._add_components(entity, [
            Ship(),
            Team(team_id),
            Avatar(),
            Transform(x),
            Body(np),
            Hitpoints(config.Ship.hp),
            FuelTank(config.Ship.fuel_capacity, config.Ship.initial_fuel),
            Circuit(),
            Generator(config.Generator.voltage),
            LifeSupport(),
            Sensor(self.load_ghost(config.Sensor.range)),
            AttitudeThrusters(config.Ship.torque),
            LinearThrusters(config.Ship.thrust),
            WarpDrive(),
            Shields(config.Ship.shield_effectiveness),
            TorpedoGun(),
            LaserGun(),
        ])
        return entity

    def create_station(self, entity, team_id, home, x):
        # type: (Entity, int, Entity, Vec3D) -> Entity
        """Creates a space station."""
        scale = config.Station.scale
        shape = BulletSphereShape(scale / 2)
        model_path = 'assets/models/drift/drift.bam'

        np = self.load_rigidbody(
            entity=entity, mass=config.Station.mass, shape=shape,
            name="{}({})".format(
                Station.__name__, entity.guid, team_id, Team.__name__))
        # To mitigate ships ramming into them and disturbing their orbit,
        # stations don't bounce off obstacles.
        np.node().set_restitution(0)

        model = self._load_model(model_path, UNIT_SCALE * scale)
        model.set_name('model')
        model.reparent_to(np)
        for mat in model.find_all_materials():
            # Darkens the model (useful when model has no texture).
            mat.set_ambient((.1, .1, .1, 1))
            # Brightens the model in shadow.
            mat.set_emission((.1, .1, .1, 1))

        # Models like scifi_fighter require this in order not to be transparent.
        model.set_two_sided(True)

        # It is necessary that stations have some kind of linear thrusters or
        # they won't be able to correct their orbits which decay over time due
        # to numerical inaccuracy in the game.
        self._add_components(entity, [
            Station(home),
            Team(team_id),
            Avatar(),
            Transform(x),
            Body(np),
            Hitpoints(config.Station.hp),
            FuelTank(config.Station.fuel_capacity, config.Station.initial_fuel),
            Circuit(),
            Generator(config.Generator.voltage),
            LifeSupport(),
            Sensor(self.load_ghost(config.Sensor.range)),
            LinearThrusters(config.Station.thrust),
            AttitudeThrusters(config.Station.torque),
            Shields(config.Station.shield_effectiveness),
            TorpedoGun(),
            LaserGun(),
        ])
        return entity

    def create_torpedo(self, entity, fired_by, x, boost):
        # type: (Entity, Entity, Vec3D, float) -> Entity
        """Creates a torpedo."""
        team = self._cm.Team(fired_by)
        if team:
            team_id = team.id
        else:
            team_id = -1

        # The real-world Tomahawk missile weighs 1300 kg and is 6 m in length.
        scale = config.Torpedo.scale
        np = self.load_rigidbody(
            entity=entity, mass=config.Torpedo.mass,
            shape=BulletSphereShape(scale), name="{}({})".format(
                Torpedo.__name__, entity.guid, team_id, Team.__name__))
        self.disable_bounciness(entity, np)
        model = self._load_model(
            'assets/models/hellfire/hellfire.bam', UNIT_SCALE * scale)
        model.set_h(-90)
        model.reparent_to(np)
        model.set_name('model')
        for mat in model.find_all_materials():
            mat.set_emission((.6, .6, .6, 1))  # Brightens the model in shadow.

        self._add_components(entity, [
            Torpedo(),
            SelfDestruct(config.Torpedo.time_to_live),
            Projectile(fired_by, config.Torpedo.damage),
            Team(team_id),
            Avatar(),
            Transform(x),
            Body(np),
            Hitpoints(config.Torpedo.hp),
            FuelTank(config.Torpedo.fuel_capacity * boost),
            Circuit(),
            Generator(config.Generator.voltage),
            AttitudeThrusters(config.Torpedo.torque),
            LinearThrusters(config.Torpedo.thrust),
        ])
        return entity

    def create_laser(self, entity, fired_by, x, boost):
        # type: (Entity, Entity, Vec3D, float) -> Entity
        """Creates a laser."""
        team = self._cm.Team(fired_by)
        if team:
            team_id = team.id
        else:
            team_id = -1

        scale = config.Laser.scale
        np = self.load_rigidbody(
            entity=entity, mass=config.Laser.mass,
            shape=BulletSphereShape(scale), name="{}({})".format(
                Laser.__name__, entity.guid, team_id, Team.__name__))
        self.disable_bounciness(entity, np)

        model = self._load_model('misc/sphere.egg',
                                 Vec3(1.0, 100.0, 1.0) * scale,
                                 Vec3(0, 1.55, 0))
        model.reparent_to(np)
        model.set_color(Vec4(1, .3, .3, 1))
        model.setLightOff(True)
        # model.set_transparency(TransparencyAttrib.MAlpha)
        # model.setAlphaScale(0.5)

        self._add_components(entity, [
            Laser(),
            SelfDestruct(config.Laser.time_to_live),
            Projectile(fired_by, config.Laser.damage * boost),
            Team(team_id),
            Transform(x),
            Body(np),
        ])

        return entity

    def create_explosion(self, entity, x, radius):
        """Creates an explosion at ``x``."""
        # Polygon plane (4 sided square) to put an animated sprite on.
        plane_model = self._game.base.loader.loadModel('assets/models/plane')
        plane_model.set_name('model')
        plane_model.setLightOff(True)
        plane_model.setTransparency(True)
        explosion_tex = self.load_texture_movie(
            51, config.Display.tex_path + 'explosion/explosion', 'png', padding=4)

        np = self.load_rigidbody(
            entity=entity, mass=config.scale_mass,
            shape=BulletSphereShape(radius),
            name="{}({})".format(Explosion.__name__, entity.guid))
        self.disable_collisions(entity, np)

        model = plane_model.copy_to(np)
        model.set_scale(radius)
        model.node().setEffect(BillboardEffect.makePointEye())

        self._add_components(entity, [
            Explosion(radius),
            Transform(x),
            Body(np)
        ])

        def on_complete(): self._game.delete(entity)

        task = self._game.base.taskMgr.add(
            self._texture_movie, "explosion_animation")
        task.finite = True
        task.fps = 30
        task.obj = model
        task.on_complete = on_complete
        task.textures = explosion_tex

    @staticmethod
    def _texture_movie(task):
        """Returns a task to animate a texture."""
        current_frame = int(task.time * task.fps)
        if task.finite and current_frame >= len(task.textures):
            if task.on_complete:
                task.on_complete()
            return Task.done
        task.obj.setTexture(
            task.textures[current_frame % len(task.textures)], 1)
        return Task.cont

    def load_texture_movie(self, frames, name, suffix, padding=1):
        """Returns an array of textures for an animation."""
        return [self._game.base.loader.loadTexture(
            (name + "%0" + str(padding) + "d." + suffix) % i)
            for i in xrange(frames)]

    def set_in_orbit(self, satellite, barycenter, axis):
        # type: (Entity, Entity, Vec3D) -> ()
        """Sets the satellite entity in circular orbit around the barycenter
        entity.  This is only effective for dynamic (not kinematic)
        satellites."""
        # Sets the satellite in a stable circular orbit, relative to the
        # barycenter's own velocity.
        if not self._game.gravity_processor.gravity_enabled:
            return

        # center_pos = self._cm.Transform(barycenter)
        # center_body = self._cm.Body(barycenter)
        # satellite_pos = self._cm.Transform(satellite)
        satellite_body = self._cm.Body(satellite)

        # Rather err on the safe side due to numerical error, so overestimate.
        v = self._game.gravity_processor.orbital_velocity(barycenter, satellite,
                                                          axis) * 1.01

        # target_orb = self._cm.KineticOrbiter(barycenter)
        # if target_orb is not None:
        #     v += self._game.gravity_processor.orbital_velocity(
        #         target_orb.barycenter, barycenter)

        satellite_body.v = v

        # We set x, but the BubbleProcessor doesn't know about it unless we
        # call update_component().
        self._ew.update_component((satellite,), Transform)
        self._ew.update_component((satellite,), Body)

    def fire_projectile(self, entity, projectile, launch_speed, direction):
        # type: (Entity, float, Vec3D) -> ()
        """Fires the projectile in the direction specified, relative to the
        avatar's direction."""
        with Components(self._game.entity_world, entity) as c:
            avatar_body = c.Body
            avatar_transform = c.Transform

        # We don't take account of the avatar's angular velocity when computing
        # the projectile's direction.
        forward = avatar_transform.get_relative_vector(direction.normalized())
        xc = avatar_transform.x
        # We must place the projectile at a distance greater than radius, in
        # case the avatar is rectangular.  Otherwise, when rotating at high
        # speed, the avatar might rotate into a fired projectile.
        xt = type_casting.cast_vec3d(forward * avatar_body.shape_radius * 2)
        vc = type_casting.cast_vec3d(avatar_body.v)
        vt = forward * launch_speed

        projectile_t = self._game.component_mappers.Transform(projectile)
        projectile_t.x = xc + xt
        projectile_t.q = look_at(vt, Vec3.up())
        self._game.entity_world.update_component((projectile,), Transform)

        projectile_body = self._game.component_mappers.Body(projectile)
        projectile_body.v = vc + vt

        # Newton's 1st law: applies equal and opposite force to the avatar.
        # This arbitrarily applies an impulse over 1e-0 seconds.
        # f = projectile_gun.launch_speed * projectile_body.mass * constants.scale_force
        # avatar_body.apply_central_impulse(-forward * f * 1e-0)

    def _load_model(self, path, scale=1.0, offset=Vec3.zero()):
        # type: (str, float) -> NodePath
        """Loads the 3D model. For use by a higher-level load function."""
        model = self._loader.loadModel(path)  # type: NodePath
        model.set_pos(offset)
        model.set_scale(scale)
        model.set_name('model')  # Required by team_processor.
        return model

    def _load_planet_model(self, radius):
        # type: (float) -> NodePath
        """Loads a spherical planet model. For use by a higher-level load
        function."""
        # This assumes the model has radius 1. 
        # (planet_sphere.egg has radius 1.00000083447)
        return self._load_model(
            'assets/models/planet_sphere.egg', Vec3(1., 1., 1.) * radius)

    @staticmethod
    def load_rigidbody(entity, name, shape, mass, kinematic=False,
                       static=False):
        # type: (Entity, str, BulletShape, float, bool, bool) -> NodePath
        """Creates a NodePath to a new BulletRigidBodyNode for the entity.
        For use by a higher-level load function."""
        np = NodePath(BulletRigidBodyNode(name))
        # Intended for using with collision detection.
        np.set_python_tag(ENTITY_TAG, entity)
        # Group 1 can be picked up by scanners and crashed into.
        np.setCollideMask(BitMask32(1))
        b = np.node()  # type: BulletRigidBodyNode
        b.setMass(mass)
        b.addShape(shape)
        b.set_linear_damping(0)
        b.set_angular_damping(0)
        b.setDeactivationEnabled(False)
        b.set_kinematic(kinematic)
        b.set_static(static)
        b.notifyCollisions(True)
        return np

    @staticmethod
    def load_ghost(radius, mask=BitMask32.allOn()):
        """Adds a collision sphere."""
        np = NodePath(BulletGhostNode('scan'))
        b = np.node()  # type: BulletGhostNode
        b.addShape(BulletSphereShape(radius))
        b.setDeactivationEnabled(False)
        b.set_static(True)
        # np.setCollideMask(mask)
        return np

    @staticmethod
    def disable_collisions(e, np):
        """Disables collisions for explosions and observers."""
        np.setCollideMask(BitMask32(2))
        np.node().setIntoCollideMask(BitMask32(2))
        np.node().notifyCollisions(False)

        # The above doesn't prevent bounce-back.
        # Now this works, but also disables mouse picking using Bullet.
        np.node().set_collision_response(False)

    @staticmethod
    def disable_bounciness(e, np):
        """Disables bounciness, which is useful to prevent projectiles bouncing
        back instead of hitting targets."""
        np.node().set_restitution(0)
        np.node().set_collision_response(False)


__doc__ = EntityFactory.__doc__
