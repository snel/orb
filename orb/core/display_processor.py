import logging
import math

import typing
from direct.gui.OnscreenImage import OnscreenImage
from six import viewitems
from transitions import Machine
from typing import Set

from orb.components import (
    Bubble, Star, Transform, Team, Body)
from orb.components.component_mappers import ComponentMappers
from orb.core import config
from orb.esper import Entity, EntityWorld, Processor
from orb.util import type_casting
from orb.util.log_config import add_stdout
from orb.util.dic import merge
from orb.util.formatter import Formatter as Fmt
from orb.util.grid import ThreeAxisGrid
from pypanda3d.bullet import BulletDebugNode
from pypanda3d.core import (
    Lens, NodePath, TextNode, Vec3, Vec4, Texture, AmbientLight,
    WindowProperties, GraphicsWindow, Quat, Point2, Point3, Vec3D,
    TransparencyAttrib)
from pypanda3d.direct.gui import OnscreenText

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)
add_stdout(log)

clr_components = {Team, Body}
"""Component types required to render team colours."""

TOP_LEFT_LABELS = (
    ('name',),
    ('hp', 'shields'),
    ('fuel',),
    ('thrust',),
    ('v', 'a'),
    ('w', 'roll'),
    ('SELF-DESTRUCT IN',)
)
"""Describes which data to display for the avatar in the top-left corner of 
the screen. """

TOP_RIGHT_LABELS = (
    ('name',),
    ('hp', 'shields'),
    # ('fuel',),
    # ('thrust',),
    # ('v', 'a'),
    # ('w', 'roll'),
    ('r', 'theta', 'phi'),
    ('progress',),
)
"""Describes which data to display for the target in the top-right corner of 
the screen. """

acceleration_cache = {}


# TODO (@Sean): Auto-reset view after dying -> respawning.
class DisplayProcessor(Processor):
    """Manages all rendering and display. Rendering is performed relative to
    the selected ``Bubble``, see
    :class:`orb.core.bubble_processor.BubbleProcessor`."""

    def __init__(self, game, fullscreen):
        # type: (Game, bool) -> ()
        super(DisplayProcessor, self).__init__()
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers
        self._ew = game.entity_world  # type: EntityWorld

        self._fullscreen = game.base.win.is_fullscreen()
        if bool(fullscreen) != bool(self._fullscreen):
            self.toggle_fullscreen()

        self._stare_at_target = False
        self._first_person = False

        self.target = None  # type: Entity
        self.fov = None  # type: float
        self._path = None  # type: NodePath

        self._bubble = None  # type: Bubble
        self._display_np = NodePath('display_np')
        """Root node of nodes managed by DisplayProcessor."""
        game.base.render.attachNewNode(self._display_np.node())

        self._added_stars = set()  # type: Set[Entity]
        self._pending_stars = set()  # type: Set[Entity]

        self.cam = NodePath('camera')
        game.base.camera.reparentTo(self.cam)

        game.base.setFrameRateMeter(True)
        game.base.setBackgroundColor(0.0, 0.0, 0.0, 1)

        # These lines are supposed to fix aspect-ratio issues when resizing
        # or maximizing the window.  The HackFu build is still broken in this
        # regard because we don't get any window events.  This build seems to be
        # missing the X11 library, as the warnings printed to stdout shows:
        #   $ Xlib:  extension "XFree86-DGA" missing on display
        #   $ ":0". :display:x11display(warning): Couldn't open input method.
        self._prevWindowProperties = game.base.win.getProperties()  # type: WindowProperties
        game.base.accept('window-event', self._window_event)
        # This appears to take 2 arguments.  I did get it fire randomly once.
        # game.base.accept('aspectRatioChanged', self._window_event)

        self._time_since_update = 0

        self._font = game.base.loader.loadFont(config.Display.font) \
            if config.Display.font else None
        self._text = {}
        self._labels_top_left = [
            self._create_label('', i, 'top-left')
            for i in xrange(len(TOP_LEFT_LABELS))]
        self._labels_top_right = [
            self._create_label('', i, 'top-right')
            for i in xrange(len(TOP_RIGHT_LABELS))]

        self._center_label = self._create_label('', 0, 'center', .2)

        # Adds a skybox.
        sky_tex = game.base.loader.loadTexture(config.Display.sky_tex)  # type: Texture
        self._skybox = game.base.loader.loadModel(
            'assets/models/solar_sky_sphere.egg')  # type: NodePath
        self._skybox.setTexture(sky_tex, 1)
        self._skybox.set_color_scale(
            Vec4(.4, .4, .4, 1))  # Darkens the texture.
        self._skybox.setScale(1e4)
        self._skybox.set_compass()
        self._skybox.set_two_sided(True)
        self._skybox.set_bin("background", 0)
        self._skybox.set_light_off()
        self._skybox.set_depth_write(False)

        self._skybox.reparentTo(game.base.cam)

        # Adds HUD icons.
        self._h_icon = self.create_icon('assets/textures/icons/crosshair.png',
                                        15. / 200)
        self._v_icon = self.create_icon('assets/textures/icons/reticule.png',
                                        5. / 140)
        # self._w_icon = self.create_icon('assets/textures/icons/crosshair2.png',
        #                                 8. / 200)
        self._t_icon = self.create_icon(
            'assets/textures/icons/selectionbox.png', 8. / 200)
        self._object_icon_root = NodePath('object icons')
        self._object_icon_root.reparent_to(game.base.aspect2d)
        self._object_icons = {}

        # Adds a grid.
        self._grid = ThreeAxisGrid()
        self._grid.XYPlaneShow = 1
        self._grid.XZPlaneShow = 0
        self._grid.YZPlaneShow = 0
        self._grid_np = self._grid.create()
        self._grid_np.set_scale(config.scale_dist * 2)
        self._grid_np.setLightOff(True)
        # self._grid_np.reparentTo(self._display_np)  # Grid disabled in prod.
        self._grid_np.hide()

        # Sets up ambient light.
        alight = AmbientLight('ambient_light')
        alight.setColor(Vec4(config.Display.ambient_light,
                             config.Display.ambient_light,
                             config.Display.ambient_light, 1))
        alight_np = self._display_np.attachNewNode(alight)
        self._game.base.render.clearLight()
        self._game.base.render.setLight(alight_np)

        # Sets up debug UI.
        self._debug_np = NodePath(BulletDebugNode('debug'))  # type: NodePath
        self._debug_np.setLightOff(True)
        self._debug_np.hide()
        self._debug_np.reparent_to(self._display_np)
        self._debug_np.node().showWireframe(True)
        self._debug_np.node().showConstraints(False)
        self._debug_np.node().showBoundingBoxes(False)
        self._debug_np.node().showNormals(False)
        # self._debug_np.showTightBounds()  # Displays "world bounds" box.
        # self._debug_np.showBounds()  # Displays "world bounds" sphere.
        # self._debug_np.show()

        game.base.setBackgroundColor(0.0, 0.0, 0.0, 1)

        self.display_fsm = DisplayMachine(game)
        self.display_fsm.to_default()

    def _create_label(self, text, i, corner='top-left', scale=.05):
        # type: (str, int, str) -> OnscreenText
        """Creates a text field."""
        if corner == 'top-left':
            x = (-1.3, 0.9 - .06 * i)
            align = TextNode.ALeft
        elif corner == 'top-right':
            x = (1.3, 0.9 - .06 * i)
            align = TextNode.ARight
        elif corner == 'center':
            x = (0, 0)
            align = TextNode.ACenter
        else:
            raise ValueError("invalid corner")

        return OnscreenText(
            text=text, pos=x, fg=config.Display.fg, bg=config.Display.bg,
            align=align, scale=scale, font=self._font)

    # TODO (@Sean): update to use VersionedRef.
    @property
    def controlled(self):
        return self._game.avatar.value

    def create_icon(self, image_path, scale):
        """Creates a new on-screen icon using the given texture and scaling."""
        model = OnscreenImage(image=image_path, pos=(0., 0., 0.), scale=scale)
        model.set_name('hud_icon')
        model.set_transparency(TransparencyAttrib.MAlpha)
        model.set_color(config.Display.fg)
        model.reparent_to(self._game.base.aspect2d)
        model.is_front = True
        return model

    def toggle_object_icons(self):
        # type: () -> ()
        """Toggles display of object icons."""
        status = self._object_icon_root.is_hidden()
        if status:
            self._object_icon_root.show()
        else:
            self._object_icon_root.hide()

        log.info("Object icons {}.".format(Fmt.on_status(status)))

    def toggle_fullscreen(self):
        # type: () -> ()
        """Toggles halt mode."""
        self._fullscreen = not self._fullscreen
        props = WindowProperties()
        # props.setSize(w, h)
        props.setFullscreen(self._fullscreen)
        self._game.base.win.request_properties(props)
        # self._game.base.adjustWindowAspectRatio(base.getAspectRatio())
        log.info("Fullscreen {}.".format(Fmt.on_status(self._fullscreen)))

    def toggle_view(self):
        # type: () -> ()
        """Toggles first/third person view mode."""
        self._first_person = not self._first_person
        self.reset_camera()
        log.info("First person {}.".format(Fmt.on_status(self._first_person)))

    def toggle_stare_at_target(self):
        # type: () -> ()
        """Toggles camera stare-at-target mode."""
        self._stare_at_target = not self._stare_at_target
        log.info("Stare-at-target {}.".format(
            Fmt.on_status(self._stare_at_target)))

    def compute2d_position(self, np, point=Point3(0, 0, 0), w=1.):
        """Computes a 3-d point, relative to the indicated node, into a 2-d
        point as seen by the camera.

        The ``w`` parameter isn't currently in use as homogeneous coordinates
        aren't required to display icons correctly for our use case.
        """
        cam = self._game.base.cam
        lens = cam.node().get_lens()

        # Convert the point into the camera's coordinate space.
        p3d = cam.getRelativePoint(np, point)

        # Projects the 3d point to 2d.
        # If project() returns false, it means the point was behind the lens.
        p2d = Point2()
        in_front = lens.project(p3d, p2d)
        # p2d, in_front = project(lens, Vec4(p3d[0], p3d[1], p3d[2], w))

        p2d = (p2d[0] * self._game.base.getAspectRatio(), p2d[1])

        if in_front:
            return p2d, True

        # Clamps position inside screen space.
        x0 = -1. * self._game.base.getAspectRatio()
        x1 = 1. * self._game.base.getAspectRatio()
        y0 = -1.
        y1 = 1.
        p2d = (min(x1, max(x0, p2d[0])), min(y1, max(y0, p2d[1])))
        return p2d, False

    def _update_icon(self, icon, u, w=1.):
        # type: (NodePath, Vec3, float) -> ()
        """Updates the colour of icons depending on whether they point at
        things in front of, or behind the camera."""
        y, front = self.compute2d_position(self._game.base.render, u, w)
        icon.set_pos(y[0], 0, y[1])
        if front != icon.is_front:
            icon.is_front = front
            # TODO: this is incompatible with "team colouring"
            if not front:
                icon.set_color(config.Display.fg_behind)
            else:
                icon.set_color(config.Display.fg)

    @staticmethod
    def quat_uv(u, v):
        # type: (Vec3, Vec3) -> Quat
        """Returns a quaternion representing rotation from ``u`` to ``v``.

        See: http://stackoverflow.com/questions/1171849/finding-quaternion-representing-the-rotation-from-one-vector-to-another.
        """
        k_cos_theta = u.dot(v)
        k = math.sqrt(u.length_squared() * v.length_squared())

        # if k_cos_theta / k == -1:
        # # 180 degree rotation around any orthogonal vector
        #    return Quaternion(0, normalized(orthogonal(u)));

        return Quat(k_cos_theta + k, u.cross(v))

    def _process(self, dt):
        # type: (float) -> ()
        """Updates the GUI. Called from :class:`orb.esper.EntityWorld`."""
        if self._bubble is None:
            return

        if (self._stare_at_target and self.target and
                    self.target in self._bubble.nps):
            x = self._bubble.nps[self.target].get_pos()
            self.cam.look_at(self._game.base.render, x)

        self._update_icons()
        # self.update_velocity_vector()

        # The rest of this need not be updated every frame. In fact, it would
        # make the text hard to read if it did.
        self._time_since_update += dt
        if self._time_since_update <= config.Display.hud_refresh_rate:
            return

        self._update_text()
        self._time_since_update = 0.

    def _update_icons(self):
        c = self.controlled
        if self._bubble is None or c is None:
            return

        c_transform = self._cm.Transform(c)
        if not c_transform:
            return

        c_body = self._cm.Body(c)
        if not c_body:
            return

        # We place "directional" points arbitrarily far away from the avatar.
        # This may seem like a poor man's imitation of homogeneous coordinates,
        # but we actually do use homogeneous coordinates as well. This is a
        # hack to avoid confusion when viewing from 3rd person.
        d = 1e30

        hd = c_transform.get_relative_vector(Vec3D.forward() * d)
        h = type_casting.cast_vec3(hd)
        self._update_icon(self._h_icon, h, 0.)

        v = c_body.v
        if self.target and self.target != c:
            t_body = self._cm.Body(self.target)
            if t_body:
                v = v - t_body.v
        v = v.normalized() * d
        self._update_icon(self._v_icon, v, 0.)

        tgt = self._bubble.nps.get(self.target)
        if tgt:
            u = tgt.get_pos()
            self._update_icon(self._t_icon, u)
            if self.target != c:
                self._t_icon.show()
            else:
                self._t_icon.hide()
        else:
            self._t_icon.hide()

        for e, icon in viewitems(self._object_icons):
            tgt = self._bubble.nps.get(e)
            if tgt:
                u = tgt.get_pos()
                self._update_icon(icon, u)
                if e != self.target and e != c and icon.is_front:
                    icon.show()
                else:
                    icon.hide()

    def _team_colour(self, icon, e):
        if not config.Display.hud_colours:
            return

        team1 = self._cm.Team(e)
        if team1:
            if config.Display.hud_colours == "friend-or-foe":
                team0 = self._cm.Team(self.controlled)
                if not team0 or team1.id == team0.id:
                    x = config.Display.friend_colour
                else:
                    x = config.Display.foe_colour
            else:
                x = config.Display.team_colours[team1.id]

            y = 1. / 255
            x = (x[0] * y, x[1] * y, x[2] * y, 1)
        else:
            x = config.Display.fg

        icon.set_color(x)

    def _update_text(self):
        c = self.controlled
        t = self.target

        self._text = {
            'avatar': self._details(c, t) if c else {},
            'target': merge(
                self._details(t, c),
                self._spherical(c, t)) if t else {},
        }

        self._update_labels(TOP_LEFT_LABELS, self._labels_top_left,
                            self._text['avatar'], rename={'name': 'avatar'})
        self._update_labels(TOP_RIGHT_LABELS, self._labels_top_right,
                            self._text['target'], rename={'name': 'target'})

    def _details(self, e, relative_to=None):
        """Returns a dictionary of details about the entity."""
        body = self._cm.Body(e)
        team = self._cm.Team(e)
        generator = self._cm.Generator(e)
        hitpoints = self._cm.Hitpoints(e)
        shields = self._cm.Shields(e)
        fuel_tank = self._cm.FuelTank(e)
        linear_thrusters = self._cm.LinearThrusters(e)
        construction = self._cm.Construction(e)
        self_destruct = self._cm.SelfDestruct(e)

        if body:
            v = body.v
            if e != relative_to:
                body1 = self._cm.Body(relative_to)
                if body1:
                    v -= body1.v

            # Computes acceleration. bullet.get_total_force() always returns 0,
            # so we can't use it with F=ma.
            prev_v = acceleration_cache.get(e, 0)
            acceleration_cache[e] = v
            a = (v - prev_v) / self._time_since_update

        return {
            'name': self._name(e) if body else None,
            'team': team.id if team else None,
            'hp': Fmt.frac(hitpoints.hp) if hitpoints else None,
            'shields': Fmt.frac(
                shields.capacitor.charge /
                (shields.capacitor.capacitance * generator.voltage))
            if shields and generator else None,
            'fuel': Fmt.frac(fuel_tank.fuel / fuel_tank.capacity)
            # 'fuel': "{:E}".format(fuel_tank.fuel / fuel_tank.capacity)  # TODO: Replace in prod
            if fuel_tank else None,
            'thrust': Fmt.thrust(linear_thrusters.engine.throttle, 1)
            if linear_thrusters else None,
            'v': Fmt.v(v.length()) if body else None,
            'w': Fmt.v(body.w.length()) if body else None,
            'roll': Fmt.w(-body.w[1]) if body else None,
            'a': Fmt.a(a.length()) if body else None,
            'progress': Fmt.frac(construction.progress) if construction else None,
            'SELF-DESTRUCT IN': Fmt.time(self_destruct.countdown.time_left) if self_destruct else None,
        }

    def _update_labels(self, layout, labels, text, rename=None):
        if rename is None:
            rename = {}
        for i, item in enumerate(layout):
            labels[i].node().setText(
                ', '.join(
                    "{}={}".format(rename.get(j, j), text[j]) for j in item if
                    j in text and text[j] is not None))

    def _name(self, entity):
        np = self._bubble.nps[entity]
        team = self._cm.Team(entity)
        s = "Team({})".format(team.id) if team else ""
        return ', '.join((np.get_name(), s))

    # TODO (@Sean): refactor to utils?
    def _spherical(self, base, target):
        """Computes spherical coordinates of ``target``, relative to the ``base``.

        Returns:
            A dict with keys for:
                'r': Distance to target, in meters,
                'theta': Relative horizontal angle.
                'phi': Relative vertical angle.
        """
        x = Vec3.zero()
        r = k = 0.
        c_transform = self._cm.Transform(base)
        t_transform = self._cm.Transform(target)
        if c_transform and t_transform:
            x = t_transform.x - c_transform.x
            x = c_transform.get_relative_vector(x)
            c_body = self._cm.Body(base)
            t_body = self._cm.Body(target)
            y = 0.
            if c_body:
                y += c_body.shape_radius

            if t_body:
                t_p = self._cm.Planet(target) or self._cm.Star(target)
                if t_p:
                    y += t_p.radius
                else:
                    y += t_body.shape_radius
            d = x.length()
            r = d - y
            if d:
                k = x.z / d
                k = max(-1., min(1., k))  # Just in case of numerical error.

        return {
            'r': Fmt.x(r),
            'theta': Fmt.angle2(math.atan2(x.x, x.y)),
            'phi': Fmt.angle2(.5 * math.pi - math.acos(k)),
        }

    def _updated(self, component_type, entities):
        if component_type is Team:
            for e in entities:
                if self.target:
                    self._team_colour(self._t_icon, self.target)

                    icon = self._object_icons.get(e)
                    if icon:
                        self._team_colour(icon, e)

    def _added(self, component_type, entities):
        # Applies team colours to the model.
        # TODO (@Sean): we don't have a correspnding update function yet.
        if config.Display.model_colours and component_type in clr_components:
            for e in entities:
                if self._cm.has_all(e, clr_components):
                    team = self._cm.Team(e)
                    np = self._cm.Body(e).np
                    model = np.find('model')
                    x = config.Display.team_colours[team.id]
                    y = 5. / 255
                    if self._cm.Station(e):
                        y *= .1
                    x = (x[0] * y, x[1] * y, x[2] * y, 1)
                    model.clear_color_scale()
                    model.set_color_scale(x)

        if (component_type is Transform or component_type is Body or
                    component_type is Star):
            for e in entities:
                if self._cm.has_all(e, (Transform, Body, Star)):
                    if self._bubble:
                        self._star_added(e)
                    else:
                        self._pending_stars.add(e)

                if self._cm.has_all(e, (Transform, Body)):
                    icon = self._object_icons[e] = self.create_icon(
                        'assets/textures/icons/circle.png', 4. / 200)
                    icon.reparent_to(self._object_icon_root)
                    icon.show()

                    if e == self.controlled:
                        self._center_label.node().setText('')

    # We must deselect the currently targeted entity, because if it's
    # destroyed, the reference is no longer valid.
    def _removed(self, component_type, entities):
        if component_type is Transform:
            for e in entities:
                if e == self.target:
                    self.target = None
                    break

        if component_type is Transform or component_type is Body:
            for e in entities:
                icon = self._object_icons.pop(e, None)
                if icon:
                    icon.remove_node()

                if self.controlled is None or e == self.controlled:
                    self._center_label.node().setText('DESTROYED')

        if component_type in (Transform, Body, Star):
            for e in entities:
                if self._cm.has_all(e, (Transform, Body, Star)):
                    star_np = self._bubble.nps.get(e)
                    if star_np:
                        light_np = star_np.find('sunlight')
                        self._game.base.render.setLightOff(light_np)
                        light_np.remove_node()

    def _star_added(self, e):
        # type: (Entity) -> ()
        """Sets up light effects when a star has been created."""
        star = self._cm.Star(e)
        star_np = self._bubble.nps[e]
        light_np = star.light_np.copy_to(star_np)
        self._game.base.render.setLight(light_np)

    def _window_event(self, win=None, *args, **kwargs):
        # type: (GraphicsWindow) -> ()
        """Fixes the aspect ratio if the window is maximised."""
        base = self._game.base
        if win != base.win:
            return  # This event isn't about our window.

        properties = win.getProperties()
        if properties != self._prevWindowProperties:
            self._prevWindowProperties = properties
            base.adjustWindowAspectRatio(base.getAspectRatio())

    def set_bubble(self, bubble):
        # type: (Bubble) -> ()
        """Sets the bubble to use as reference frame for display."""
        if self._bubble is not None:
            self._bubble.root.reparent_to(NodePath('empty'))
            self._bubble.bullet_world.clear_debug_node()
            self._cleanup_view()
        self._bubble = bubble

        # This ensures that old lights don't accumulate when switching bubbles.
        for e_star in self._added_stars:
            star_np = self._bubble.nps[e_star]
            light_np = star_np.get_child(0)
            light_np.remove_node()
        self._pending_stars = self._pending_stars.union(self._added_stars)
        self._added_stars.clear()

        bubble.root.reparent_to(self._game.base.render)
        bubble.bullet_world.setDebugNode(self._debug_np.node())

        # Loads any star lights that couldn't load until a bubble was set.
        for e_star in self._pending_stars:
            self._star_added(e_star)
        self._pending_stars.clear()

    def reset_camera(self, target_entity=None):
        # type: (Entity) -> ()
        """Resets the camera position to its default position at the controlled
        entity."""

        self._center_label.node().setText('')

        # Cam can be `None` if no window was started.
        if not self._game.base.camera or not self._bubble:
            return

        self._cleanup_view()
        if (target_entity == self.target or target_entity == self.controlled):
            self.target = None

        if target_entity is None:
            target_entity = self.controlled

        t_body = self._cm.Body(target_entity)
        if t_body is None:
            return

        t_np = self._bubble.nps[target_entity]

        # Adjusts the camera. Default Panda3D values are:
        #   nearFar: (0.1, 10000.0)
        #   fov: 40

        self.fov = 40
        cam = self._game.base.cam  # type: NodePath
        lens = cam.node().get_lens()  # type: Lens
        lens.set_fov(self.fov)

        # Attaches the camera to the target.
        self.cam.reparentTo(t_np)
        camera = self._game.base.camera

        if target_entity == self.controlled:
            self._reset_view(target_entity)
        else:
            # Places the camera behind the target.
            r = t_body.shape_radius
            camera.set_pos(Vec3(0, -3 * r, 0))
            camera.set_p(0)

        # Refreshes the clipping planes and skybox.
        self.zoom(1)

    def _reset_view(self, target_entity):
        """Resets the view to the currently selected mode: either first-person
        or third-person."""
        camera = self._game.base.camera
        cam = self._game.base.cam  # type: NodePath

        camera.set_quat(Quat.ident_quat())
        cam.set_quat(Quat.ident_quat())
        self.cam.set_quat(Quat.ident_quat())

        body = self._cm.Body(target_entity)
        if not body or not self._bubble:
            return

        if self._first_person:
            camera.set_pos(Vec3(0, 0, 0))
            camera.set_quat(Quat.ident_quat())
            # np.hide() doesn't have the intended effect.
            np = self._bubble.nps[target_entity]
            np.setAlphaScale(0)
        else:
            # Places the camera slightly above and behind the target ship.
            r = body.shape_radius
            camera.set_pos(Vec3(0, -r, .15 * r) * 4.)
            camera.set_p(-5)

    def _cleanup_view(self):
        # type: () -> ()
        """Performs cleanup before resetting the view to another object.
        In particular, this sets the opacity of the object to ``1`` in case
        we were in first-person mode."""
        # If we switch camera views, we must first reset the state of the old
        # target.
        if self._bubble is None:
            return

        np = self._bubble.nps.get(self.controlled)
        if np:
            np.setAlphaScale(1)  # Entering first-person mode sets this to 0.

    def set_target(self, target):
        # type: (Entity) -> bool
        """Sets the entity targeted by the player.
        Returns ``True`` iff it was a valid target."""
        if target is not None and not self._cm.Transform(target):
            return False
        if target == self.target:
            return False
        self.target = target
        return True

    def zoom(self, ratio):
        # type: (float) -> ()
        """Zooms in or out by the specified ratio."""
        camera = self._game.base.camera
        x = camera.get_pos()
        y = x + x * 2 * (ratio - 1)

        r = y.length_squared()
        r_min = config.scale_dist
        r_max = config.Display.r_max
        if r < r_min:
            y.normalize()
            r = r_min
        elif r > r_max * r_max:
            if not x.normalize():
                log.error("zoom error")
            y = x * r_max
            r = r_max

        r = math.sqrt(r)
        lens = self._game.base.cam.node().get_lens()  # type: Lens
        near = .1 * max(config.scale_dist, r)
        far = 5e-4 * min(r_max, r) * r_max / config.scale_dist

        lens.set_near_far(near, far)
        self._skybox.setScale(r)
        camera.set_pos(y)

    def warp(self, entity, warp_factor):
        # type: (Entity, float) -> ()
        """Applies special effect to the camera when warping. Only needs to be
        called once (i.e. not each frame)."""
        if entity != self.controlled:
            return

        cam = self._game.base.cam  # type: NodePath
        lens = cam.node().get_lens()  # type: Lens

        if warp_factor == 0:
            lens.set_fov(self.fov)
            return

        w = (1 - math.log(1e4 * config.scale_dist / warp_factor))
        fov = self.fov * w
        fov = min(max(fov, 10.), 170.)
        lens.set_fov(fov)

    def toggle_debug(self):
        # type: () -> ()
        """Toggles debug mode, which displays bounding boxes and a grid."""
        debug_np = self._debug_np

        if debug_np.isHidden():
            self._grid_np.show()
            debug_np.show()
            log.info("Debug mode ON.")
        else:
            self._grid_np.hide()
            debug_np.hide()
            log.info("Debug mode OFF.")

    # This function isn't currently in use, but demonstrates how to plot
    # lines in 3D space.
    # def update_velocity_vector(self):
    #     # type: () -> ()
    #     """Updates the 'velocity vector' line."""
    #     e = self.controlled
    #     if e is None or self._bubble is None:
    #         return
    #
    #     e_body = self._cm.Body(e)
    #     if not e_body:
    #         return
    #
    #     e_transform = self._cm.Transform(e)
    #     if not e_transform:
    #         return
    #
    #     if self._path:
    #         self._path.np.removeNode()
    #         self._path = None
    #
    #     t_body = self._cm.Body(self.target)
    #     if t_body:
    #         target_v = t_body.v
    #     else:
    #         target_v = Vec3(0, 0, 0)
    #
    #     v = e_body.v - target_v
    #     v = Transform.get_relative_vector_inv(v, e_transform)
    #     v = type_casting.cast_vec3f(v)
    #
    #     # noinspection PyUnusedLocal
    #     def field(dt, x1, args):
    #         return x1 + v * dt, None
    #
    #     e_np = self._bubble.nps[e]
    #     self._path = FieldPath(
    #         field=field, n=1, step=1e6 * config.scale_ships / 3e3)
    #     self._path.np.setLightOff(True)
    #     self._path.np.reparent_to(e_np)


class DisplayMachine(Machine):
    """State machine of display states."""

    def __init__(self, game):
        # type: (Game) -> ()
        self._game = game  # type: Game
        super(DisplayMachine, self).__init__(
            'self', states=['default', 'flight'])

    def on_enter_flight(self):
        # self._game.display_processor.set_bubble(self._game.input_processor.controlled)
        self._game.display_processor.reset_camera()
