import cPickle as pickle
import logging
import os
from datetime import datetime

from direct.task import Task
from six import viewitems, viewvalues

from orb.components import Body, Sensor
from orb.components.components import Components
from orb.core import config
from orb.core.game import Game
from pypanda3d.core import Vec3D

log = logging.getLogger(__file__)

# Creates the saved_games path if it does not exist.
autosave_path = 'saved_games'
path = os.path.abspath(os.path.dirname(autosave_path))
if not os.path.exists(path):
    os.makedirs(path)


class GameMaster(Game):
    """Game master instance that hosts a multi-player game."""

    def __init__(self, *args, **kwargs):
        # type: () -> ()
        Game.__init__(self, is_client=False, *args, **kwargs)

        self.time_compression = 1.0
        """Time compression setting. By default, one second of real time is one
        second of simulated time.  Higher values increase the simulation
        rate."""

        self.autosave = 2*60
        """Autosave interval, in seconds."""

        task = self.base.taskMgr.add(self._autosave, "saved_games")
        task.last_time = self.global_clock.get_long_time()

    def _dt(self):
        return self.global_clock.getDt() * self.time_compression

    def pause(self):
        """Pauses the game."""
        self.time_compression = 0

    def resume(self):
        """Resumes the game if paused."""
        self.time_compression = 1

    def _autosave(self, task):
        if not self.autosave:
            return Task.cont
        now = self.global_clock.get_long_time()
        if now >= task.last_time + self.autosave:
            try:
                log.info("autosaving...")
                self.save("{}/save_{}.dat".format(autosave_path, datetime.now()))
            except Exception as e:
                log.warn(e, exc_info=1)
            task.last_time = now

        return Task.cont

    def set_gravity(self, value):
        """Sets gravity on or off.  Be careful when setting this after a game
        has started."""
        self.gravity = value
        self.entity_factory.enable_orbits = value
        self.gravity_processor.gravity_enabled = value

    # def set_bubble(self, entity):
    #     """Sets the bubble to use as reference point for display."""
    #     if not self.display_processor:
    #         return
    #     self.display_processor.set_bubble(entity)

    # noinspection PyProtectedMember
    def load(self, filename='saved_games/save.dat'):
        """Loads a previously saved game.

        Connected clients will experience errors because things like planets
        are deleted on the server before being recreated.  Client sessions
        don't support adding new planets (yet). Clients are recommended to
        restart their client sessions after the game is reloaded.

        There are known performance implications when loading from a saved game
        multiple times.  This will show up on clients as "jittery" movement.
        Simply close the master program and restart, then reload the game.
        """
        w0 = self.entity_world
        ef = self.entity_factory
        with open(filename, 'r') as f:
            data = pickle.load(f)

        # Performs cleanup to avoid performance problems.

        self.base.render.clear()
        self.base.render2d.clear()

        # Continues loading the state.

        w1 = data['entity_world']
        physics = data['physics']
        self.team_processor.events = data['events']
        self.team_processor.scores = data['scores']
        self.construction_processor._stations_under_construction = data['stations_under_construction']
        # self.input_processor.controlled = data['controlled']

        for component_type, entities in viewitems(w0._components):
            entities = entities.copy()
            for processor in w0._processors:
                processor._removed(component_type, entities)

        w0._entities = {}
        w0._components = {}
        w0._next_entity_id = w1._next_entity_id

        def add_components(e, components):
            for c in components:
                if isinstance(c, Body):
                    body = w1._entities.get(e).get(Body)
                    body.np = c.np
                    body.v = physics[e][0]
                    body.w = physics[e][1]
                    # body.mass = physics[e][2]
                elif isinstance(c, Sensor):
                    sensor = w1._entities.get(e).get(Sensor)
                    sensor.ghost.np = c.ghost.np

        # Trickery to initialise components with NodePaths.
        tmp = ef._add_components  # Temporarily disables entity add callbacks.
        ef._add_components = add_components
        for e in w1._components.get(Body, ()):
            c = Components(w1, e)
            if c.Ship:
                ef.create_ship(e, 0, Vec3D.zero())
            elif c.Station:
                ef.create_station(e, c.Team.id, 0, Vec3D.zero())
            elif c.Torpedo:
                ef.create_torpedo(e, 0, Vec3D.zero(), 0)
            elif c.Laser:
                ef.create_laser(e, 0, Vec3D.zero(), 0)
            elif c.Star:
                star = c.Star
                ef.create_star(e, star.name, Vec3D.zero(), physics[e][2], star.radius,
                               texture="assets/textures/2k/2k_sun.jpg")
            elif c.Planet:
                planet = c.Planet
                ef.create_planet(e, planet.name, 0, physics[e][2], planet.radius, texture=planet.texture)
            elif c.Explosion:
                ef.create_explosion(e, Vec3D.zero(), c.Explosion.radius)
            elif c.Observer:
                ef.create_observer(e, Vec3D.zero())
            else:
                log.warn("{} is not a recognised type".format(e))
        ef._add_components = tmp

        for e, components in viewitems(w1._entities):
            for c in viewvalues(components):
                self.entity_world.add_component(e, c)

        self.input_processor.take_control(data['controlled'])
        log.info("Loaded game from {}.".format(filename))
        if config.pause_on_reload:
            self.pause()

    def save(self, filename='saved_games/save.dat'):
        """Saves the game."""
        path = os.path.abspath(os.path.dirname(filename))
        if not os.path.exists(path):
            os.makedirs(path)

        ew = self.entity_world
        with open(filename, 'w') as f:
            bodies = {e: ew.component_for_entity(e, Body) for e in ew.components.get(Body, ())}
            data = dict(
                entity_world=ew,
                physics={e: (c.v, c.w, c.mass) for e, c in viewitems(bodies)},
                events=self.team_processor.events,
                scores=self.team_processor.scores,
                stations_under_construction=
                    self.construction_processor._stations_under_construction,
                controlled=self.avatar.value,
            )
            pickle.dump(data, f, protocol=2)
        log.info("Saved to {}.".format(filename))
