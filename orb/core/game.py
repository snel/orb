import logging
import direct.stdpy.threading as threading
from time import sleep

from direct.showbase import ShowBase
from direct.task import Task

from orb.components.component_mappers import ComponentMappers
from orb.core import config
from orb.core.astro_processor import AstroProcessor
from orb.core.countdown_processor import CountdownProcessor
from orb.core.bubble_processor import BubbleProcessor
from orb.core.circuit_processor import CircuitProcessor
from orb.core.construction_processor import ConstructionProcessor
from orb.core.damage_processor import DamageProcessor
from orb.core.display_processor import DisplayProcessor
from orb.core.entity_factory import EntityFactory
from orb.core.gravity_processor import GravityProcessor
from orb.core.input_processor import InputProcessor
from orb.core.sensor_processor import SensorProcessor
from orb.network.master_processor import MasterProcessor
from orb.core.team_processor import TeamProcessor
from orb.core.thruster_processor import ThrusterProcessor
from orb.esper import Entity, EntityWorld
from orb.util.versioned import VersionedVal
from pypanda3d.core import ClockObject, loadPrcFileData

log = logging.getLogger(__file__)


class Game(object):
    """Orb game instance.

    Contains the game loop and references to various subsystems, e.g.
    Processors.  Also responsible for graphics display using Panda3d.
    """

    def __init__(self, is_client, headless, fullscreen, gravity, avatar=None,
                 *args, **kwargs):
        # type: (bool, bool, bool, bool) -> ()

        for p in config.panda_config:
            loadPrcFileData('', p)
        # messenger.toggleVerbose()

        self._running = False
        self._headless = headless
        if headless:
            # TODO (@Sean): Still possible to open a window later if we wish?
            loadPrcFileData('', 'window-type none')
            self.target_framerate = 1. / 30

        self.is_client = is_client
        """``True`` iff this game is a client session."""

        self.gravity = gravity
        """``True`` iff gravity is enabled."""

        self.avatar = avatar
        """Player's avatar entity."""
        if not isinstance(self.avatar, VersionedVal):
            self.avatar = VersionedVal(self.avatar)

        self.base = ShowBase.ShowBase()
        """Panda3D engine interface."""

        # print self.base.getAllAccepting()
        # self.base.ignoreAll()

        self.global_clock = ClockObject.getGlobalClock()  # type: ClockObject
        """Panda3D clock, synchronized with the low-level engine."""

        self.entity_world = EntityWorld()
        """Entity database."""

        self.component_mappers = ComponentMappers(self.entity_world)
        """Provides access to component mappers."""

        self.entity_factory = EntityFactory(self)
        """Entity factory."""

        self.lock = threading.Lock()
        """Lock for synchronizing actions with the main thread."""

        self.log_framerate = 0
        """Framerate logging interval, in seconds. When non-zero, the mean
        frame rate will be logged periodically.  This is also useful in
        headless mode, to check on the server's performance."""

        task = self.base.taskMgr.add(self._log_framerate, "log_framerate")
        task.dt = 0
        task.n = 0
        task.last_time = 0

        self.astro_processor = AstroProcessor(self)
        self.bubble_processor = BubbleProcessor(self)
        self.circuit_processor = CircuitProcessor(self)
        self.construction_processor = ConstructionProcessor(self)
        self.countdown_processor = CountdownProcessor(self)
        self.damage_processor = DamageProcessor(self)
        self.gravity_processor = GravityProcessor(self, gravity)
        self.input_processor = InputProcessor(self)
        self.sensor_processor = SensorProcessor(self)
        self.master_processor = MasterProcessor(self)
        self.team_processor = TeamProcessor(self)
        self.thruster_processor = ThrusterProcessor(self)

        # Order is important, e.g. bubble_processor must come before
        # display_processor.
        for p in [
            self.gravity_processor,
            self.bubble_processor,
            self.team_processor,
            self.damage_processor,
            self.circuit_processor,
            self.sensor_processor,
            self.thruster_processor,
            self.input_processor,
            self.countdown_processor,
            self.astro_processor,
            self.master_processor,
            self.construction_processor,
        ]:
            self.entity_world.add_processor(p)

        if headless:
            self.display_processor = None
        else:
            p = self.display_processor = DisplayProcessor(
                self, fullscreen=fullscreen)
            self.entity_world.add_processor(p)
            p.set_bubble(self.bubble_processor.bubble)

    def run(self):
        # type: () -> ()
        """Runs the game. Must be run from the main thread, which will be
        blocked until the game ends."""
        try:
            if self._running: raise Exception('already running')
            self._running = True
            self.base.taskMgr.add(self._process, 'update_world')
            log.info("Game started.")
            self.base.run()

            # [Clears the scene graph, removes tasks and releases resources.]
            # Well that's what we should be doing, but we just end the task
            # and kill the process because that's simpler.
            self.base.taskMgr.remove('update_world')
            self.base.userExit()  # Not sure if this is always necessary.

            # We could set up more sophisticated resource management (time
            # budget) for tasks like this:
            # taskMgr.setupTaskChain(
            #   'body_simulation', numThreads=None, tickClock=None,
            #   threadPriority=None, frameBudget=None,
            #   frameSync=None, timeslicePriority=None)

        except (KeyboardInterrupt, SystemExit):
            pass
        except:
            log.fatal('Error raised.', exc_info=1)
            raise
        finally:
            self._running = False

    def quit(self):
        # type: () -> ()
        """Quits the game."""
        self.base.taskMgr.stop()
        log.info("Quit the game.")

    def _dt(self):
        """Returns the time elapsed since the previous frame.  This is factored
        to a method so that GameMaster can adjust the value (i.e. speed up/slow
        down time).  That won't be good in prod though, because the clients
        will constantly be out of sync."""
        return self.global_clock.getDt()

    # noinspection PyBroadException
    def _process(self, task):
        # type: (Task) -> ()
        """Updates game state."""
        try:
            t0 = 0
            with self.lock:
                self.entity_world.process(self._dt())

            # Yields to other threads if frame rate is high.  This is important
            # when running a server in headless mode, otherwise the main thread
            # suffocates client connections.
            if self._headless:
                t1 = self.global_clock.getDt()
                dt = t1 - t0
                if dt < self.target_framerate:
                    sleep((self.target_framerate - dt) / 2.)
        except:
            log.error("Error: thread %s", threading.current_thread().getName(), exc_info=1)
            self.base.taskMgr.stop()

        return task.cont

    def delete(self, entity):
        # type: (Entity) -> ()
        """Deletes the entity immediately."""
        # Nothing to do if already deleted. Possible if a delayed callback runs
        # after entity is already deleted.
        if entity not in self.entity_world.entities:
            return

        self.entity_world.delete_entity(entity)

    def _collision(self, e0, e1):
        """Called when two entities collide."""
        cm = self.component_mappers
        if cm.Construction(e0) or cm.Construction(e1):
            return
        elif cm.Projectile(e0):
            self.damage_processor.projectile_impact(e0, e1)
        elif cm.Projectile(e1):
            self.damage_processor.projectile_impact(e1, e0)
        elif cm.Avatar(e0) and (
                    cm.Star(e1) or cm.Planet(e1)):
            self.damage_processor.crash(e0, e1)
        elif cm.Avatar(e1) and (
                    cm.Star(e0) or cm.Planet(e0)):
            self.damage_processor.crash(e1, e0)
        # These checks mitigate cheating by warping into other avatars.
        elif cm.Station(e0):
            self.damage_processor.crash(e1, e0)
        elif cm.Station(e1):
            self.damage_processor.crash(e0, e1)
        elif cm.Ship(e0) and cm.Ship(e1):
            self.damage_processor.crash(e0, e1)
            self.damage_processor.crash(e1, e0)

    def _log_framerate(self, task):
        """Logs the framerate to the console."""
        if not self.log_framerate:
            return Task.cont
        now = self.global_clock.get_long_time()
        if now >= task.last_time + self.log_framerate:
            try:
                log.info("framerate: {}".format(task.n / task.dt))
            except Exception as e:
                log.warn(e, exc_info=1)
            task.n = 0
            task.dt = 0.
            task.last_time = now
        else:
            task.n += 1
            task.dt += self.global_clock.getDt()

        return Task.cont


__doc__ = Game.__doc__
