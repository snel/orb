import math
import logging

import typing

from orb.components import (
    Transform, Body, AttitudeThrusters, LinearThrusters, WarpDrive, FuelTank)
from orb.components.component_mappers import ComponentMappers
from orb.core import config
from orb.esper import Processor
from pypanda3d.core import Vec3D, Vec3

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)

required_components = {Transform, Body, FuelTank}
optional_components = {AttitudeThrusters, LinearThrusters, WarpDrive}

c_squared = config.scaled_c ** 2


class ThrusterProcessor(Processor):
    """Computes thruster output and fuel use for linear thrusters, attitude
    thrusters and warp drives.

    Fuel drain for linear and attitude thrusters:
        To compute the mass `m` used to generate thrust `F` for time `t`,
        with efficiency `y`:

        .. math::
            :nowrap:

            \\begin{eqnarray}
                E &= \\frac{1}{2} m v^2              & \\mbox{[kinetic energy of exhaust]} \\\\
                E &= \\frac{F x}{y}                  & \\mbox{[work done, for $y \\neq 0$]} \\\\
                \\Rightarrow E &= \\frac{F v \\Delta t}{y} & \\\\
                \\Rightarrow \\frac{m v^2}{2} &= \\frac{F v \\Delta t}{y} & \\\\
                \\Rightarrow m &= \\frac{2 F \\Delta t}{y v}   & \\mbox{[fuel spent]}
            \\end{eqnarray}

    Fuel drain for warp drive:
        To compute the mass `m` used to warp a body with mass :math:`m_b`
        at speed `v` for time `t`, with efficiency `y`:

        .. math::
            :nowrap:

            \\begin{eqnarray}
                E &= m c^2                          & \\mbox{[chemical energy of fuel]} \\\\
                E &= \\frac{m_b v^2}{2 y}             & \\mbox{[kinetic energy gained]} \\\\
                \\Rightarrow \\frac{m_b v^2}{2y} &= m c^2 & \\\\
                \\Rightarrow m &= \\frac{m_b * v^2}{2 y c^2}  & \\mbox{[fuel spent]}
            \\end{eqnarray}

    Warp drive:
        Linear thrusters are disabled while the warp drive is engaged.

        In the spirit of the Alcubierre drive, the exit velocity is the same as
        the entry velocity before activating the warp drive.
    """

    def __init__(self, game):
        # type: (Game) -> ()
        super(ThrusterProcessor, self).__init__()
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers
        self._entities = set()

    def _process(self, dt):
        for e in self._entities:
            fuel_tank = self._cm.FuelTank(e)
            body = self._cm.Body(e)
            transform = self._cm.Transform(e)
            forward = transform.get_relative_vector(Vec3D.forward())
            enabled = fuel_tank.fuel and not self._cm.Construction(e)

            eng = self._cm.WarpDrive(e)
            if enabled and eng and eng.engine.throttle > 0.1 and (
                            eng.appliance.min_current <=
                            eng.appliance.current_available <=
                        eng.appliance.max_current):

                v = eng.engine.throttle * eng.speed
                eng.previous_speed = v

                if v:
                    t0 = 3.  # Time to accelerate to c.
                    fwd = body.v.project(forward)
                    fwd = fwd.dot(fwd)
                    fwd = math.sqrt(fwd) if fwd > 0 else 0

                    u0 = min(v * .999, fwd)
                    b = t0 ** -2 * config.scaled_c
                    t = math.sqrt(u0 / b)
                    if t < t0:
                        # Gives a slow start until lightspeed.
                        u1 = b * (t + dt) ** 2
                    else:
                        # Gives an asymptotic limit at v.
                        v0 = b * t0 ** 2
                        a = 20.
                        t = -a * math.log(1. - (u0 - v0) / v)
                        u1 = v0 + v * (1. - math.e ** (-(t + dt) / a))

                    body.v = (eng.entry_velocity -
                              eng.entry_velocity.project(forward) +
                              forward * u1)
                    body.w = Vec3.zero()

                    y = config.WarpDrive.efficiency
                    fuel_tank.fuel = max(
                        0., fuel_tank.fuel - .5 * body.mass * u1 ** 2 /
                        (y * c_squared))

                    if self._game.display_processor:
                        self._game.display_processor.warp(e, u1)
                else:
                    eng.engine.throttle = 0
                # Disables impulse thrusters while warp drive is active.
                enabled = False

            elif eng and eng.previous_speed:
                eng.factor = 0.
                eng.energy = 0.
                eng.previous_speed = 0.
                body.v = eng.entry_velocity
                if self._game.display_processor:
                    self._game.display_processor.warp(e, 0)

            eng = self._cm.AttitudeThrusters(e)
            if enabled and eng and (eng.time or eng.stabilise) and (
                            eng.appliance.min_current <=
                            eng.appliance.current_available <=
                        eng.appliance.max_current):

                # Performing stabilisation by adjusting velocity directly
                # avoids problems with precession when setting torque instead.
                if eng.stabilise:
                    # We can't modify body.w's components in-place.
                    w_ = body.w
                    f = 0
                    for i in xrange(3):
                        w = w_[i]
                        x = min(w, eng.torque) * dt
                        f += x
                        w -= x
                        if w > 1e3:
                            w = 1e3
                        elif w < -1e3:
                            w = -1e3
                        if abs(w) < 1e-3:
                            w = 0
                        w_[i] = w

                    body.w = w_
                    eng.engine.throttle = f / eng.torque
                    eng.direction = -w_
                    self._game.entity_world.update_component((e,),
                                                             AttitudeThrusters)
                else:
                    eng.time = max(0, eng.time - dt)
                    f = eng.engine.throttle * eng.torque
                    force = transform.get_relative_vector(eng.direction) * f
                    body.apply_torque(force)

                v = config.AttitudeThrusters.exhaust_speed
                y = config.AttitudeThrusters.efficiency
                fuel_tank.fuel = max(
                    0., fuel_tank.fuel - 2. * f * dt / y / v)

            eng = self._cm.LinearThrusters(e)
            if enabled and eng and eng.time and (
                            eng.appliance.min_current <=
                            eng.appliance.current_available <=
                        eng.appliance.max_current):
                eng.time = max(0, eng.time - dt)

                f_ = transform.get_relative_vector(eng.direction)
                fwd = f_.project(forward)
                k = eng.engine.throttle * eng.thrust
                force = (fwd * k * eng.forward_factor +
                         (f_ - fwd) * k * eng.strafe_factor)
                body.apply_central_force(force)

                f = force.length()
                v = config.LinearThrusters.exhaust_speed
                y = config.LinearThrusters.efficiency
                fuel_tank.fuel = max(
                    0., fuel_tank.fuel - 2. * f * dt / y / v)

            if enabled:
                self._game.entity_world.update_component(self._entities,
                                                         FuelTank)

    def _added(self, component_type, entities):
        if component_type in required_components or \
                        component_type in optional_components:
            for e in entities:
                if (self._cm.has_all(e, required_components) and
                        self._cm.has_any(e, optional_components)):
                    self._entities.add(e)

    def _removed(self, component_type, entities):
        if component_type in required_components:
            self._entities.difference_update(entities)

        elif component_type in optional_components:
            for e in entities:
                if self._cm.is_only(e, optional_components, component_type):
                    self._entities.discard(e)
