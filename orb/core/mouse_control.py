import typing

from pypanda3d.core import NodePath, Vec2, Vec3, WindowProperties

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game


class MouseControl(object):
    """Provides third-person style camera control using the mouse."""

    def __init__(self, game):
        # type: (Game) -> ()
        super(MouseControl, self).__init__()
        self._game = game  # type: Game
        self.speed = 1
        self._grabbed = False
        self._position = None  # type: Vec2

        self._discrete = True
        """This causes the rotation to be discrete, i.e. the camera will stop
        moving once the mouse stops."""

    def process(self, dt):
        # Can be `None` if no window was started.
        m = self._game.base.mouseWatcherNode
        if not m or not m.hasMouse() or not self._grabbed:
            return

        mpos = m.getMouse()
        hpr = Vec3(mpos.x - self._position.x, self._position.y - mpos.y, 0)

        if self._discrete:
            hpr *= 10

        self.camera_turn(hpr * 5)

        if self._discrete:
            self._position *= 0

        # Resets the mouse position to the center of the window.
        # Otherwise the game may lose control of the mouse.
        self._center_pointer()

    def camera_turn(self, hpr):
        """Turns the camera by ``hpr`` (heading-pitch-roll, in degrees)."""
        cam = self._game.display_processor.cam  # type: NodePath
        hpr = cam.get_hpr() - hpr
        cam.set_hpr(hpr)

    def grab(self, b):
        """Grabs mouse input if ``b`` is ``True``, otherwise releases it."""
        if b:
            self._center_pointer()
            mpos = self._game.base.mouseWatcherNode.getMouse()
            self._position = Vec2(mpos.x, mpos.y)
            props = WindowProperties()
            props.setCursorHidden(True)
        else:
            props = WindowProperties()
            props.setCursorHidden(False)

        props.setMouseMode(WindowProperties.M_absolute)
        self._game.base.win.requestProperties(props)
        self._grabbed = b

    def _center_pointer(self):
        """Resets the mouse pointer in the center of the window."""
        props = self._game.base.win.getProperties()
        self._game.base.win.movePointer(
            0, int(props.getXSize() / 2), int(props.getYSize() / 2))
        self._position = Vec2(0, 0)
