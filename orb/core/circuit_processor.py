import logging

import math
import typing

from orb.components import (
    AttitudeThrusters, LaserGun, LifeSupport, LinearThrusters, Sensor, Shields,
    TorpedoGun, WarpDrive, FuelTank)
from orb.components.circuit import Circuit
from orb.components.component_mappers import ComponentMappers
from orb.components.subcomponents.appliance import Appliance
from orb.components.subcomponents.capacitor import Capacitor
from orb.core import config
from orb.esper import Processor

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game


log = logging.getLogger(__file__)


c_squared = config.c ** 2
"""Real c**2, not the scaled version, as this isn't used in a scaled
context."""

circuit_components = {
    AttitudeThrusters,
    LaserGun,
    LifeSupport,
    LinearThrusters,
    Sensor,
    Shields,
    TorpedoGun,
    WarpDrive,
}
"""Electrical component types."""


def r_series(x):
    """Computes the equivalent series resistance for the appliance/capacitor."""
    appliance = getattr(x, 'appliance', 0)
    capacitor = getattr(x, 'capacitor', 0)
    r = 0.
    if appliance:
        r += appliance.resistance
    if capacitor:
        r += capacitor.resistance
    return r


class CircuitProcessor(Processor):
    """Computes electrical output and fuel use.

    Circuit arrangement:
        Components are set in parallel.
        Components with capacitors are considered as a unit in series
        within the larger parallel circuit.

    Fuel drain:
        To compute the mass `m` used to generate current `I` at `V` Volts
        for time `t` with efficiency `y`:

        .. math::
            :nowrap:

            \\begin{eqnarray}
                E &= m c^2                         & \\mbox{[chemical energy of fuel]} \\\\
                E &= \\frac{V I t}{y}              & \\mbox{[energy converted to electricity]} \\\\
                \\Rightarrow m &= \\frac{V I t}{y c^2}. &
            \\end{eqnarray}

    Capacitor charge:
        To compute the charge on the capacitor at time :math:`t = t_0 + dt`:

        .. math::
            :nowrap:

            \\begin{eqnarray}
                Q(t) &= C V (1 - e^{-t_0/(R C)}.
            \\end{eqnarray}

        For this we need to find the current time, :math:`t_0`:

        .. math::
            :nowrap:

            \\begin{eqnarray}
                Q(t_0) = Q_0 &= C V (1 - e^{-t_0/(R C)}) & \\\\
                \\Rightarrow e^{-t_0/(R C)} &= 1 - \\frac{Q_0}{C V} & \\\\
                \\Rightarrow t_0 &= -R C \\ln{[1 - \\frac{Q_0}{C V}]} & \mbox{Eq. 1}.
            \\end{eqnarray}

        Now we compute `Q(t)` from eq. 0 using eq. 1.
        Capacitors will charge faster if resistance is set to minimum. ;)

        .. note::
            Capacitance is reduced proportional to `I/min_current` if
            this ratio is less than 1.

        .. seealso::
            http://homepages.wmich.edu/~kaldon/classes/ph207-6-rc.htm.
    """

    def __init__(self, game):
        # type: (Game) -> ()
        super(CircuitProcessor, self).__init__()
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers

    def _process(self, dt):
        entities = self.entity_world._components.get(Circuit, ())
        # Computes electrical output for each circuit. The circuit component
        # is just a marker, it doesn't store data at this time.
        for e in entities:
            # circuit = self._cm.Circuit(e)

            generator = self._cm.Generator(e)
            fuel_tank = self._cm.FuelTank(e)
            cir = [x for x in (self.entity_world.component_for_entity(e, c)
                               for c in circuit_components) if x]

            if generator and fuel_tank and fuel_tank.fuel:
                v = generator.voltage
                rx = (r_series(x) for x in cir)
                # Effective conductivity of the circuit. This is the inverse of
                # resistance (1/r).
                g = sum(1. / r for r in rx if r > 0)
                # Total current through circuit.
                i = min(v * g, generator.max_current)

                # Drains fuel supply proportional to current.
                fuel_tank.fuel = max(0., fuel_tank.fuel -
                                     v*i*dt / generator.efficiency / c_squared)
                self._game.entity_world.update_component((e,), FuelTank)
            else:
                v = 0.

            # Distributes current, assuming components are in parallel (i.e.
            # voltage is the same as EMF).
            for x in cir:
                appliance = getattr(x, 'appliance', 0)  # type: Appliance
                capacitor = getattr(x, 'capacitor', 0)  # type: Capacitor

                # If resistance is too low (or 0), the circuit breaker trips.
                # For now we just do this implicitly.
                r = r_series(x)
                if not r or not appliance:
                    r = float('inf')
                    i = 0
                else:
                    i = v / r
                    if i > appliance.max_current:
                        r = float('inf')
                        i = 0

                # Supplies the appliance with power.
                if appliance:
                    appliance.current_available = i

                    # Charges the capacitor, if any.
                    if capacitor:
                        c = capacitor.capacitance
                        y = i / appliance.min_current
                        if y < 1:
                            c *= y
                        if capacitor.charge >= c*v:
                            capacitor.charge = c*v
                        else:
                            q_0 = max(0., capacitor.charge)
                            t_0 = -r*c*math.log(1. - q_0/(c*v))
                            k = 1. - math.e**(-(t_0+dt)/(r*c))
                            capacitor.charge = max(0., c*v*k)

        # Notifies other processors of changes.
        for c in circuit_components:
            self._game.entity_world.update_component(entities, c)

        # Allows boosting/attenuating signal range by adjusting radius of
        # collision sphere.  Note this will only take effect on the next frame,
        # after physics are recomputed.
        for e in self.entity_world._components.get(Sensor, ()):
            sensor = self._cm.Sensor(e)
            x = (config.Sensor.range * sensor.appliance.current_available /
                 sensor.appliance.max_current)

            # The sensor's range must be positive.  This avoids scaling the
            # ghost to 0, which causes Bullet AABB overflow errors.
            sensor.ghost.np.set_scale(max(x, 1e-3))
            self._game.entity_world.update_component((e,), Sensor)

    def _added(self, component_type, entities):
        if self._game.is_client or not config.Capacitor.start_fully_charged:
            return

        # Initializes capacitors to full charge.
        # Assumes a generator has already been added; when loading a saved
        # game this may not be the case, but then it doesn't matter.
        for e in entities:
            cmp = self._game.entity_world.component_for_entity(e, component_type)
            if hasattr(cmp, 'capacitor'):
                generator = self._cm.Generator(e)
                if not generator: return
                cap = cmp.capacitor
                cap.charge = cap.capacitance * generator.voltage
            else:
                # Looks no further if this is not a relevant component type.
                return
