"""
This module contains classes that define constants and functions available to
players from the console.
"""

# This ignores warnings by IPython about missing optional libraries.
import warnings
warnings.simplefilter('ignore', ImportWarning)

import atexit
import inspect
import logging
from pprint import pprint
import sys
import types

import typing
from IPython.terminal.embed import InteractiveShellEmbed
from IPython.utils import terminal
from six import viewitems, wraps
from tornado.ioloop import IOLoop
from traitlets.config import get_config

from orb.components.components import Components
from orb.core import config, orb_help
from orb.core.orb_help import setup_help
from orb.esper import Entity
from orb.util import threading_util
from orb.util.dic import merge
from orb.util.formatter import Formatter as Fmt
from orb.util.banner import banner as splash_banner

# For type hints which would introduce circular dependencies.
from orb.util.log_config import add_stdout

if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)
add_stdout(log)


# noinspection PyMethodMayBeStatic
class OrbConsole(object):
    """Defines objects and methods that are exposed directly to the Orb
    console."""

    class direction(object):
        """
        All directions are relative to the avatar's bearing.
        The coordinate system is `y`-forward and `z`-up; such that
        `[0, 0, 1]` is up, `[0, 1, 0]` is forward and `[1, 0, 0]` is starboard.
        """
        stern = forward = front = [0, 1, 0]
        """To stern (forward direction)"""
        aft = backward = [0, -1, 0]
        """To aft (backward direction)"""
        port = left = [-1, 0, 0]
        """To port (left direction)"""
        starboard = right = [1, 0, 0]
        """To starboard (right direction)"""
        above = up = [0, 0, 1]
        """Above (up direction)"""
        below = down = [0, 0, -1]
        """Below (down direction)"""

    def __init__(self, **kwargs):
        setup_help(self)

    def open_console(self, banner='', same_thread=False, ns=None):
        """Opens an Orb console."""
        shell, _  = self._shell(banner, ns)  # Must be created on main thread.

        # Workaround for readline bug that breaks the terminal when Python
        # quits.
        @atexit.register
        def exit_server():
            import os
            os.system('stty sane')

        def run():
            IOLoop.clear_current()  # clear threadlocal current
            IOLoop.clear_instance()  # or del IOLoop._instance in tornado < 3.3
            # log.info(banner)
            shell()
        if same_thread:
            run()
        else:
            threading_util.run_daemon(target=run)

    def _members(self):
        """Returns member functions and variables."""
        methods = {x[0]: x[1] for x in
                   inspect.getmembers(self, predicate=inspect.ismethod)
                   if not x[0].startswith('_') and
                   x[0] not in {'open_console'}}
        members = {x[0]: x[1] for x in viewitems(self.__dict__)
                   if not x[0].startswith('_')}
        return methods, members

    def _local(self):
        """Returns a dictionary to set as the console's local namespace."""
        from orb.network.api import Api
        methods, members = self._members()
        return merge(methods, members, {
            'direction': self.direction,
            'config': config,
            'Api': Api,
        })

    def _shell(self, banner='', ns=None):
        # See http://ipython.readthedocs.io/en/5.x/config/options/terminal.html
        c = get_config()
        c.InteractiveShellApp.log_level = 'DEBUG'
        c.InteractiveShell.autoindent = True
        c.InteractiveShell.autocall = 2
        # c.InteractiveShell.confirm_exit = False
        # c.InteractiveShell.automagic = False
        c.TerminalIPythonApp.display_banner = True
        c.TerminalInteractiveShell.true_color = True
        # c.PrefilterManager.multi_line_specials = True

        orb_module = types.ModuleType("Orb", "")
        orb_module.__dict__.update(self._local())

        shell = InteractiveShellEmbed(
            config=c,
            banner1=splash_banner + orb_help.quick_guide,  # + banner,
            banner2='',
            exit_msg='Goodbye.',
            user_module=orb_module,
            term_title=True,
        )
        terminal.set_term_title('Orb console')

        for x in [
            'get_ipython',
            '__doc__',
            '__name__',
        ]:
            del orb_module.__dict__[x]

        if ns:
            orb_module.__dict__.update(ns)

        @wraps(shell.ask_exit)
        def ask_exit():
            sys.exit()
        shell.ask_exit = ask_exit
        return shell, orb_module


class GameConsole(OrbConsole):
    """Extends ``OrbConsole`` with convenience methods to work with a local
    game simulation."""

    def __init__(self, game, **kwargs):
        """Constructor.

        Args:
            game (Game): Game instance.
        """
        OrbConsole.__init__(self, **kwargs)
        self.game = game  # type: Game
        self.cm = game.component_mappers

    def _shell(self, banner='', ns=None):
        # Note that due to the GIL, individual Python statements are
        # effectively `atomic`.  But GameConsole synchronizes all commands
        # with the game engine anyway, to prevent race conditions.
        shell, orb = OrbConsole._shell(self, banner, ns)
        _run_code = shell.run_code

        @wraps(shell.run_code)
        def run_code(code_obj, result=None):
            with self.game.lock:
                _run_code(code_obj, result)
        shell.run_code = run_code

        @wraps(shell.ask_exit)
        def ask_exit():
            self.game.quit()
            sys.exit()
        shell.ask_exit = ask_exit

        if not self.game.is_client:
            orb.orb = self

        return shell, orb

    def reset_camera(self, entity):
        """Resets the camera at the entity. This can be used to place the
        camera on arbitrary objects, e.g. planets, torpedoes or enemy ships."""
        self.game.display_processor.reset_camera(entity)

    def take_control(self, entity):
        """Takes control of the entity. Assumes you are authorised to do so!
        This does not reset ``avatar`` nor ``api`` in the global namespace. """
        self.game.input_processor.take_control(entity)

    def status(self, entity=None):
        """Returns a status report of the entity.

        Args:
            entity: Entity id. Default is the player's entity.
        """
        if entity is None:
            entity = self.game.avatar.value
        y = {}

        with Components(self.game.entity_world, entity) as c:
            self_destruct = c.SelfDestruct
            if self_destruct:
                y['SelfDestruct'] = {
                    'countdown': Fmt.frac(self_destruct.countdown.time_left),
                }
            construction = c.Construction
            if construction:
                y['Construction'] = {
                    'progress': Fmt.frac(construction.progress),
                    'actor': Fmt.frac(construction.relation.actor),
                    'target': Fmt.frac(construction.relation.target),
                }
            body = c.Body
            if body:
                y['Body'] = {
                    'name': body.np.get_name(),
                    'v': Fmt.v(body.v.length()),
                    'w_': Fmt.w(body.w.length()),
                    'w': Fmt.w(-body.w[1]),
                }
            transform = c.Transform
            if transform:
                y['Transform'] = {
                    'x': transform.x,
                    'q': transform.q,
                }
            team = c.Team
            if team:
                y['Team'] = {
                    'id': team.id,
                }
            hp = c.Hitpoints
            if hp:
                y['Hitpoints'] = {
                    'hp': Fmt.frac(hp.hp),
                }
            generator = c.Generator
            shields = c.Shields
            if generator and shields:
                y['Shields'] = {
                    'charge': Fmt.frac(
                        shields.capacitor.charge /
                        (
                            shields.capacitor.capacitance * generator.voltage)),
                }
            fuel_tank = c.FuelTank
            if fuel_tank:
                y['FuelTank'] = {
                    'fuel': Fmt.frac(fuel_tank.fuel / fuel_tank.capacity),
                }
            linear_thrusters = c.LinearThrusters
            if linear_thrusters:
                y['LinearThrusters'] = {
                    'throttle': Fmt.thrust(
                        linear_thrusters.engine.throttle, 1),
                }
            attitude_thrusters = c.AttitudeThrusters
            if attitude_thrusters:
                y['AttitudeThrusters'] = {
                    'throttle': Fmt.thrust(
                        linear_thrusters.engine.throttle, 1),
                }
            warp_drive = c.WarpDrive
            if warp_drive:
                y['WarpDrive'] = {
                    'throttle': Fmt.thrust(
                        warp_drive.engine.throttle, 1),
                }
            planet = c.Planet
            if planet:
                y['Planet'] = {
                    'radius': planet.radius,
                    'is_moon': planet.is_moon,
                }
            star = c.Star
            if star:
                y['Star'] = {
                    'radius': star.radius,
                }
            colony = c.Colony
            if colony:
                y['Colony'] = {
                    'actor': Fmt.frac(colony.relation.actor),
                    'target': Fmt.frac(colony.relation.target),
                }

        pprint(y, indent=4, width=-1)

    def scores(self):
        """Prints current game scores."""
        tp = self.game.team_processor
        scores = [(team_id, tp.scores.get(team_id, 0))
                  for team_id in set(tp.entities).union(tp.scores)]
        s = ["{}. team {}: {}".format(i, team_id, score)
             for i, (team_id, score) in enumerate(
                sorted(scores, key=lambda x: -x[1]))]
        pprint(s, indent=4, width=-1)

    # TODO: Might be nice to let players name their own avatars...
    def entity_by_name(self, id):
        """Looks up an entity by name or number.

        If ``id`` is of the form `e_x` with `x` an integer, this returns entity
        `x`.  If `id` is the name of an astrological body, this returns its
        entity object.  If `id` is a number, this returns an entity object.

        Note that the Entity returned may reference an object that does not
        exist in the game."""
        if isinstance(id, Entity):
            return id
        if str.startswith(id, "e_"):
            return id[2:]

        astro = self.game.astro_processor
        if id in astro.stars:
            return astro.stars[id]
        if id in astro.planets:
            return astro.planets[id]
        if id in astro.moons:
            return astro.moons[id]

        try:
            return Entity(int(id))
        except (TypeError, ValueError):
            raise ValueError("entity not found: {}".format(id))

    def team_entities(self, team_id):
        tp = self.game.team_processor
        return tp.entities.get(team_id, ())

    def push_updates(self, entities):
        from orb.components import (
            AttitudeThrusters, Avatar, BlocksSensor, Body, Circuit,
            Colonisable, Colony, Construction, Explosion, FuelTank,
            Generator, Hitpoints, KineticOrbiter, Laser, LaserGun, LifeSupport,
            LinearThrusters, Observer, Planet, Projectile, Sensor, Shields,
            Ship, Star, Station, Team, Torpedo, TorpedoGun, Transform,
            WarpDrive, SelfDestruct)
        components = (
            AttitudeThrusters, Avatar, BlocksSensor, Body, Circuit,
            Colonisable, Colony, Construction, Explosion, FuelTank,
            Generator, Hitpoints, KineticOrbiter, Laser, LaserGun, LifeSupport,
            LinearThrusters, Observer, Planet, Projectile, Sensor, Shields,
            Ship, Star, Station, Team, Torpedo, TorpedoGun, Transform,
            WarpDrive, SelfDestruct)
        for c in components:
            self.game.entity_world.update_component(entities, c)

    def set_handicap(self, team_id, handicap=1):
        entities = self.team_entities(team_id)
        entities = [e for e in entities if self.cm.FuelTank(e)]
        for e in entities:
            fuel_tank = self.cm.FuelTank(e)
            fuel_tank.fuel *= handicap

        self.push_updates(entities)

