import logging

import typing
from six import viewitems

from orb import ENTITY_TAG
from orb.components import Bubble, Body, Transform, Sensor
from orb.components.component_mappers import ComponentMappers
from orb.esper import Processor
from orb.util import type_casting
from pypanda3d.bullet import BulletWorld, BulletRigidBodyNode
from pypanda3d.core import PythonCallbackObject

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)

required_components = {Transform, Body}
ghost_components = {Sensor}


class BubbleProcessor(Processor):
    """Updates game physics. Objects' positions and orientation in the physics
    space, as stored in their ``Body`` components, are synchronized to their
    ``Transform`` components.

    ``Transform`` vs ``Body``:
        The ``Transform`` components are authoritative on global position and
        orientation, even though the information is also stored in the ``Body``
        for the local physics space.  This distinction becomes important when
        working with multiple physics spaces.

    ``Body.np`` vs ``Bubble.np[e]``:
        Since each ``Body`` component corresponds 1-to-1 with its
        representative in the physics space, we add the ``Body``'s ``BulletNode``
        directly to the ``BulletWorld``.  This is an implementation detail,
        however. The node stored in the component should be considered a
        template. Users should access the ``bubble.np[e].bullet`` property to
        access the Bullet node for entity ``e``. This design allows easier
        migration to/from multi-world physics implementations.

    ``substeps`` and ``substep_time``:
        The ``doPhysics()`` step requires that ``dt < substeps * substep_time``, or
        we will lose time.  We set the substep time as low as possible to keep
        acceptable accuracy, but keep the ``substeps * substep_time`` high to
        ensure stability.

        The first effect of inadequate accuracy is that high-speed objects like
        lasers will fly through objects without colliding with them. The second
        effect is jittery movement.

    AABB overflow errors:
         Bullet warnings about "AABB overflow" errors in the console are due to
         NaN's or infinities in Bullet's MotionState matrices. The most likely
         cause of this are quaternions with the 4th component set to 0, i.e. we
         have a numeric instability when computing the rotation of an object
         somewhere in Orb.

    Bubbles:
        This version of ``BubbleProcessor`` only works with one ``Bubble``
        object. This works because we use a custom build of Panda3d with
        double-precision floating-point numbers. Thus the entire game world
        can accurately be modeled with a single origin.

        Multiple bubbles are useful to avoid numerical problems when objects
        move far from the origin.  This is especially problematic in a space
        game when the game engine uses single-precision floating point numbers
        to represent global coordinates. However, since we run a custom build
        of Panda3d with double-precision floating-point numbers, one bubble
        suffices to model the scale of the solar system.

        The player may periodically placed back at the origin, while everything
        else is 'treadmilled', or moved back relative to the player, based on
        their global coordinates.

    Collision detection:
        The Panda3d forums suggest that Bullet collision callbacks be added as
        follows:

           >>> from pandac.PandaModules import loadPrcFileData
           >>> loadPrcFileData('', 'bullet-enable-contact-events true')
           >>> def added(node0, node1): pass  # Collision detected.
           >>> game.base.accept('bullet-contact-added', added)

        But the callbacks are called at the end of the frame, and only for
        one ``BulletWorld``.  Because we started with multi-world physics we
        have implemented this a different way.  Using setContactAddedCallback
        does work on more than one ``BulletWorld``, but does not seem to notify
        us of some collisions, particularly those near the edges of objects.
        ``setTickCallback()`` appears to work best but is prohibitively
        expensive.
    """

    substeps = 5  # Affects stability.
    substep_time = 1 / 180.  # Affects accuracy and performance.

    def __init__(self, game):
        # type: (Game) -> ()
        Processor.__init__(self)
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers
        self._entities = set()
        self.bubble = Bubble()  # Bubble singleton for uni-world physics.

        # We need to prevent registering collisions multiple times during
        # an epoch, which can happen sometimes due to numerical
        # inaccuracy. This prevents deleting entities twice, for example.
        # Also, we can use these sets to notify the game of collisions and
        # allow it to safely delete entities.
        self._collisions = {}

        # This callback is in fact a global variable.
        o = PythonCallbackObject(self._contact_added)
        BulletWorld().setContactAddedCallback(o)

    def save(self):
        return dict(
            entities=self._entities,
        )

    def load(self, data):
        self._entities = data['entities']

    def _process(self, dt):
        self._collisions.clear()

        # Applies physics step (with one or more substeps).
        self.bubble.bullet_world.doPhysics(dt, self.substeps, self.substep_time)

        # We could do ray-testing here to do collision checking for fast-movers
        # like lasers, but that does not seem necessary at this time.

        for e0, entities in viewitems(self._collisions):
            for e1 in entities:
                # noinspection PyProtectedMember
                self._game._collision(e0, e1)

        # Updates Transform components.
        for e in self._entities:
            trans_e = self._cm.Transform(e)
            np_e = self.bubble.nps[e]
            trans_e.x = type_casting.cast_vec3(np_e.get_pos())
            trans_e.q = type_casting.cast_quat(np_e.get_quat())

        # Notifies other processors of changes.
        self._game.entity_world.update_component(self._entities, Transform)
        self._game.entity_world.update_component(self._entities, Body)

    # noinspection PyBroadException
    def _contact_added(self, m):
        n0 = m.get_node0()
        n1 = m.get_node1()
        if not n0.notifiesCollisions() or not n1.notifiesCollisions():
            return

        try:
            e0 = n0.get_python_tag(ENTITY_TAG)
            e1 = n1.get_python_tag(ENTITY_TAG)
            if e0 is None or e1 is None:
                return

            collisions = self._collisions.get(e0)
            if collisions is None:
                if self._collisions.get(e1):
                    return
                self._collisions[e0] = collisions = set()
            collisions.add(e1)

        except:
            # Note that we must be extra careful with error handling here,
            # since it's called from native code.  Unhandled exceptions could
            # mean segfaults!
            log.error("Unhandled exception: %s.", exc_info=1)

    def _updated(self, component_type, entities):
        cm = self._cm
        if component_type is Transform:
            for e in entities:
                if cm.has_all(e, required_components):
                    trans = cm.Transform(e)
                    np = cm.Body(e).np
                    np.set_pos(type_casting.cast_vec3(trans.x))
                    np.set_quat(type_casting.cast_quat(trans.q))

    def _added(self, component_type, entities):
        if component_type in required_components:
            for e in entities:
                if self._cm.has_all(e, required_components):
                    body = self._cm.Body(e)
                    np = body.np

                    self.bubble.nps[e] = np
                    self._entities.add(e)
                    self._add(np)
                    self._updated(Transform, (e,))

                    for c in ghost_components:
                        x = self._game.entity_world.component_for_entity(e, c)
                        if x:
                            self._add_ghost(e, x.ghost)

        for c in ghost_components:
            if component_type is not c:
                continue
            for e in entities:
                if self._cm.has_all(e, required_components):
                    x = self._game.entity_world.component_for_entity(e, c)
                    self._add_ghost(e, x.ghost)

    def _removed(self, component_type, entities):
        if component_type in required_components:
            for e in entities:
                if e not in self._entities:
                    continue
                if self._cm.has_all(e, required_components):
                    for c in ghost_components:
                        if component_type is not c:
                            continue
                        self._remove_ghost(
                            self._game.entity_world.component_for_entity(
                                e, c).ghost)
                self._remove(e)
            self._entities.difference_update(entities)

        for c in ghost_components:
            if component_type is not c:
                continue
            for e in entities:
                if e not in self._entities:
                    continue
                self._remove_ghost(
                    self._game.entity_world.component_for_entity(e, c).ghost)

    def _add(self, np):
        b = self.bubble
        np.reparent_to(b.world_np)
        b.bullet_world.attach(np.node())

    def _remove(self, e):
        b = self.bubble
        np = b.nps.pop(e)
        b.bullet_world.remove(np.node())
        np.removeNode()

    def _add_ghost(self, e, ghost):
        np = self.bubble.nps[e]
        ghost.np.reparent_to(np)
        self.bubble.bullet_world.attach(ghost.np.node())

    def _remove_ghost(self, ghost):
        self.bubble.bullet_world.remove(ghost.np.node())
        ghost.np.remove_node()
