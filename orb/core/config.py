"""
Constants, including scaling factors, universal constants and game config.
"""

from orb.util import colours

# ---------------------------------------------------------------------------- #
# Fundamental unit scaling.
# ---------------------------------------------------------------------------- #

# The standard Panda3d build uses single-precision floating point numbers, so
# we have to scale the world down.
scale_dist = 1e-3
"""Scales distance.  Helps improve precision where it matters and prevent 
numeric overflow in Bullet."""
scale_mass = scale_dist
"""Scales mass.  Should be the same as dist scale.."""
scale_force = scale_mass * 1.
"""Scales force."""
scale_torque = scale_force * scale_dist**2  # dist**2, even though t = f*r!
"""Scales torque."""

scale_astro_mass = 1e-2 * scale_mass
"""Scales mass of astronomical bodies; this affects orbital speed."""
scale_astro_radius = 1e-1 * scale_dist
"""Scales radii of astronomical bodies. This scales their size by r**2."""
scale_astro_dist = 2e-4 * scale_dist
"""Scales distance between astronomical bodies; keeps planets within visible 
distance. """

# ---------------------------------------------------------------------------- #
# Universal constants.
# ---------------------------------------------------------------------------- #

c = 299792458.
"""Speed of light, in m/s."""

scaled_c = c * scale_astro_dist
"""Speed of light, in m/s, scaled by the apparent astronomical distance
factor."""

G = 6.67408e-11 * scale_dist * scale_dist * scale_dist / scale_mass
"""Scaled gravitational constant, in (m*m*m)/kg/(s*s) or N*(m*m)/(kg*kg)"""

seed = 1
"""Seed for random number generation."""

# ---------------------------------------------------------------------------- #
# Display and general game config.
# ---------------------------------------------------------------------------- #

panda_config = [
    'window-title ' + 'Orb',
    # A value of "-1 -1" sets the window in the top-left corner;
    # "-2 -2" centers it on the screen.
    'win-origin -2 -2',
    'win-size 800 600',
    # 'sync-video 0',  # Doesn't appear to make a difference.
    'show-frame-rate-meter 1',
    'frame-rate-meter-scale 0.05',
    'frame-rate-meter-side-margin 0.1',
    'bullet-solver-iterations 10',  # Default is 10.

    # We didn't compile with audio support, so this silences warnigns.
    'audio-library-name null',

    # Enables the scene graph debug window. Requires the Tk pip package.
    # 'want-directtools #t',
    # 'want-tk #t',
]
"""Panda3d configuration. 
Each value is passed as parameter ``p`` to ``loadPrcFileData('', p)``.
See: https://www.panda3d.org/manual/index.php/List_of_All_Config_Variables."""

pause_on_reload = False
"""Whether the game master will pause the game after loading from a file."""


class Display(object):
    fg = (0.01, 1, 0.01, .8)
    """Colour of text and icons when objects are in front of the camera."""

    fg_behind = (0.90, .95, 0.90, .8)
    """Icon colour when objects are behind the camera."""

    bg = (0, 0, 0, .5)
    """Background colour of text fields."""

    team_colours = {
        1: colours.AQUA.rgba(),
        2: colours.CADMIUMORANGE.rgba(),
        3: colours.CHARTREUSE3.rgba(),
        4: colours.LAVENDER.rgba(),
        5: colours.LIGHTBLUE.rgba(),
        6: colours.LIGHTPINK.rgba(),
        7: colours.LIGHTYELLOW3.rgba(),
        8: colours.NAVY.rgba(),
        9: colours.AZURE3.rgba(),
    }
    """Maps team numbers to colours, for use with icons or model tinting."""

    friend_colour = colours.LIGHTSEAGREEN.rgba()
    """Colour to render friendly icons."""

    foe_colour = colours.RED2.rgba()
    """Colour to render enemy icons."""

    model_colours = False
    """Whether to tint avatars with team colours."""

    hud_colours = None  # "friend-or-foe"
    """Hud icon colour mode. One of:
        None: Everything is the same, default colour.
        "friend-or-foe": Your team is blue, other teams are red.
        "rainbow": Each team has a unique colour.
    """

    ambient_light = 0.1
    """Ambient light coefficient."""

    tex_path = "assets/textures/"
    """Path to textures."""

    sky_tex = tex_path + "1k/stars_1k_tex.jpg"
    """Path to the "sky" texture."""

    font = None  # "assets/fonts/sharetech/ShareTechMono-Regular.ttf"
    """HUD font.
    Fixed-width fonts look better when text changes rapidly (e.g. velocity 
    indicators).  Experienced an issue with fonts during deployment testing."""

    r_max = 1e6
    """Affects view distance."""

    hud_refresh_rate = .1
    """Rate at which the HUD is refreshed."""


# ---------------------------------------------------------------------------- #
# Systems.
# ---------------------------------------------------------------------------- #

class Generator(object):
    voltage = 240.
    """Electro-magnetic force (EMF) of the generator, in Volts."""

    efficiency = 1e-13
    """Energy conversion efficiency of the generator."""

    max_current = 1e5
    """Current rating, in Coulomb."""


def i(t):
    """
    Computes current required to drain 1 kg of fuel in `t` seconds.
    See :class:`~orb.core.circuit_processor.CircuitProcessor`:

    .. math::
        :nowrap:

        Let `y = efficiency`, then
        \\begin{eqnarray}
            Q(t_0) &= \frac{V I t }{y c^2} \\\\
            \\Rightarrow I &= \frac{m y c^2}{V t}.
        \\end{eqnarray}
    """
    return Generator.efficiency * c ** 2 / (Generator.voltage * t)

m = 1e4
"""Typical value for a full tank of fuel (1e4 kg)."""
Generator.current_sec = i(1.)*m
"""Current required to drain full tank of fuel in 1 second."""
Generator.current_min = i(60.)*m
"""Current required to drain full tank of fuel in 1 minute."""
Generator.current_hour = i(60.*60.)*m
"""Current required to drain full tank of fuel in 1 hour."""


class Standard(object):
    min_current = Generator.current_hour / 4.  # 4 hours
    """Standard current requirement for appliances."""

    off = float('inf')
    """Infinite resistance to turn an appliance off."""

    boost = 2.
    """Standard boost coefficient for appliances."""

    efficiency = .8
    """Engine efficiency."""


class Capacitor(object):
    start_fully_charged = True
    """Whether or not capacitors should start fully charged."""


class AttitudeThrusters(object):
    min_current = Standard.min_current
    """Nominal current, in Coulomb."""

    max_current = min_current * Standard.boost
    """Current rating, in Coulomb."""

    resistance = Generator.voltage / min_current
    """Initial resistance setting, in Ohm."""

    efficiency = 1e-1 * Standard.efficiency
    """Energy conversion efficiency."""

    exhaust_speed = 1e4
    """Speed that fuel is exhausted into space."""


class LinearThrusters(object):
    min_current = Standard.min_current
    """Nominal current, in Coulomb."""

    max_current = min_current * Standard.boost
    """Current rating, in Coulomb."""

    resistance = Generator.voltage / min_current
    """Initial resistance setting, in Ohm."""

    efficiency = Standard.efficiency
    """Energy conversion efficiency."""

    exhaust_speed = 1e4
    """Speed that fuel is exhausted into space."""

    forward_factor = 1.
    """Coefficient determinining how much thrust can be applied along the 
    longitudinal axis, i.e. how fast we can go forwards or backwards."""

    strafe_factor = .1
    """Coefficient determinining how much thrust can be applied along 
    directions other than the longitudinal axis, i.e. how fast we can strafe."""


class WarpDrive(object):
    min_current = Generator.current_min / 20.  # 20 min
    """Nominal current, in Coulomb."""

    max_current = min_current * Standard.boost
    """Current rating, in Coulomb."""

    resistance = Standard.off  # Generator.voltage / min_current
    """Initial resistance setting, in Ohm."""

    efficiency = 5e9 * Standard.efficiency
    """Energy conversion efficiency.  This is much larger than 1, which is 
    the real world would be the maximum possible. """

    default_warp_speed = 300. * scaled_c
    """Default max_factor setting for warp drives (in m/s)."""

    cooldown = 20 # 5.
    """Time between uses, in seconds."""


class Sensor(object):
    min_current = Generator.current_hour
    """Nominal current, in Coulomb."""

    max_current = min_current * Standard.boost
    """Current rating, in Coulomb."""

    resistance = Generator.voltage / min_current
    """Initial resistance setting, in Ohm."""

    range = 4e8 * scale_astro_dist
    """Range of sensor, in meters. Approximate distance from Earth to Luna."""


class Shields(object):
    min_current = Generator.current_min / 20.  # 20 min
    """Nominal current, in Coulomb."""

    max_current = min_current * Standard.boost
    """Current rating, in Coulomb."""

    resistance = Standard.off  # Generator.voltage / min_current
    """Initial resistance setting, in Ohm."""

    capacitance = 1e2
    """Capacitance of the shield capacitor, in Farad."""

    # Set to half of capacitor's max charge.
    effectiveness = 1. / (capacitance * Generator.voltage / 2.)
    """Hitpoints absorbed per Coulomb charge."""


class LifeSupport(object):
    min_current = Standard.min_current
    """Nominal current, in Coulomb."""

    max_current = min_current * Standard.boost
    """Current rating, in Coulomb."""

    resistance = Generator.voltage / min_current
    """Initial resistance setting, in Ohm."""

    damage_rate = .02
    """HP drain per second if not supplied with min_current."""


# ---------------------------------------------------------------------------- #
# Weapons.
# ---------------------------------------------------------------------------- #

class ProjectileWeapon(object):
    min_current = Standard.min_current
    """Nominal current, in Coulomb."""

    max_current = min_current * Standard.boost
    """Current rating, in Coulomb."""

    resistance = Generator.voltage / min_current
    """Initial resistance setting, in Ohm."""


# For a launch speed of c, with TTL of 2s, effective range is
# 1C*2 = 2*299792458 = 6e8 metres.
class LaserGun(ProjectileWeapon):
    launch_speed = scaled_c
    """Initial velocity of laser pulses fired by the gun."""

    cooldown = .3
    """Time taken to reload the gun."""

    fuel = 35
    """Fuel spent to fire a laser."""


# Takes 1/.1 = 10 hits to destroy a ship with no shields.
# Shields take .1/2 damage each hit, so it takes more than
#   1/.05=20 hits to break through shields.
# So takes roughly 10+20 = 30 hits to destroy a ship with 100% shields,
# and 10+(20*6) = 130 to destroy a station.
class Laser(object):
    scale = 3e1 * scale_dist
    """Approximate radius of the laser pulse."""

    mass = 1e-7 * scale_mass
    """Mass in kg.  Although real lasers are massless photons, 
    the physics engine expects a finite non-zero mass to work as expected."""

    damage = .1
    """The potential damage caused to an object, in hitpoints."""

    time_to_live = 2.
    """Time before the laser self-destructs, in seconds."""


# Note that the launch speed is high relative to c, but seems slow relative to
# the player. This is because c was scaled by the astro distance factor, not
# the micro distance factor. For a launch speed of c, with TTL of 2s, effective
# range is 1C*2 = 2*299792458 = 6e8 metres.
class TorpedoGun(ProjectileWeapon):
    launch_speed = 4e-2 * scaled_c
    """Initial velocity of torpedoes fired by the gun."""

    cooldown = 5.
    """Time taken to reload the gun."""

    fuel = 300
    """Fuel spent to fire a torpedo."""


# Takes 1/1. = 1 hit to destroy a ship with no shields.
# Shields take 1./2 damage each hit, so it takes more than
#   1/.5=2 hits to break through shields.
# So takes roughly 1+2 = 3 hits to destroy a ship with 100% shields,
# and 1+(2*6) = 13 to destroy a station.
class Torpedo(object):
    scale = 2e1 * scale_dist
    """Approximate radius of the torpedo."""

    mass = 1.5e3 * scale_mass
    """Mass in kg."""

    fuel_capacity = 1e2
    """Fuel capacity, in kg."""

    damage = 1
    """The potential damage caused to an object, in hitpoints."""

    hp = 1e-2
    """Torpedo hit points."""

    time_to_live = 30
    """Time before the torpedo self-destructs, in seconds."""

    torque = 3e4 * scale_torque
    """Magnitude of torque at full throttle."""

    thrust = 1e2 * scale_force
    """Magnitude of thrust at full throttle."""


# ---------------------------------------------------------------------------- #
# Avatars.
# ---------------------------------------------------------------------------- #


class Avatar(object):
    self_destruct_delay = 10.
    """Time for self-destruct sequence to complete, in seconds."""


class Ship(object):
    scale = 1.75e2 * scale_dist
    """Approximate radius of the ship."""

    mass = 1.5e8 * scale_mass
    """Mass in kg."""

    hp = 1
    """Hit points."""

    fuel_capacity = 1e4
    """Fuel capacity, in kg."""

    initial_fuel = .5 * fuel_capacity
    """Starting fuel amount, in kg."""

    torque = 3e11 * scale_torque
    """Magnitude of torque at full throttle."""

    thrust = 5e7 * scale_force
    """Magnitude of thrust at full throttle."""

    shield_effectiveness = Shields.effectiveness
    """Hitpoints absorbed per Coulomb charge."""


class Station(object):
    scale = 1e1 * Ship.scale
    """Approximate radius of the station."""

    mass = 1.5e9 * scale_mass
    """Mass in kg."""

    hp = 1
    """Hit points."""

    fuel_capacity = 2e4
    """Fuel capacity, in kg."""

    initial_fuel = .5 * fuel_capacity
    """Starting fuel amount, in kg."""

    torque = 1e13 * scale_torque
    """Magnitude of torque at full throttle, in Newton."""

    thrust = 4e7 * scale_force
    """Magnitude of thrust at full throttle, in Newton."""

    shield_effectiveness = Shields.effectiveness * 6
    """Hitpoints absorbed per Coulomb charge."""


class Observer(object):
    scale = Ship.scale
    """Approximate radius of the observer."""

    mass = 1.5e8 * scale_mass
    """Mass in kg."""

    torque = 4e13 * scale_torque
    """Magnitude of torque at full throttle, in Newton."""

    thrust = 2e8 * scale_force
    """Magnitude of thrust at full throttle, in Newton."""


# ---------------------------------------------------------------------------- #
# Miscellaneous.
# ---------------------------------------------------------------------------- #

class Team(object):
    respawn_time = 60.
    """Time before ships are respawned upon destruction, in seconds."""

    damage_points = .2
    """Points awarded for causing damage to an opposing team."""

    destroy_points = 10.
    """Points awarded for destroying an opposing team's avatar."""

    max_transfer_distance = 2e9
    """Maximum distance between avatars permitted for fuel transactions."""


class Colony(object):
    max_distance = 2e5 * scale_dist
    """Maximum distance ships must be from the surface of a planet to 
    colonise it.  Note that two ships must be within this distance of the 
    planet for construction to begin."""

    fuel_per_second = 100
    """Rate at which a colony generates fuel, in kg/s.
    
    At 100 kg/s it will be able to refuel a station and one ship every
    `(1e4*3)/100 = 300 s = 5 minutes`.
    When the station is running at minimum power, its power use is negligible,
    so it will be able to refuel a ship every
    `(1e4*2)/100 = 200 s = 3.3 minutes`.
    """

    station_points_per_second = 1./60  # 1 point per minute
    """Points awarded per station/colony per second."""


class Construction(object):
    progress_per_second = 1./60  # Completes within 1 minute.
    """Construction rate, as a fraction per second."""
