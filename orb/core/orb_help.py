import IPython.core.usage as ipython
from IPython.core import page
from IPython.core.magic import line_magic
from IPython.core.magics.basic import BasicMagics

from orb.network.server import api_doc

quick_reference = r"""
Orb Console -- Quick Reference Card
================================================================

obj?, obj??      : Get help, or more help for object (also works as
                   ?obj, ??obj).
                   
Orb uses IPython, an enhanced console for Python. See: http://ipython.org/
Some of its features are:
- Tab completion in the local namespace.
- System shell escape via ! , eg !ls.
- Magic commands, starting with a % (like %ls, %pwd, %cd, etc.)
- Keeps track of locally defined variables via %who, %whos.
- Show object information with a ? eg ?x or x? (use ?? for more info).
- Autocall functions without brackets, e.g. use either `f()` or just `f`.
"""

quick_guide = r"""
Orb uses IPython, an enhanced console for Python 2.7.

?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.
"""

manual = api_doc


def setup_help(orb_console):
    """Overrides IPython's help system with info specific to Orb."""
    ipython.quick_reference = quick_reference
    ipython.quick_guide = quick_guide

    @line_magic
    def quickref(self, arg):
        methods, members = orb_console._members()
        h = """\
        
Functions:
    {}
    
Objects:
    {}
        """.format(', '.join(sorted(methods)),
                   ', '.join(sorted(members)))

        page.page(quick_reference + h)

    BasicMagics.quickref = quickref
