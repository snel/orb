import logging

import typing

from orb.components import LaserGun, TorpedoGun, WarpDrive, Station, Ship, \
    SelfDestruct
from orb.components.component_mappers import ComponentMappers
from orb.components.respawn import Respawn
from orb.core import config
from orb.esper import Processor

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game


log = logging.getLogger(__file__)
components = (TorpedoGun, LaserGun, WarpDrive, Respawn, SelfDestruct)


class CountdownProcessor(Processor):
    """Updates countdown timers.

    We cannot simply place functions on a queue, because that is not
    serializable for network transmission or for saving the game.
    We also can't use absolute time offsets for the same reason.
    """

    def __init__(self, game):
        # type: (Game) -> ()
        super(CountdownProcessor, self).__init__()
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers

    # noinspection PyBroadException
    def _process(self, dt):
        # Updates countdown timers.
        ew = self._game.entity_world
        respawned = []
        self_destructed = []
        for component_type in components:
            for e in ew._components.get(component_type, ()):
                c = ew._entities.get(e, {}).get(component_type)
                if c.countdown.time_left > 0:
                    c.countdown.time_left = max(0, c.countdown.time_left - dt)
                    ew.update_component((e,), component_type)
                elif component_type is Respawn:
                    respawned.append(e)
                elif component_type is SelfDestruct:
                    self_destructed.append(e)

        if self._game.is_client:
            return  # Just to be safe for now.

        for e in respawned:
            # Assumes the entity to respawn is a ship.
            team = self._cm.Team(e)
            if not team:
                log.warn("Failed to respawn %s, "
                         "entity has no Team component", e)
            elif not self._game.team_processor.of_type(team.id, Station):
                if self._game.team_processor.of_type(team.id, Ship):
                    log.debug("Failed to respawn %s, "
                              "team %s has no stations.", e, team)
                    self._cm.Respawn(e).countdown = config.Team.respawn_time
                    ew.update_component((e,), Respawn)
                else:
                    log.debug("Failed to respawn %s, "
                              "team %s has been defeated.", e, team)
                    ew.remove_component((e,), Respawn)
            else:
                self._game.entity_factory.spawn_ship(team.id, entity=e)
                self.entity_world.remove_component(e, Respawn)
                log.info("Respawned %s", e)

        for e in self_destructed:
            self.entity_world.remove_component(e, SelfDestruct)
            if not self._cm.Projectile(e):
                log.info("{} self-destructed.".format(e))
            self._game.damage_processor.destroy(e, e)
