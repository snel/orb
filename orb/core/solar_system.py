"""Constants relating to the real solar system."""

from typing import Sequence


class Star(object):
    """Star prototype--do not confuse this with the
    :class:`orb.components.Star` component."""

    def __init__(self, name, radius, mass, planets=()):
        """Constructor.

        Args:
            name (str): Name.
            radius (float): Radius at equator, in m.
            mass (float): Mass, in kg.
            planets (Sequence[Planet]): List of planets.
        """
        self.name = name
        self.radius = radius
        self.mass = mass
        self.planets = planets


class Planet(object):
    """Planet prototype--do not confuse this with the
    :class:`orb.components.Planet` component."""

    def __init__(self, name, radius, distance, mass, moons=()):
        """
        Args:
            name (str): Name.
            radius (float): Radius at equator, in m.
            distance (float): Mean distance from sol, in m.
            mass (float): Mass, in kg.
            moons (Sequence[Moon]): List of moons.
        """
        self.name = name
        self.radius = radius
        self.distance = distance
        self.mass = mass
        self.moons = moons


class Moon(object):
    """Moon prototype."""

    def __init__(self, name, radius, distance, mass):
        """
        Args:
            name (str): Name.
            radius (float): Radius at equator, in m.
            distance (float): Mean distance from planet, in m.
            mass (float): Mass, in kg.
        """
        self.name = name
        self.radius = radius
        self.distance = distance
        self.mass = mass


Mercury = Planet(
    name="Mercury",
    radius=2440e3,
    distance=57909e6,
    mass=3.3022e23)

Venus = Planet(
    name="Venus",
    radius=6052e3,
    distance=108200e6,
    mass=4.8685e24)

Luna = Moon(
    name="Luna",
    radius=1737e3,
    distance=384400e3,
    mass=7.342e22)

Earth = Planet(
    name="Earth",
    radius=6378e3,
    distance=149600e6,
    mass=5.9736e24,
    moons=(Luna,))

Phobos = Moon(
    name="Phobos",
    radius=10e3,
    distance=9380e3,
    mass=1.0659e16)

Deimos = Moon(
    name="Deimos",
    radius=6e3,
    distance=23460e3,
    mass=1.4762e15)

Mars = Planet(
    name="Mars",
    radius=3396e3,
    distance=227940e6,
    mass=6.4185e23,
    moons=(Phobos, Deimos))

Ceres = Planet(
    name="Ceres",
    distance=414000e6,
    radius=473e3,
    mass=9.393e20)

Io = Moon(
    name="Io",
    radius=1815e3,
    distance=422e3,
    mass=8931900e16)

Europa = Moon(
    name="Europa",
    radius=1565e3,
    distance=671e3,
    mass=4800000e16)

Ganymede = Moon(
    name="Ganymede",
    radius=2634e3,
    distance=1070e3,
    mass=14819000e16)

Callisto = Moon(
    name="Callisto",
    radius=2403e3,
    distance=1883e3,
    mass=10759000e16)

Jupiter = Planet(
    name="Jupiter",
    radius=71492e3,
    distance=778400e6,
    mass=1.8986e27,
    moons=(Io, Europa, Ganymede, Callisto))

Saturn = Planet(
    name="Saturn",
    radius=60268e3,
    distance=1423600e6,
    mass=5.6846e26)

Uranus = Planet(
    name="Uranus",
    radius=25362e3,
    distance=2867000e6,
    mass=8.681e25)

Neptune = Planet(
    name="Neptune",
    radius=24764e3,
    distance=4488400e6,
    mass=10.243e25)

Pluto = Planet(
    name="Pluto",
    radius=1151e3,
    distance=5909600e6,
    mass=1.309e22)

Sol = Star(
    name="Sol",
    radius=695508e3,
    mass=1.9891e30,
    planets=(Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune))
