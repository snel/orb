import logging

import typing
from panda3d.core import BitMask32

from orb import ENTITY_TAG
from orb.components import Sensor, Star, Planet
from orb.components.component_mappers import ComponentMappers
from orb.esper import Processor

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game


log = logging.getLogger(__file__)


class SensorProcessor(Processor):
    """Updates the set of entities within range of each sensor.

    We always include the avatar itself along with all stars and planets.
    Other entities may be hidden behind large objects like planets.

    This processor does not update the properties (i.e. components) associated
    with each entity; for that see :module:`orb.core.master_processor`.

    This processor only updates one sensor each frame to reduce computational
    load.
    """

    def __init__(self, game):
        # type: (Game) -> ()
        super(SensorProcessor, self).__init__()
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers
        self._i = -1

    def _process(self, dt):
        sensor_entities = list(self.entity_world._components.get(Sensor, ()))
        if not sensor_entities:
            return
        self._i = (self._i + 1) % len(sensor_entities)

        for i, e in enumerate(sensor_entities):
            sensor = self._cm.Sensor(e)
            if i == self._i or not sensor.entities:
                entities = set(self._scan(e) + [e])
                sensor.entities = entities.union(
                    self._game.entity_world._components.get(Star, set()),
                    self._game.entity_world._components.get(Planet, set()))

    @property
    def _bubble(self):
        """Returns the bubble object."""
        return self._game.bubble_processor.bubble

    # noinspection PyArgumentList
    def _scan(self, entity):
        # (Entity) -> List[Entity]
        """Returns all entities that overlap with the avatar's sensor collision
        sphere."""
        # Trivial, but cheap method that returns all entities.
        # return self._game.entity_world.entities.keys()

        bubble = self._bubble
        np = bubble.nps.get(entity)
        if not np: return []
        node = np.find("scan").node()
        entities0 = (n.get_python_tag(ENTITY_TAG)
                     for n in node.getOverlappingNodes())
        x0 = self._pos(bubble, entity)
        return [e for e in entities0 if e is not None and
                self._line_of_sight(entity, bubble, x0, e)]

    @staticmethod
    def _pos(bubble, e):
        return bubble.nps[e].get_pos()

    def _line_of_sight(self, avatar_entity, bubble, x0, e):
        """Filters for line-of-sight so that players can hide behind planets."""
        nodes = bubble.bullet_world.rayTestAll(
            x0, self._pos(bubble, e), BitMask32.allOn()).getHits()
        entities = (n.getNode().get_python_tag(ENTITY_TAG)
                    for n in nodes if n.getNode())
        return all(e == avatar_entity or e == e1 or
                   not self._cm.BlocksSensor(e1) for e1 in entities)
