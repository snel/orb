import logging
from pprint import pprint

import typing
from direct.controls import InputState
from direct.showbase.InputStateGlobal import inputState
from future.utils import string_types, viewvalues, viewitems
from six import wraps
from tornado.httpclient import HTTPError
from transitions import Machine
from typing import Callable, Dict, Sequence, Tuple, Union, Optional

from orb import ENTITY_TAG
from orb.components import Body, Avatar, Transform
from orb.components.component_mappers import ComponentMappers
from orb.core.avatar_controller import AvatarController
from orb.core.mouse_control import MouseControl
from orb.esper import Entity, Processor
from orb.network.api import GameApi
from orb.network.json_encoding import parse_entity
from orb.util.log_config import add_stdout
from orb.util.dic import merge
from orb.util.decoration import class_decorator
from orb.util.versioned import VersionedRef
from pypanda3d.core import (
    CollisionHandlerQueue, CollisionNode, CollisionRay, CollisionTraverser,
    GeomNode, NodePath, Vec3, Vec3D)

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)
add_stdout(log)


# Use with caution, this silences all warnings incl out-of-fuel warnings.
# log.setLevel(logging.INFO)

avatar_components = {Avatar, Transform, Body}


def on_status(value):
    """Returns ON if ``value`` is ``True``, and ``False`` otherwise."""
    if value:
        return "ON"
    else:
        return "OFF"


def error_handler(f):
    """Decorator that swallows and logs error info to be prevent clients from
    crashing."""

    @wraps(f)
    def g(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except HTTPError as e:
            log.error(e.response.reason, exc_info=False)
        except Exception as e:
            log.error(str(e), exc_info=True)

    return g


@class_decorator(error_handler, predicate=lambda x: True)
class InputProcessor(Processor):
    """Manages direct input, i.e. keyboard and mouse inputs, but not console
    input."""

    def __init__(self, game):
        # type: (Game) -> ()
        super(InputProcessor, self).__init__()
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers
        self._is_thrusting_lin = False
        self._is_thrusting_att = False
        self._selected_weapon_index = 0
        self._selected_weapon = ""
        self._prev_avatar = None

        self._avatar = VersionedRef(game.avatar)
        self._avatar_controller = AvatarController(game, self._avatar.value)
        self._api = None  # type: Optional[GameApi]

        # Mouse picker.

        self._picker_ray = CollisionRay()
        picker_n = CollisionNode('mouse_picker')
        picker_n.setFromCollideMask(GeomNode.getDefaultCollideMask())
        picker_n.addSolid(self._picker_ray)
        self._picker_np = NodePath(picker_n)  # type: NodePath
        self._collision_queue = CollisionHandlerQueue()
        self._collision_traverser = CollisionTraverser()
        self._collision_traverser.add_collider(self._picker_np, self._collision_queue)

        # Input mappings.

        self.mouse_zoom_speed = 1.1
        self._game.base.disableMouse()
        self.mouse_control = MouseControl(game)

        look_angle = 90./12

        def repeat(x): return x, x+'-repeat'

        # Note that changing the key mappings at runtime won't take effect
        # until the input mode is reset (see `reload_input_mappings()`).

        self.default_key_mappings = {
            'mouse-pick': ('mouse1-up', self._mouse_pick),
            'grab-camera': ('mouse3', lambda: self.mouse_control.grab(True)),
            'ungrab-camera': ('mouse3-up', lambda: self.mouse_control.grab(False)),
            'zoom-in': (('wheel_up',)+repeat('page_up'), lambda: game.display_processor.zoom(1. / self.mouse_zoom_speed)),
            'zoom-out': (('wheel_down',)+repeat('page_down'), lambda: game.display_processor.zoom(self.mouse_zoom_speed)),

            'look-left': (repeat('arrow_left'), lambda: self.mouse_control.camera_turn(Vec3(look_angle, 0, 0))),
            'look-right': (repeat('arrow_right'), lambda: self.mouse_control.camera_turn(Vec3(-look_angle, 0, 0))),
            'look-up': (repeat('arrow_up'), lambda: self.mouse_control.camera_turn(Vec3(0, look_angle, 0))),
            'look-down': (repeat('arrow_down'), lambda: self.mouse_control.camera_turn(Vec3(0, -look_angle, 0))),

            'quit': ('escape', game.quit),
            'help': ('f1', self.help),
            'reset-camera': ('shift-v', self._reset_camera),
            'toggle-stare-at-target': ('f2', lambda: game.display_processor.toggle_stare_at_target()),
            'cam-on-target': ('f3', self._cam_on_target),
            'take-control': ('f4', self.take_control),
            # 'slow-down': ('f9', self._slow_down),
            # 'fast-forward': ('f10', self._fast_forward),

            'toggle-fullscreen': ('alt-enter', lambda: game.display_processor.toggle_fullscreen()),
            'toggle-object-icons': ('o', lambda: game.display_processor.toggle_object_icons()),
            'toggle-debug': ('f5', lambda: game.display_processor.toggle_debug()),
            # 'toggle-wireframe': ('', game.base.toggleWireframe),
            # 'toggle-textures': ('', game.base.toggleTexture),
            # 'take-screenshot': ('f10', self._take_screenshot),
        }
        # type: Dict[str, Tuple[Union[str, Sequence[str]], Union[str, Callable]]]

        self.flight_key_mappings = merge(self.default_key_mappings, {
            'throttle-full-forward': ('+', 'throttle_full_forward'),
            'throttle-full-reverse': ('-', 'throttle_full_reverse'),
            'pitch-down': ('w', 'pitch_down'),
            'pitch-up': ('s', 'pitch_up'),
            'yaw-left': ('a', 'yaw_left'),
            'yaw-right': ('d', 'yaw_right'),
            'roll-left': ('q', 'roll_left'),
            'roll-right': ('e', 'roll_right'),
            'switch-weapon': ('\\', self._switch_weapon),
            'fire': ('enter', 'fire'),
            'warp': ('`', self._warp),
            'stabilise': ('.', self._stabilise),

            'toggle_view': ('v', lambda: game.display_processor.toggle_view()),
            'next-target': (repeat('tab'), lambda: self.cycle_target(step=1)),
            'prev-target': (repeat('shift-tab'), lambda: self.cycle_target(step=-1)),
            'target-none': ('control-tab', lambda: self.set_target(None)),
            'self-destruct': ('shift-delete', self._self_destruct),
        })
        # type: Dict[str, Tuple[Union[str, Sequence[str]], Union[str, Callable]]]

        self._input_mode = InputMode(game, self)
        self._input_mode.to_default()

    def hook_api(self, host, creds):
        """Hooks input to transmit to the server through the API."""
        self._api = GameApi(self._avatar_controller, host, creds, async=False)
        # Extends the API to include a function from AvatarController.
        self._api.arsenal = self._avatar_controller.arsenal

    @property
    def controller(self):
        """Magical property that returns an
        :class:`~orb.network.api.Api` object (if running a client session) or
        `~orb.core.avatar_controller.AvatarController` (if running as game
        master) object for the player's current avatar."""
        control = self._avatar_controller
        if self._avatar.update():
            e = self._avatar.value
            if not self._cm.Avatar(e):
                e = None
            control.set_avatar(e)
            if self._api:
                self._api.entity = e.guid if e else 0

        return self._api if self._api else control

    def _process(self, dt):
        # type: (float) -> ()
        # Can be `None` if no window was started.
        if not self._game.base.camera:
            return

        self.mouse_control.process(dt)

        if not self._avatar.value:
            return
        control = self.controller

        if inputState.isSet('fire'):
            self._fire()

        lin_throttle = att_throttle = 0
        lin_dir = Vec3D(0, 0, 0)
        att_dir = Vec3D(0, 0, 0)

        if inputState.isSet('throttle_full_forward'):
            lin_dir.setY(1)
            lin_throttle = 1
        if inputState.isSet('throttle_full_reverse'):
            lin_dir.setY(-1)
            lin_throttle = 1
        if inputState.isSet('pitch_down'):
            att_dir.setY(-1)
            att_throttle = 1
        if inputState.isSet('pitch_up'):
            att_dir.setY(1)
            att_throttle = 1
        if inputState.isSet('roll_left'):
            att_dir.setZ(1)
            att_throttle = 1
        if inputState.isSet('roll_right'):
            att_dir.setZ(-1)
            att_throttle = 1
        if inputState.isSet('yaw_left'):
            att_dir.setX(-1)
            att_throttle = 1
        if inputState.isSet('yaw_right'):
            att_dir.setX(1)
            att_throttle = 1

        # The 'is_thrusting' flag lets the user send a request to cut thrust
        # once they are done.
        if self._is_thrusting_lin or lin_throttle:
            control.thrust(lin_dir, lin_throttle, .1)
            self._is_thrusting_lin = lin_throttle

        if self._is_thrusting_att or att_throttle:
            control.turn(att_dir, att_throttle, .1)
            self._is_thrusting_att = att_throttle

    def _removed(self, component_type, entities):
        if component_type in avatar_components:
            if self.controller.avatar in entities:
                if self._prev_avatar:
                    self.take_control(self._prev_avatar)
                    self._prev_avatar = None
                else:
                    self.release_control()

    def help(self):
        # type: () -> ()
        """Prints key mappings to the console."""
        bindings = {k: v[0] for k, v in viewitems(self.flight_key_mappings)}
        print("Key bindings:")
        pprint(bindings)

    def take_control(self, entity=None):
        # type: (Entity) -> ()
        """Takes control of the ship entity.

        Args:
            entity: If ``None``, the entity previously selected is used instead.
                If it is not an avatar entity, this will have no effect.
        """
        if entity is None:
            entity = self._game.display_processor.target
            if not self._cm.has_all(entity, avatar_components):
                return
        elif not isinstance(entity, Entity):
            entity = Entity(int(entity))

        self.release_control()
        self._game.avatar.value = entity
        self._input_mode.to_flight()
        log.info("You have taken control of {}.".format(entity))

    def release_control(self):
        # type: () -> ()
        """Releases control of the avatar."""
        entity = self.controller.avatar
        self._input_mode.to_default()
        log.debug("You have released control of {}.".format(entity))

    def _mouse_pick(self):
        # type: () -> ()
        """Sets the target according to what the player clicked on."""
        if self._game.base.mouseWatcherNode.hasMouse():
            self._picker_np.reparent_to(self._game.base.camera)
            mouse_pos = self._game.base.mouseWatcherNode.getMouse()
            self._picker_ray.setFromLens(
                self._game.base.camNode, mouse_pos.get_x(), mouse_pos.get_y())
            self._collision_traverser.traverse(self._game.base.render)

        if self._collision_queue.get_num_entries() > 0:
            self._collision_queue.sort_entries()
            for i in xrange(self._collision_queue.get_num_entries()):
                picked_obj = self._collision_queue.getEntry(i).getIntoNodePath()
                picked_e = picked_obj.findNetPythonTag(ENTITY_TAG)

                if not picked_e.isEmpty():
                    entity = picked_e.get_python_tag(ENTITY_TAG)
                    if entity != self.controller.avatar:
                        log.debug("Picked {0}.".format(entity))
                        self.set_target(entity)
                        return
        self.set_target(None)

    # noinspection PyUnresolvedReferences
    def _fast_forward(self):
        # type: () -> ()
        """Slows down time; only for use by the game master."""
        if hasattr(self._game, 'time_compression'):
            self._set_time_compression(max(0.0625, self._game.time_compression * 2.))

    # noinspection PyUnresolvedReferences
    def _slow_down(self):
        # type: () -> ()
        """Slows down time; only for use by the game master."""
        if hasattr(self._game, 'time_compression'):
            self._set_time_compression(self._game.time_compression / 2.)

    def _set_time_compression(self, time_compression):
        # type: (float) -> ()
        """Setting time compression too high or too low will cause physics
        accuracy to deteriorate.  So we limit it arbitrarily to [0, 16], while
        values less than 0.25 are equivalent to 0."""
        if time_compression < 0.0625:
            tc = 0.
        elif time_compression <= 1024:
            tc = time_compression
        else:
            tc = 1024.

        if time_compression:
            self._game.bubble_processor.substep_time = 1./120. * time_compression

        self._game.time_compression = tc
        log.info("Time compression set to {0}.".format(tc))

    def _match_velocity(self):
        # type: () -> ()
        """Sets the ship's velocity equal to that of the target. Intended for
        debug only."""
        target = self._game.display_processor.target
        if target is None:
            return

        body = self._cm.Body(self.controller.avatar)
        if body is None:
            return

        target_body = self._cm.Body(target)
        if not target_body:
            target_body = self._cm.Body(target)

        if not target_body:
            body.v = target_body.v

        log.info("Matched velocity to target.")

    def _reset_camera(self):
        # type: () -> ()
        """Resets the camera to its default settings for the current display
        mode."""
        self._game.display_processor.reset_camera()
        log.debug("Reset camera.")

    def _cam_on_target(self, target_entity=None):
        # type: (Entity) -> ()
        """Attaches the camera to the target."""
        if target_entity is None:
            target_entity = self._game.display_processor.target
        if target_entity is None:
            return

        self._game.display_processor.reset_camera(target_entity)
        log.info("camera on target {}".format(target_entity))

    def _take_screenshot(self, filename='screenshot'):
        # type: (str) -> ()
        """Takes a screenshot."""
        raise NotImplementedError()
        self._game.base.screenshot(filename)  # Seg fault?
        log.info("Screenshot taken.")

    def cycle_target(self, step=1):
        # type: (str) -> ()
        """Cycles the currently targeted entity."""
        t = self._game.display_processor.target
        if t is None:
            i0 = 0
        elif isinstance(t, Entity):
            i0 = t.guid
        else:
            i0 = int(t)

        n = self._game.entity_world._next_entity_id
        for i in xrange(1, n):
            j = (i0 + i*step) % n
            if self._game.display_processor.set_target(j):
                break

    def set_target(self, target):
        # type: (Entity) -> ()
        """Sets the currently targeted entity."""
        if self._game.display_processor.set_target(target):
            log.info("Set target to {0}.".format(
                self._game.display_processor.target))
        elif target is not None:
            log.warn("Invalid target.")

    def _self_destruct(self):
        # type: () -> ()
        """Starts the self-destruct sequence."""
        control = self.controller
        control.self_destruct()

    def _switch_weapon(self):
        # type: () -> ()
        """Cycles through all weapon types."""
        control = self.controller
        arsenal = [x for x in control.arsenal()]
        if not arsenal: return

        self._selected_weapon_index = (self._selected_weapon_index+1) % len(arsenal)
        self._selected_weapon = arsenal[self._selected_weapon_index]
        log.info("Selected weapon {}.".format(self._selected_weapon))

    def _fire(self):
        # type: () -> ()
        """Fires the currently selected weapon directly ahead."""
        def f(x):
            if not x.error:
                log.debug("Fired {}.".format(parse_entity(x.body)))

        # The AvatarController doesn't have the callback parameter.
        control = self.controller
        if isinstance(control, AvatarController):
            control.fire(self._selected_weapon, Vec3D.forward())
        else:
            control.fire(self._selected_weapon, Vec3D.forward(), callback=f)

    def _stabilise(self):
        # type: () -> ()
        """Activates the attitude stabiliser."""
        self.controller.stabilise()

    def _warp(self):
        # type: () -> ()
        """Toggles warp mode."""
        control = self.controller

        warp_drive = self._cm.WarpDrive(control.avatar)
        if not control.avatar or not warp_drive:
            log.warn("No warp drive available.")
            return

        x = 0. if warp_drive.engine.throttle else 1.
        control.warp(x)
        if x:
            log.info("Warp ON.")
        else:
            log.info("Warp OFF.")

    def _register_input_mappings(self, mappings):
        # type: (Dict[str, Tuple[Union[str, Sequence[str]], Union[str, Callable]]]) -> ()
        for k, v in viewvalues(mappings):
            if isinstance(k, string_types):
                self._register_key(k, v)
            else:
                for i in k:
                    self._register_key(i, v)

    def _register_key(self, k, v):
        # type: (str, Union[str, Callable]) -> ()
        base = self._game.base
        inp = inputState  # type: InputState

        if isinstance(v, string_types):
            inp.watchWithModifiers(v, k)
        else:
            base.accept(k, v)

    def reload_input_mappings(self):
        """Reloads updated input mappings."""
        s = self._input_mode.state
        self._input_mode.to_initial()
        self._input_mode.trigger("to_"+s)


# noinspection PyProtectedMember
class InputMode(Machine):
    """State machine of input modes."""

    def __init__(self, game, input_processor):
        # type: (Game) -> ()
        self._game = game  # type: Game
        self._input_processor = input_processor  # type: InputProcessor
        super(InputMode, self).__init__('self', states=['default', 'flight'])

    def on_exit_initial(self):
        self._unmap()

    def on_enter_default(self):
        ip = self._input_processor
        # ip._register_input_mappings(ip.default_key_mappings)

    def on_enter_flight(self):
        if self._game.avatar.value:
            self._detach()
        ip = self._game.input_processor
        ip._register_input_mappings(ip.flight_key_mappings)
        if self._game.display_processor:
            self._game.display_processor.display_fsm.to_flight()

    def on_exit_flight(self):
        self._detach()

    def _detach(self):
        self._unmap()
        if self._game.display_processor:
            self._game.display_processor.display_fsm.to_default()

    def _unmap(self):
        # NOT base.ignoreAll()! That disables window and mouse events too!
        if self.state == "flight":
            for k in self._game.input_processor.flight_key_mappings:
                self._game.base.ignore(k)
        elif self.states == "default":
            for k in self._game.input_processor.default_key_mappings:
                self._game.base.ignore(k)
