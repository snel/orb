import logging

import typing

from orb.components import (
    Body, Construction, Colony, Transform, Station, Ship, SelfDestruct)
from orb.components.component_mappers import ComponentMappers
from orb.core import config
from orb.esper import Processor, Entity
from pypanda3d.core import Vec3D, TransparencyAttrib

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)

constructor_components = {Transform}
"""Components types needed for an entity to construct things."""

construction_components = {Transform, Construction}
"""Components types needed for an entity to be under construction."""

station_under_construction_components = {Station, Construction}
"""Components types needed for a station to be under construction."""

body_under_construction_components = {Body, Construction}
"""Components types needed for an entity to be rendered on-screen
as being under construction."""


class ConstructionProcessor(Processor):
    """Manages construction of new stations and establishment of new
    colonies. """

    def __init__(self, game):
        # type: (Game) -> ()
        Processor.__init__(self)
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers
        self._ew = game.entity_world
        self._stations_under_construction = {}  # type: Dict[Entity, Set[Entity]]

    def _process(self, dt):
        removed_constructs = []
        deleted_constructs = []
        for e in self.entity_world._components.get(Construction, ()):
            c_actor = self._cm.Construction(e)
            rel = c_actor.relation
            if not rel.actor or not rel.target:
                removed_constructs.append(e)
                continue

            if e != rel.actor:
                continue

            progress = c_actor.progress + dt * config.Construction.progress_per_second
            c_actor.progress = progress

            c_target = self._cm.Construction(rel.target)
            if c_target:
                c_target.progress = progress

            if progress < 1:
                self._ew.update_component((rel.actor, rel.target), Construction)
            else:
                log.info("%s completed construction of %s.", rel.actor, rel.target)
                removed_constructs.append(rel.actor)
                removed_constructs.append(rel.target)

                # Sets newly built objects to be opaque once construction is
                # complete.
                body = self._cm.Body(rel.target)
                if not body.np.isEmpty():
                    body.np.set_transparency(TransparencyAttrib.MNone)
                    body.np.setAlphaScale(1)

                station = self._cm.Station(rel.target)
                if station:
                    self._ew.add_component(
                        rel.target, Colony(rel.target, station.home))
                    self._ew.add_component(
                        station.home, Colony(rel.target, station.home))
                    # Aborts rival construction projects.
                    rivals = self._stations_under_construction.get(
                        rel.target, set())
                    rivals.discard(rel.target)
                    deleted_constructs.extend(rivals)
                    if rivals:
                        log.info("Construction of %s aborted.", rivals)

        removed_colonies = []
        for e in self.entity_world._components.get(Colony, ()):
            rel = self._cm.Colony(e).relation
            if not rel.target or not rel.actor:
                removed_colonies.append(e)

        for e in removed_colonies:
            self._ew.remove_component(e, Colony)
        for e in removed_constructs:
            self._ew.remove_component(e, Construction)
        for e in deleted_constructs:
            self._game.delete(e)

    def _added(self, component_type, entities):
        # Sets objects under construction to be transparent until
        # construction is complete.
        if component_type in body_under_construction_components:
            for e in entities:
                if (self._cm.has_all(e, body_under_construction_components) and
                            e == self._cm.Construction(e).relation.target):
                    np = self._cm.Body(e).np
                    if not np.isEmpty():
                        np.set_transparency(TransparencyAttrib.MAlpha)
                        np.setAlphaScale(0.2)

    def _removed(self, component_type, entities):
        # Removes invalidated stations from the list of contestants for a
        # colony.
        if component_type in station_under_construction_components:
            for e in entities:
                if self._cm.has_all(e, station_under_construction_components):
                    construction = self._cm.Construction(e)
                    self._stations_under_construction.get(
                        construction.relation.target, set()).discard(e)

        # Removes Colony component symmetrically from planets and stations when
        # colonies are destroyed (e.g. by destroying the station).
        if component_type is Colony:
            self._remove_symmetric(entities, Colony)

        # Removes Construction components symmetrically when one of
        # the construction-relation pair is invalidated.
        if (component_type in constructor_components or
                    component_type in construction_components):
            self._remove_symmetric(entities, Construction)

    def _remove_symmetric(self, entities, component_type):
        """Sets the actors and targets of the symmetric components to None
        for the component of the given type for each entity from the list of
        entities. """
        for e in entities:
            c0 = self._ew.component_for_entity(e, component_type)
            if c0:
                e1 = c0.relation.actor if e != c0.relation.actor else c0.relation.target
                c1 = self._ew.component_for_entity(e1, component_type)
                if c1:
                    c1.relation.actor = c1.relation.target = None
                c0.relation.actor = c0.relation.target = None

    def build(self, builder):
        # type: (Entity) -> Entity
        """Starts building a new entity.  Returns the new entity."""
        if self._cm.Construction(builder):
            raise ValueError('already busy building')

        target = self._ew.create_entity()
        self._ew.add_component(builder, Construction(builder, target))
        self._ew.add_component(target, Construction(builder, target))

        log.info("%s started construction of %s.", builder, target)
        return target

    def abort(self, builder):
        # type: (Entity) -> ()
        """Aborts construction by the builder entity."""
        x = self._cm.Construction(builder)
        if not x or not x.relation:
            raise ValueError('not busy building anything')
        log.info("%s aborted construction of %s.",
                 x.relation.actor, x.relation.target)
        self._game.delete(x.relation.target)
        return

    def build_station(self, builder, world):
        """Starts building a new station in orbit around the ``world``."""
        planet = self._cm.Planet(world)
        radius = planet.radius if planet else self._cm.Body(world).shape_radius

        b_transform = self._cm.Transform(builder)
        w_transform = self._cm.Transform(world)
        forward = b_transform.get_relative_vector(Vec3D.forward())
        x = b_transform.x + forward * 1e4 * config.scale_dist
        y = (w_transform.x - x).length() - radius
        if y > config.Colony.max_distance:
            raise ValueError("too far away from planet: {} > {}".format(
                y, config.Colony.max_distance))
        if self._cm.Colony(world):
            raise ValueError("world already colonised")
        if not self._cm.Colonisable(world):
            raise ValueError("target not colonisable")
        team = self._cm.Team(builder)
        if not team:
            raise ValueError("not part of a team")

        ships_x = (self._cm.Transform(builder).x
                   for e in self._game.team_processor.of_type(team.id, Ship)
                   if e != builder)
        if not any(y for y in ships_x if
                   (y - x).length() - radius <= config.Colony.max_distance):
            raise ValueError("one more ship required nearby to start")

        e = self.build(builder)
        self._game.entity_factory.create_station(e, team.id, world, x)
        self._cm.Body(e).v = self._cm.Body(builder).v
        self._game.entity_factory.setup_station(e)

        bag = self._stations_under_construction.get(e, None)
        if bag is None:
            bag = self._stations_under_construction[e] = set()
        bag.add(e)
        return e
