import logging

import typing

from orb.core import config
from orb.components.component_mappers import ComponentMappers
from orb.components import Team, Station, Colony, FuelTank
from orb.esper import Processor

# For type hints which would introduce circular dependencies.

if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)


class TeamProcessor(Processor):
    """Manages the state of teams, score allocation and colony fuel income."""

    def __init__(self, game):
        # type: (Game) -> ()
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers

        self.events = {}
        """Maps team numbers to the event logs."""

        self.entities = {}
        """Maps team numbers to entities owned by them."""

        self.scores = {}
        """Maps team numbers to the team's total score."""

    def of_type(self, team_id, component_type):
        return set(e for e in self.entities.get(team_id, ())
                   if self.entity_world.component_for_entity(e, component_type)
                   and self._cm.Transform(e))  # Skips respawning ships.

    @property
    def team_ids(self):
        """Returns a sorted list of participating team ids.  Currently this is
        always an unbroken interval of integers [1, 2, ..., n]."""
        return sorted(self.entities)

    # noinspection PyProtectedMember
    def _process(self, dt):
        # type: (float) -> ()
        # Providers score income for each station.
        for e in self.entity_world._components.get(Station, ()):
            team = self._cm.Team(e)
            if team:
                points = config.Colony.station_points_per_second * dt
                self.scores[team.id] = (
                    self.scores.get(team.id, 0) + points)

        # Providers fuel income for each station/colony.
        for e in self.entity_world._components.get(Colony, ()):
            colony = self._cm.Colony(e)
            if e != colony.relation.actor: continue
            fuel_tank = self._cm.FuelTank(e)
            if not fuel_tank: continue

            fuel_tank.fuel = min(fuel_tank.capacity, fuel_tank.fuel +
                                 dt * config.Colony.fuel_per_second)
            self._game.entity_world.update_component((e,), FuelTank)

    def _added(self, component_type, entities):
        if component_type is Team:
            for e in entities:
                team = self._cm.Team(e)
                bag = self.entities.get(team.id)
                if bag is None:
                    bag = self.entities[team.id] = set()
                bag.add(e)

    def _removed(self, component_type, entities):
        if component_type is Team:
            for e in entities:
                team = self._cm.Team(e)
                bag = self.entities.get(team.id)
                if bag is None:
                    bag = self.entities[team.id] = set()
                bag.remove(e)
                # We don't bother deleting the team if the bag is empty.

                if not self._game.is_client and not bag:
                    self.defeated(team.id)

    # NOTE team changes not implemented; for now delete and re-add if necessary.
    # def _updated(self, component_type, entities):
    #    if component_type is Team:
    #        for e in entities:
    #            team = self._cm.Team(e)
    #            pass

    def award(self, team_id, points):
        """Awards some points to the given team.  ``points`` may be negative."""
        score = self.scores[team_id] = (
            self.scores.get(team_id, 0) + points)
        log.debug("Team {} awarded {} points, total score is {}.".format(
            team_id, points, score))

    def damaged(self, entity, damaged_by):
        """Adds and subtracts points when one entity damages another."""
        team0 = self._cm.Team(entity)
        team1 = self._cm.Team(damaged_by)
        points = config.Team.damage_points

        if team0:
            self.award(team0.id, -points)
        if team0 and team1:
            if team0.id != team1.id:
                self.award(team1.id, points)
                self.add_event(
                    team0.id, "{} (team {}) hit by {} (team {}).".format(
                        entity, team0.id, damaged_by, team1.id))
            self.add_event(
                team1.id, "{} (team {}) hit {} (team {}).".format(
                    damaged_by, team1.id, entity, team0.id))

    def destroyed(self, entity, destroyed_by):
        """Adds and subtracts points when one entity destroys another."""
        team0 = self._cm.Team(entity)
        team1 = self._cm.Team(destroyed_by)
        points = config.Team.destroy_points

        if team0:
            self.award(team0.id, -points)

        if team0 and team1:
            if team0.id != team1.id:
                self.award(team1.id, points)
                self.add_event(
                    team0.id, "{} (team {}) destroyed by {} (team {}).".format(
                        entity, team0.id, destroyed_by, team1.id))
            self.add_event(
                team1.id, "{} (team {}) destroyed {} (team {}).".format(
                    destroyed_by, team1.id, entity, team0.id))

    def defeated(self, team_id):
        """Prints to the event log when a team has been defeated."""
        log.info("Team {} defeated.".format(team_id))
        for i in self.team_ids:
            self.add_event(i, "Team {} defeated.".format(team_id))

    def add_event(self, team_id, event):
        """Adds an event to the team's event log."""
        events = self.events.get(team_id, [])
        events.append(event)
        self.events[team_id] = events
