import logging

import typing

from orb.components import Shields, Hitpoints, LifeSupport, Respawn, Colony, \
    SelfDestruct
from orb.components.component_mappers import ComponentMappers
from orb.core import config
from orb.esper import Entity, Processor
from pypanda3d.core import Vec3D

# For type hints which would introduce circular dependencies.
if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)


class DamageProcessor(Processor):
    def __init__(self, game):
        # type: (Game) -> ()
        super(DamageProcessor, self).__init__()
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers

    def _process(self, dt):
        if self._game.is_client:
            return

        # Destroys stations when they stray too far from their planet.
        for e in self.entity_world._components.get(Colony, ()):
            rel = self._cm.Colony(e).relation
            if e != rel.actor or self._cm.SelfDestruct(rel.actor): continue
            transform0 = self._cm.Transform(rel.actor)
            transform1 = self._cm.Transform(rel.target)
            body1 = self._cm.Body(rel.target)
            y = (transform1.x - transform0.x).length() \
                if (transform0 and transform1) else None
            r = body1.radius if body1 else 0
            if y is None or y - r > config.Colony.max_distance * 4:
                self._game.entity_world.add_component(
                    e, SelfDestruct(config.Avatar.self_destruct_delay))

        # Damages avatars when LifeSupport is underpowered.
        for e in self.entity_world._components.get(LifeSupport, ()):
            appliance = self._cm.LifeSupport(e).appliance
            x = appliance.current_available / appliance.min_current
            if x < 1.:
                h = self._cm.Hitpoints(e)
                if not h: continue
                h.hp = max(0., h.hp - (1. - x) * config.LifeSupport.damage_rate * dt)
                self._game.entity_world.update_component((e,), Hitpoints)

        destroyed = []
        for e in self.entity_world._components.get(Hitpoints, ()):
            h = self._cm.Hitpoints(e)
            if h.hp <= 0:
                destroyed.append(e)
        for e in destroyed:
            self._game.damage_processor.destroy(e, e)

    def crash(self, victim, obstacle):
        # type: (Entity, Entity) -> ()
        hp = self._cm.Hitpoints(victim)
        if not hp:
            return

        log.info("%s crashed into %s.", victim, obstacle)
        hp.hp = 0
        self.destroy(victim, obstacle)

    def projectile_impact(self, projectile_entity, victim):
        # type: (Entity, Entity) -> ()
        """Destroys the ``projectile_entity`` and damages the ``victim``."""
        projectile = self._cm.Projectile(projectile_entity)
        log.debug("Projectile %s (fired by %s) has hit %s.",
                  projectile_entity, projectile.fired_by, victim)

        x = self._cm.Transform(projectile_entity).x
        self._game.entity_factory.create_explosion(
            self.entity_world.create_entity(), x,
            self._cm.Body(projectile_entity).shape_radius)

        self._impact(victim, projectile.fired_by, projectile.damage)
        self._game.delete(projectile_entity)

    def _impact(self, entity, damaged_by, damage):
        # type: (Entity, Entity, float) -> ()
        """Damages the ``entity`` by the specified amount, and blames the
        ``damaged_by`` entity for the damage."""
        hp = self._cm.Hitpoints(entity)
        if not hp:
            return

        # direction = (self._cm.Transform(projectile_entity).x -
        #              self._cm.Transform(victim).x)
        # if not direction.normalize():
        #     log.warn("zero-length direction", exc_info=1)

        shields = self._cm.Shields(entity)
        if shields:
            q = shields.capacitor.charge
            damage_absorbed = min(damage, q * shields.effectiveness)
            charge_loss = damage_absorbed / shields.effectiveness
            damage = max(0., damage - damage_absorbed)

            if damage_absorbed:
                shields.capacitor.charge = max(0., q - charge_loss)
                self._game.entity_world.update_component((entity,), Shields)

        if not damage:
            # Logs the hit, even though it didn't cause hull damage.
            self._game.team_processor.damaged(entity, damaged_by)
            return
        if hp.hp <= damage:
            self.destroy(entity, damaged_by)
        else:
            h = max(0., hp.hp - damage)
            dh = hp.hp - h
            hp.hp = h
            self._game.entity_world.update_component((entity,), Hitpoints)
            log.debug("%s damaged %s by %s.", damaged_by, entity, dh)
            self._game.team_processor.damaged(entity, damaged_by)

    def destroy(self, entity, destroyed_by=None):
        # type: (Entity, Entity) -> ()
        if self._game.is_client:
            return  # Not strictly necessary.  Might reconsider not returning.

        if destroyed_by and not destroyed_by == entity:
            log.info("%s destroyed %s.", destroyed_by, entity)
        else:
            log.info("%s was destroyed.", entity)

        hp = self._cm.Hitpoints(entity)
        if not hp:
            return
        hp.hp = 0

        body = self._cm.Body(entity)
        if body:
            radius = body.shape_radius
        else:
            radius = 1

        transform = self._cm.Transform(entity)
        if transform:
            self._game.entity_factory.create_explosion(
                self.entity_world.create_entity(), transform.x, radius)

        self._game.team_processor.destroyed(entity, destroyed_by)
        ship = self._cm.Ship(entity)
        if ship:
            team = self._cm.Team(entity)
            if not team:
                log.warn("Failed to respawn %s, "
                         "entity has no Team component", entity)
            else:
                self._game.delete(entity)
                self.entity_world.add_component(entity, team)
                self.entity_world.add_component(entity, ship)
                self.entity_world.add_component(entity, Respawn())
        else:
            self._game.delete(entity)
