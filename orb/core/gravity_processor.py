import logging
import math

import typing

from orb.components import Body, KineticOrbiter
from orb.components import Transform
from orb.components.component_mappers import ComponentMappers
from orb.core import config
from orb.esper import Entity, Processor
from pypanda3d.core import Vec3D

if typing.TYPE_CHECKING:
    from orb.core.game import Game

log = logging.getLogger(__file__)


class GravityProcessor(Processor):
    """Applies gravity to bodies, including kinematic orbiters and dynamic
    bodies.

    Only the gravitational force between a dynamic body and the nearest
    kinematic body is applied.  This both improves game performance a bit and
    makes it simpler for players to compute stable orbits.

    Must be processed before ``BubbleProcessor``, because this uses NodePaths
    which have to contain something or Bullet will crash.
    """

    def __init__(self, game, gravity_enabled):
        # type: (Game, bool) -> ()
        Processor.__init__(self)
        self._game = game  # type: Game
        self._cm = game.component_mappers  # type: ComponentMappers
        self.gravity_enabled = gravity_enabled

        self._kinetic_bodies = set()
        self._kinetic_orbiters = set()
        self._dynamic_bodies = set()

        self._dk = {}
        """Maps each dynamic body to one kinetic body."""
        self._dynamic_next = []

    def _process(self, dt):
        if not self.gravity_enabled:
            return

        # Updates the next dynamic body in the sequence.
        if not self._dynamic_next:
            self._dynamic_next = list(self._dynamic_bodies)
        if self._dynamic_next:
            e0 = self._dynamic_next.pop()
            nearest = self._find_nearest_kbody(e0)
            self._dk[e0] = nearest

        # self._update_kinetic_orbits(dt)
        self._update_gravity()

    def _find_nearest_kbody(self, e0):
        transform0 = self._cm.Transform(e0)
        if not transform0:
            return
        x0 = transform0.x

        e_nearest = None
        y_nearest = float('inf')
        for e in self._kinetic_bodies:
            y = (self._cm.Transform(e).x - x0).length_squared()
            if y < y_nearest:
                e_nearest = e
                y_nearest = y
        return e_nearest

    def _added(self, component_type, entities):
        if component_type is KineticOrbiter:
            for e in entities:
                self._kinetic_orbiters.add(e)

        elif component_type is Body:
            for e in entities:
                if self._cm.Body(e).kinematic:
                    self._kinetic_bodies.add(e)
                else:
                    self._dynamic_bodies.add(e)

    def _removed(self, component_type, entities):
        if component_type is Transform:
            for e in entities:
                self._dk.pop(e, None)

        elif component_type is KineticOrbiter:
            for e in entities:
                self._kinetic_orbiters.discard(e)

        elif component_type is Body:
            for e in entities:
                if e in self._kinetic_bodies:
                    self._kinetic_bodies.discard(e)
                else:
                    self._dynamic_bodies.discard(e)

    def orbital_velocity(self, barycenter, satellite, axis=Vec3D.up()):
        # type: (Entity, Entity, Vec3D) -> Vec3D
        """
        Gets the orbital velocity, tangential to the orbit target.
        This assumes the orbit is perfectly circular (`eccentricity = 0`).
        The tangent vector is clockwise around the orbital axis.

        :math:`v = \\sqrt{\\frac{G(m+M)}{r}}`
        """
        if not axis.length_squared():
            log.warn("zero-length axis vector", exc_info=1)
            axis = Vec3D.zero()

        center_pos = self._cm.Transform(barycenter)
        center_body = self._cm.Body(barycenter)
        satellite_pos = self._cm.Transform(satellite)
        satellite_body = self._cm.Body(satellite)

        if (center_pos is None or center_body is None or
            satellite_pos is None or satellite_body is None):
            log.warn("invalid arguments to orbital_velocity", exc_info=1)
            return Vec3D.zero()

        r = satellite_pos.x - center_pos.x  # type: Vec3D
        up = axis.normalized()
        direction = r.normalized().cross(up).normalized()

        m = center_body.mass + satellite_body.mass
        v = direction * math.sqrt(config.G * m / r.length())
        return v

    def gravitational_force(self, barycenter, satellite):
        # type: (Entity, Entity) -> Vec3D
        """
        Gets the gravitational force acting on ``satellite`` from ``barycenter``
        (i.e. in the direction of ``barycenter``).

        :math:`F = \\frac{GmM}{r^2}`
        """
        center_pos = self._cm.Transform(barycenter)
        center_body = self._cm.Body(barycenter)
        satellite_pos = self._cm.Transform(satellite)
        satellite_body = self._cm.Body(satellite)

        if (center_pos is None or center_body is None or
            satellite_pos is None or satellite_body is None):
            log.warn("invalid arguments to gravitational_force", exc_info=1)
            return Vec3D.zero()

        r = center_pos.x - satellite_pos.x  # type: Vec3D
        r_squared = r.length_squared()

        # Avoids division by zero when the two bodies are on top of each
        # other.
        if r_squared > 1e-8:
            f = (config.G * satellite_body.mass * center_body.mass /
                 r_squared)
            return r.normalized() * f
        else:
            return Vec3D.zero()

    def _update_gravity(self):
        # type: () -> ()
        for e0 in self._dynamic_bodies:
            if self._cm.Observer(e0):
                continue
            body0 = self._cm.Body(e0)

            # Applies the force continuously for the next time step.
            # Note that we must apply f, and not df = f * dt.
            e1 = self._dk.get(e0)
            if not e1:
                continue
            f = self.gravitational_force(e1, e0)
            body0.apply_central_force(f)
            # for e1 in self._kinetic_bodies:
            #    f = self.gravitational_force(e1, e0)
            #    body0.apply_central_force(f)

        self.entity_world.update_component(self._dynamic_bodies, Body)

    # Unused at this time.
    # def _update_kinetic_orbits(self, dt):
    #     # type: (float) -> ()
    #     for e in self._kinetic_orbiters:
    #         trans_e = self._cm.Transform(e)
    #         body_e = self._cm.Body(e)
    #         orbiter = self._cm.KineticOrbiter(e)
    #         barycenter = orbiter.barycenter
    #         trans_c = self._cm.Transform(barycenter)
    #         body_c = self._cm.Body(barycenter)
    #
    #         dist = (trans_c.x - trans_e.x).length()
    #
    #         # Assumes periapsis = apoapsis.
    #         A = math.pi * dist * dist
    #         P = 2. * math.pi * math.sqrt(
    #             dist * dist * dist / config.G / body_c.mass)
    #         w = 2 * A / P / dist / dist
    #         orbiter.theta += w * dt * orbiter.period
    #
    #         # No longer correct, since orbits no longer in the xy-plane.
    #         # Could be corrected by transforming the plane using `axis`.
    #         trans_e.x = Vec3D(math.cos(orbiter.theta) * dist,
    #                           math.sin(orbiter.theta) * dist, 0) + trans_c.x
    #     self.entity_world.update_component(self._kinetic_orbiters, Transform)
