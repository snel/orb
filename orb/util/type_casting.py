"""
Provides functions for casting between Panda3d native types.
This is necessary because Panda3d fails when incorrect types are given.
"""

from panda3d.core import (
    QuatD, QuatF, Vec3, Vec3D, LVecBase3d, LVecBase3f, Vec3F)

from typing import Tuple


def cast_vec3d(v):
    # type: (Vec3) -> Vec3D
    """Returns the 3-dimensional vector as a ``Vec3D``."""
    if isinstance(v, LVecBase3d):
        return v
    if isinstance(v, LVecBase3f):
        return Vec3D(v.x, v.y, v.z)
    else:
        return Vec3D(v[0], v[1], v[2])


def cast_vec3f(v):
    # type: (Vec3D) -> Vec3F
    """Returns the 3-dimensional vector as a ``Vec3F``."""
    if isinstance(v, LVecBase3f):
        return v
    if isinstance(v, LVecBase3d):
        return Vec3(v.x, v.y, v.z)
    else:
        return Vec3(v[0], v[1], v[2])


def cast_quatd(q):
    # type: (QuatF) -> QuatD
    """Returns the quaternion (4-dimensional vector) as a ``QuatD``."""
    if isinstance(q, QuatD):
        return q
    if isinstance(q, QuatF):
        return QuatD(q.get_x(), q.get_y(), q.get_z(), q.get_w())
    else:
        return QuatD(q[0], q[1], q[2], q[3])


def cast_quatf(q):
    # type: (QuatD) -> QuatF
    """Returns the quaternion (4-dimensional vector) as a ``QuatF``."""
    if isinstance(q, QuatF):
        return q
    if isinstance(q, QuatD):
        return QuatF(q.get_x(), q.get_y(), q.get_z(), q.get_w())
    else:
        return QuatF(q[0], q[1], q[2], q[3])


def cast_tuple3(x):
    # type: (Vec3D) -> Tuple
    """Returns the 3-dimensional vector as a tuple."""
    return x.x, x.y, x.z


def cast_tuple4(q):
    # type: (QuatD) -> Tuple
    """Returns the quaternion (4-dimensional vector) as a tuple."""
    return q.get_x(), q.get_y(), q.get_z(), q.get_w()


# cast_vec3 = cast_vec3f
# cast_quat = cast_quatf
cast_vec3 = cast_vec3d
cast_quat = cast_quatd
