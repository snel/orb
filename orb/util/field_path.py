from typing import Callable

from pypanda3d.core import LineSegs, NodePath, Vec3, Vec4


class FieldPath(object):
    """Renders a path of connected line segments, given a function describing a
    3D field.  The line is computed by simulating a particle moving through the
    field."""

    def __init__(self, field, initial_position=Vec3(0, 0, 0), initial_args=None, step=1, n=100,
                 colour=Vec4(0, 1, .7, 1), thickness=5):
        # type: (Callable[[float, Vec3, A], (Vec3, A)], Vec3, A, float, int, Vec4, float) -> ()
        """Constructor.
        
        Args:
            field (Callable[[float, Vec3, A], (Vec3, A)):
                Function describing a 3D field: ``(dt, Vec3, A) -> (Vec3, A)``.
            initial_position (Vec3): Initial position of the simulated particle.
            initial_args (A): Initial arguments passed to the field function.
            step (float): Time step, called ``dt`` above.
            n (int): Number of time steps.
            colour (Vec4): Line colour.
            thickness (float): Line thickness, in pixels.
        """
        self._initial_position = initial_position
        self._initial_args = initial_args
        self._field = field
        self._step = step
        self._n = n
        self._colour = colour
        self._thickness = thickness

        # Lines must be separate ``LineSeg`` objects in order to allow
        # different thicknesses. The parent node ``np`` groups them together
        # for convenience. All may be accessed individually if necessary.

        self.np = NodePath('path')
        self._lines_n = None
        self._lines_np = None
        self._line_segs = None  # type: LineSegs

        self._create()

    def _create(self):
        self._line_segs = LineSegs()
        self._line_segs.setThickness(self._thickness)
        self._line_segs.setColor(self._colour)

        x = self._initial_position
        args = self._initial_args
        self._line_segs.moveTo(x)

        for _ in xrange(self._n):
            x, args = self._field(self._step, x, args)
            self._line_segs.drawTo(x)

        lines_n = self._line_segs.create()
        self._lines_np = NodePath(lines_n)
        self._lines_np.reparentTo(self.np)
        return self.np


__doc__ = FieldPath.__doc__
