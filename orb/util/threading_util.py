"""
Threading utilities.
"""

from threading import Thread
# from direct.stdpy.threading import Thread


def run_daemon(**kwargs):
    """Starts a daemon thread and returns the new thread. The arguments are
    the same as for :class:`Thread`. """
    thread = Thread(**kwargs)
    thread.daemon = True
    thread.start()
    return thread
