class VersionedVal(object):
    """Wraps a value and maintains a count of the number of updates made to
    it."""
    def __init__(self, value):
        self._value = value
        self.version = 1
        self._reset = False

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value
        self.version += 1


class VersionedRef(object):
    """Wraps a ``VersionedVal`` and maintains a count of the number of updates made to
    it."""

    def __init__(self, versioned_val):
        if not isinstance(versioned_val, VersionedVal):
            raise TypeError()
        self._value = versioned_val
        self.version = 0

    @property
    def value(self):
        return self._value.value

    @value.setter
    def value(self, value):
        self._value.value = value
        self.version = self._value.version

    def update(self):
        """Updates the reference version and returns ``True`` iff it was out of
        date."""
        if self.version < self._value.version:
            self.version = self._value.version
            return self.version


if __name__ == "__main__":
    x = VersionedVal(42)
    y = VersionedRef(x)
    if y.update():
        print "a)", y.value  # This prints.
    if y.update():
        print "b)", y.value  # This does not print.

    x.value += 1
    if y.update():
        print "c)", y.value  # This prints.
    if y.update():
        print "d)", y.value  # This does not print.
