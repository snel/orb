"""
Decoration utilities.
"""

import inspect
import logging

log = logging.getLogger(__file__)


def class_decorator(decorator=None, predicate=None, **kwargs):
    """Generic class decorator.

    Args:
        decorator (Callable): Class method decorator.
        predicate (Callable): By default all public members are decorated.
    """

    if not predicate:
        def predicate(obj):
            return not obj[0].startswith('_')

    def decor(obj):
        methods = {x[0]: x[1] for x in
                   inspect.getmembers(obj, predicate=inspect.ismethod)
                   if predicate(x)}
        for m in methods.values():
            setattr(obj, m.__name__, decorator(m, **kwargs))
        return obj

    return decor


def get_methods(self):
    """Gets this object's methods as a dict for importing into the global
    namespace.

    Example:
        >>> api = Api(42)
        >>> globals().update(api.get_methods())
        >>> fire('LaserGun')  # Now the same as `api.fire('LaserGun')
    """
    import inspect
    return {x[0]: x[1] for x in
            inspect.getmembers(self, predicate=inspect.ismethod)
            if not x[0].startswith('_') and not x[0] == "get_methods"}
