"""
Math utilities.
"""
import random
from math import sqrt

import math

from typing import Generator, Sequence, Union

import type_casting
from pypanda3d.core import Lens, Mat3, Quat, QuatD, QuatF, Vec2, Vec3, Vec3D, \
    Vec4


# noinspection PyUnresolvedReferences
def argmin(iterable):
    # type: (Union[Generator[float], Sequence[float]]) -> int
    """Returns the index of the minimum value."""
    return min(enumerate(iterable), key=lambda x: x[1])[0]


# noinspection PyUnresolvedReferences
def argmax(iterable):
    # type: (Union[Generator[float], Sequence[float]]) -> int
    """Returns the index of the maximum value."""
    return max(enumerate(iterable), key=lambda x: x[1])[0]


def mean(numbers):
    # type: (Union[Generator[float], Sequence[float]]) -> float
    if not hasattr(numbers, '__len__'):
        numbers = [x for x in numbers]
    return float(sum(numbers)) / max(len(numbers), 1)


def signum(x):
    """Returns ``1`` if `x >= 0`, otherwise returns ``-1``."""
    if x >= 0:
        return 1.
    else:
        return -1.


def clip(x, tolerance=1.):
    """Returns ``x`` if `|x| < tolerance`, otherwise returns 0."""
    if abs(x) < tolerance:
        return 0.
    else:
        return x


def xyz(r, theta, phi):
    """Returns a ``Vec3`` from the given spherical coordinates."""
    x = r * math.sin(theta) * math.cos(phi)
    y = r * math.sin(theta) * math.sin(phi)
    z = r * math.cos(theta)
    return Vec3(x, y, z)


def random_spherical(r=1.):
    """Uses the sampling rule to returns spherical coordinates from a uniform
    distribution over a sphere with radius ``r``.

    See:
        https://stackoverflow.com/questions/5408276/sampling-uniformly-distributed-random-points-inside-a-spherical-volume
    """
    phi = random.uniform(0, 2 * math.pi)
    costheta = random.uniform(-1, 1)
    u = random.uniform(0, 1)
    theta = math.acos(costheta)
    return r * u**(1./3), theta, phi


def somewhere_nearby(x, r_min=0., r_max=None):
    # type: (Vec3, float, float) -> Vec3
    """Returns a point somewhere nearby ``x``.

    Args:
        r_min: Minimum distance from ``x``.
        r_max: Maximum distance from ``x``.
    """
    if r_max is None:
        r_max = r_min + 1e3
    # Uses sampling method to exclude points within sphere of radius r_min.
    r = theta = phi = 0.
    while r < r_min:
        r, theta, phi = random_spherical(r_max)
    return x + xyz(r, theta, phi)


def quatf():
    # type: () -> QuatF
    """Returns a new ``QuatF``."""
    return QuatF.ident_quat().__copy__()


def quatd():
    # type: () -> QuatD
    """Returns a new ``QuatD``."""
    return QuatD.ident_quat().__copy__()


def invert(q):
    # type: (QuatD) -> QuatD
    """Returns the inverse of the quaternion."""
    if isinstance(q, QuatF):
        q_inv = quatf()
    elif isinstance(q, QuatD):
        q_inv = quatd()
    else:
        raise ValueError("q must be a either a QuatF or QuatD")
    q_inv.invert_from(q)

    # if not q_inv.normalize():
    #     logs.warn("zero-length quaternion", exc_info=1)
    #     q_inv = QuatD.ident_quat().__copy__()

    return q_inv


def transform(q, x, q0=None):
    # type: (QuatD, Vec3D, QuatD) -> Vec3D
    """Rotates the vector ``x``.

    Args:
        q: Quaternion `q` representing the angle to rotate ``x`` by.
        q0: If specified, the output will also be rotated by the inverse of
            ``q0`` after rotating by ``q``.
    """
    # Avoids inverting the zero vector.
    if x.length_squared() == 0.:
        return x

    q = type_casting.cast_quatd(q)
    x = type_casting.cast_vec3d(x)

    if q0:
        q0_inv = invert(q)
        x = q0_inv.xform(x)

    return q.xform(x)


# Comparable to doing transform with matrices:
# m = Mat3D.ident_mat().__copy__()  # type: Mat3D
# self.q.extract_to_matrix(m)
# return m.xform(v)


def inverse_transform(q, x):
    # type: (QuatD, Vec3D) -> Vec3D
    """Rotates the vector ``x``.

    Args:
        q: Quaternion `q` representing the inverse angle to rotate ``x`` by.
    """
    # Avoids inverting the zero vector.
    if x.length_squared() == 0.:
        return x

    q = type_casting.cast_quatd(q)
    x = type_casting.cast_vec3d(x)

    q1 = invert(q)
    return q1.xform(x)


def _make_xi_mat(x):
    return Mat3(1, 0, 0,
                0, x[0], x[1],
                0, -x[1], x[0])


def _make_x_mat(x):
    return Mat3(1, 0, 0,
                0, x[1], x[0],
                0, -x[0], x[1])


def _make_y_mat(y):
    return Mat3(y[1], 0, -y[0],
                0, 1, 0,
                y[0], 0, y[1])


def _make_z_mat(z):
    return Mat3(z[1], -z[0], 0,
                z[0], z[1], 0,
                0, 0, 1)


def look_at_mat(fwd, up):
    # type: (Vec3, Vec3) -> Mat3
    """Returns a rotation matrix with the specified orientation.

    Args:
        fwd: Forward-facing vector.
        up: Upward-facing vector.

    Adapted from Panda3d C++ code, see:
        https://www.etc.cmu.edu/projects/panda3d/PandaDox/Panda/html/look__at__src_8cxx-source.html

        "Given two vectors defining a forward direction and an up vector,
        constructs the matrix that rotates things from the defined coordinate
        system to y-forward and z-up.  The forward vector will be rotated to
        y-forward first, then the up vector will be rotated as nearly to z-up
        as possible.  This will only have a different effect from heads_up() if
        the forward and up vectors are not perpendicular."
    """
    # Assumes Z-up coordinate system.

    # z is the projection of the forward vector into the XY plane.  Its
    # angle to the Y axis is the amount to rotate about the Z axis to
    # bring the forward vector into the YZ plane.

    z = Vec2(fwd[0], fwd[1])
    d = z.dot(z)
    if not d:
        z = Vec2(0, 1)
    else:
        z /= sqrt(d)

    # x is the forward vector rotated into the YZ plane.  Its angle to
    # the Y axis is the amount to rotate about the X axis to bring the
    # forward vector to the Y axis.

    x = Vec2(fwd[0] * z[0] + fwd[1] * z[1], fwd[2])
    d = x.dot(x)
    if not d:
        x = Vec2(1, 0)
    else:
        x /= sqrt(d)

    # Now applies both rotations to the up vector.  This will rotate
    # the up vector by the same amount we would have had to rotate
    # the forward vector to bring it to the Y axis.  If the vectors were
    # perpendicular, this will put the up vector somewhere in the
    # XZ plane.

    # y is the projection of the newly rotated up vector into the XZ
    # plane.  Its angle to the Z axis is the amount to rotate about the
    # Y axis in order to bring the up vector to the Z axis.
    y = Vec2(
        up[0] * z[1] - up[1] * z[0],
        -up[0] * x[1] * z[0] - up[1] * x[1] * z[1] + up[2] * x[0])
    d = y.dot(y)
    if not d:
        y = Vec2(0, 1)
    else:
        y /= sqrt(d)

    # Now builds the net rotation matrix.
    return _make_y_mat(y) * _make_xi_mat(x) * _make_z_mat(z)


def look_at(fwd, up):
    """Returns a quaternion facing ``fwd``.

    Args:
        fwd: Forward-facing vector.
        up: Upward-facing vector.
    """
    m = look_at_mat(fwd, up)
    q = Quat()
    q.set_from_matrix(m)
    return q


def project(lens, p4d):
    # type: (Lens, Vec4) -> (Vec2, bool)
    """Projects the homogeneous 3D point to a 2D point.  Similar to
    ``lens.project(p3d, p2d)`` except this supports homogeneous coordinates.

    Args:
        lens: Lens to project onto.
        p4d: Direction vector in homogeneous coordinates.
            If ``w=1``, the norm of the vector is the distance to the point.

    Returns:
        ``True`` iff the point is in front of the lens.

    See:
        https://www.etc.cmu.edu/projects/panda3d/PandaDox/Panda/html/lens_8cxx-source.html
    """
    m = lens.get_projection_mat()
    v = m.xform(p4d)
    if not v[3]:
        return Vec2(0, 0), False
    q = 1. / v[3]
    p2d = Vec2(v[0] * q, v[1] * q)
    return p2d, v[3] > 0. and -1 <= p2d[0] <= 1. and -1 <= p2d[1] <= 1

# C code reference:
#
# project_impl(const LPoint3f &point3d, LPoint3f &point2d) const {
#   const LMatrix4f &projection_mat = get_projection_mat();
#   LVecBase4f full(point3d[0], point3d[1], point3d[2], 1.0f);
#   full = projection_mat.xform(full);
#   if (full[3] == 0.0f) {
#     point2d.set(0.0f, 0.0f, 0.0f);
#     return false;
#   }
#   float recip_full3 = 1.0f/full[3];
#   point2d.set(full[0] * recip_full3, full[1] * recip_full3, full[2] * recip_full3);
#   return
#     (full[3] > 0.0f) &&
#     (point2d[0] >= -1.0f) && (point2d[0] <= 1.0f) &&
#     (point2d[1] >= -1.0f) && (point2d[1] <= 1.0f);
# }
