import logging

log = logging.getLogger(__file__)
log.disabled = True


def serializable(members=None, ignore=None):
    """Decorator that adds specialised serialisation functions to a class for
    use with the pickle module.

    Individual member variables can be selected or ignored.  If ``members`` is
    not None, only the members specified are serialized.  If ``ignore`` is not
    None, all members except the ignored ones are serialized.
    """
    if not ignore: ignore = ()

    def f(clazz):
        getstate = getattr(clazz, '__getstate__', None)

        def __getstate__(self):
            try:
                if members is not None and not members:
                    return {}
                state = getstate(self) if getstate else self.__dict__.copy()
                log.debug("serializing %s", self.__class__)

                for x in ignore:
                    del state[x]

                if members:
                    for x in state.copy():
                        if x not in members:
                            del state[x]

                log.debug("serialized state: %s", state)
                return state
            except:
                log.warn("in %s", self.__class__)
                raise

        clazz.__getstate__ = __getstate__
        return clazz

    return f
