# coding=utf-8
"""
Specifies the banner printed to the console on startup.
"""

banner = u"""
      ▄▄▄  ▄▄▄▄·
▪     ▀▄ █·▐█ ▀█▪
 ▄█▀▄ ▐▀▀▄ ▐█▀▀█▄
▐█▌.▐▌▐█•█▌██▄▪▐█
 ▀█▄▀▪.▀  ▀·▀▀▀▀
"""

# noinspection PyBroadException
try:
    from colorama import Fore
    banner = Fore.MAGENTA + banner + Fore.RESET
except:
    pass


# banner=""  # Uncomment this to disable the banner. :(
