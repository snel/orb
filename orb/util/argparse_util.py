"""
Utilities for parsing command-line arguments as input to a function.
"""

import inspect
import re

from typing import Callable
from argparse import ArgumentParser, RawTextHelpFormatter

from orb.util.dic import merge


def docspec(f):
    """Returns a dict of argument docstrings for the given function."""
    doc = f.__doc__
    x = 'Args:\n'
    try:
        i = doc.index(x)
    except:
        return {}
        # raise ValueError("docspec(): missing 'Args' block in docstring")
    if i > 0:
        i += len(x)
    m0 = re.findall('(.*?)(\Z|(\n {8}(?=\w)))', doc, re.DOTALL)
    return {m1[0]: "[{}] {}".format(m1[1], m1[2]) for m in m0
            for m1 in re.findall('(\w+?)(?: \((.+?)\)?\: )(.*)', m[0], re.DOTALL)}


def argspec(f, help=None):
    # type: (Callable, dict) -> dict
    """Returns a dict of default values for the arguments to the specified
    function.""" 
    if help is None:
        help = {}
    help = merge(docspec(f), help)

    args, _, _, defaults = inspect.getargspec(f)
    if not args: args = ()
    if not defaults: defaults = {}
    offset = len(args) - len(defaults)
    defaults = {k: v for k, v in zip(args[offset:], defaults)}
    help = {x: "{} (default: {})".format(help.get(x), defaults.get(x))
            for x in set(defaults).union(help)}
    return {x: {'default': defaults.get(x), 'help': help.get(x, "")} for x in args}


def arg_parser(f=None, *args, **kwargs):
    """Returns an argument parser.

    The parser will use the docstring of ``f`` to add command-line help for the
    arguments, if available.
    """
    if not f:
        def f(**kwargs): pass

    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, *args, **kwargs)
    spec = argspec(f)
    _add_argument = parser.add_argument

    def add_argument(*args, **kwargs):
        f = kwargs.get('f')
        if f:
            f_spec = argspec(f)
            del kwargs['f']
        else:
            f_spec = spec

        name = args[0][2:]
        return _add_argument(*args, **merge(f_spec.get(name, ()), kwargs))

    def invoke(f0=None):
        if not f0: f0 = f
        return f0(**parser.parse_args().__dict__)

    parser.add_argument = add_argument
    parser.invoke = invoke
    return parser


def docstr(before='', after='', args='', sep='\n'):
    """Decorator: concatenates ``before`` and ``after`` with the function's
    docstring, and inserts ``args`` to the Args block, assumed to be just
    before the Returns block."""
    if before is None: before = ''
    if after is None: after = ''
    if args is None: args = ''

    def f(obj):
        doc = obj.__doc__
        if doc is None:
            doc = ''

        doc = sep.join([before, doc, after])
        doc.replace(
            "\n    \nReturns:", "\n{}    \nReturns:".format(args))

        obj.__doc__ = doc
        return obj
    return f
