"""
Dictionary utilities.
"""

import collections

from typing import List
from six import iteritems


def merge(*dics):
    # type: (List[dict]) -> dict
    """Returns the union of all given dictionaries."""
    y = {}
    for x in dics:
        y.update(x)
    return y


def nested_dict_iter(nested):
    """Returns an iterator over all (key, value) pairs for leaf nodes in the
    nested dictionary. """
    for key, value in iteritems(nested):
        if isinstance(value, collections.Mapping):
            for inner_key, inner_value in nested_dict_iter(value):
                yield inner_key, inner_value
        else:
            yield key, value


class LazyDict(dict):
    """Lazily constructed dictionary.

    Should be initialised as a dict with values being a tuple consisting of a
    function and its arguments. For example:

        >>> LazyDict({
        >>>    'expensive1': (expensive_to_compute, 1),
        >>>    'expensive2': (expensive_to_compute, 2),
        >>> })

    See: https://stackoverflow.com/questions/16669367/setup-dictionary-lazily
    """
    def __getitem__(self, item):
        value = dict.__getitem__(self, item)
        if not isinstance(value, int):
            function, arg = value
            value = function(arg)
            dict.__setitem__(self, item, value)
        return value
