"""
Log configuration.
"""

import logging
import os
import sys
import threading
from logging.handlers import TimedRotatingFileHandler

import transitions
from tornado.log import LogFormatter

try:
    from colorama import init
    init()
except:
    print("colorama not installed")

fmt = '%(color)s%(asctime)s [%(levelname)1.1s]%(end_color)s %(message)s'
datefmt = '%H:%M:%S'

# Tornado's log formatter adds support for colorama.
stdout_hdlr = logging.StreamHandler(sys.stdout)
stdout_hdlr.setFormatter(LogFormatter(fmt, datefmt))

# Adds thread safety in case someone mistakenly calls from multiple threads.
_lock = threading.RLock()


def add_stdout(logger):
    """Adds a log handler that prints to ``stdout``."""
    with _lock:
        logger.addHandler(stdout_hdlr)


def rotation_handler(filename, **kwargs):
    """Returns a ``TimedRotatingFileHandler``."""
    # Creates the output path if it does not exist.
    path = os.path.abspath(os.path.dirname(filename))
    if not os.path.exists(path):
        os.makedirs(path)

    hdlr = TimedRotatingFileHandler(
        filename, when='d', interval=1, backupCount=7,
        encoding=None, delay=False, utc=False)
    fs = kwargs.get("format", fmt)  # BASIC_FORMAT
    dfs = kwargs.get("datefmt", datefmt)  # None
    hdlr.setFormatter(LogFormatter(fs, dfs))
    return hdlr


def config(filename="orb.log", to_console=False, **kwargs):
    """Sets up logging configuration.

    Args:
        filename (str): Path to log file.
        to_console (bool): Enables logging to the console as well as to the log
            file for the root logger.
    """
    with _lock:
        hdlr = rotation_handler(filename=filename, **kwargs)
        logging.root.addHandler(hdlr)
        level = kwargs.get("level", logging.DEBUG)
        if level is not None:
            logging.root.setLevel(level)

        # Adds a console handler in addition to the file handler created above.
        if to_console:
            add_stdout(logging.root)

        # Disables external logging. Currently this is only necessary for
        # `transitions`. In particular, the nesting module doesn't use the same
        # logging object, and produces a useless warning message. We disable it.
        transitions.logger.setLevel(logging.WARN)
        logging.getLogger('transitions.extensions.nesting').setLevel(logging.ERROR)
