import math

import humanfriendly

from orb.util.math_util import clip
from orb.core.config import scaled_c, scale_dist, scale_force

rad2deg = 180. / math.pi


r_length_size_units = reversed(humanfriendly.length_size_units)


def format_length(x):
    """Formats a distance in metres as a human readable length."""
    if x > 1e5:
        return "{:.2E} metres".format(x)
    return humanfriendly.format_length(x)


class Formatter(object):
    """Utility for formatting data as human-readable strings."""

    # TODO (@Sean): Remove the Entity class, and always format them this way..?
    @staticmethod
    def entity(e):
        """Formats the ``Entity``."""
        return str(e)

    @staticmethod
    def frac(x):
        """Formats the fraction."""
        return "{:.0f}".format(x*100)

    @staticmethod
    def time(t):
        """Formats the time."""
        return "{} s".format(t)

    @staticmethod
    def thrust(thrust, max_thrust):
        """Formats the thrust scalar/vector, relative to the maximum value
        specified. """
        if max_thrust:
            percent = 100. * thrust / max_thrust
        else:
            percent = float('nan')
        return "{:.2E} ({:.2f}%)".format(thrust / scale_force, percent)

    @staticmethod
    def x(x):
        """Formats the distance scalar or position vector."""
        x = max(0, clip(x / scale_dist))
        # return "{:.2E} m".format(x)
        return format_length(x)

    @staticmethod
    def v(v):
        """Formats the velocity scalar/vector."""
        # return "{:.2E} m/s ({:.2f} c)".format(v / scale_dist, v / scaled_c)
        return "{}/s ({:.2f} c)".format(
            format_length(v / scale_dist), v / scaled_c)

    @staticmethod
    def a(a):
        """Formats the acceleration scalar/vector."""
        return "{}/s^2".format(format_length(a / scale_dist))

    @staticmethod
    def angle(alpha):
        """Formats the angle, given in radians.
        Angles are always displayed as positive."""
        x = clip(alpha * rad2deg)
        return "{:.2E} deg".format(x)

    @staticmethod
    def angle2(alpha):
        """Formats the angle, given in radians.
        Angle signs are conserved (so negative angles are displayed as such)."""
        x = clip(alpha * rad2deg)
        return "{:-.2f} deg".format(x)

    @staticmethod
    def w(w):
        """Formats the angular velocity scalar/vector."""
        return "{}/s".format(Formatter.angle(w))

    @staticmethod
    def on_status(value):
        """Returns 'ON' if the value is ``True`` and 'OFF' otherwise."""
        return "ON" if value else "OFF"
