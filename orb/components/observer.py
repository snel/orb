from orb.esper import Component


class Observer(Component):
    """Non-player entity only visible by the game master."""

    def __init__(self):
        # type: (int) -> ()
        pass
