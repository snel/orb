from typing import Sequence, Type

from orb.components import (
    AttitudeThrusters, Avatar, BlocksSensor, Body, Circuit, Colonisable,
    Colony, Construction, Explosion, FuelTank, Generator, Hitpoints,
    KineticOrbiter, Laser, LaserGun, LifeSupport, LinearThrusters, Observer,
    Planet, Projectile, Respawn, SelfDestruct, Sensor, Shields, Ship, Star,
    Station, Team, Torpedo, TorpedoGun, Transform, WarpDrive)
from orb.esper import Component, Entity, EntityWorld


def _property(component_type):
    def getx(self):
        # type: () -> 'component_type'
        return self.world.component_for_entity(self.entity, component_type)

    def setx(self, c):
        return self.world.add_component(self.entity, c)

    def delx(self):
        return self.world.remove_component(self.entity, component_type)

    return property(getx, setx, delx, "")


# noinspection PyTypeChecker
class Components(object):
    """Convenience class for accessing components from a fixed entity.

    Example:
        >>> c = Components(world, entity)
        >>> t = c.Transform
        >>> x0 = t.x if t else None  # Gets the position of entity 42.

    If performance is critical, use :class:`ComponentMappers` instead.
    """

    # We add them this way to enable type hints in PyCharm.
    AttitudeThrusters = _property(AttitudeThrusters)  # type: AttitudeThrusters
    Avatar = _property(Avatar)  # type: Avatar
    BlocksSensor = _property(BlocksSensor)  # type: BlocksSensor
    Body = _property(Body)  # type: Body
    Circuit = _property(Circuit)  # type: Circuit
    Colonisable = _property(Colonisable)  # type: Colonisable
    Colony = _property(Colony)  # type: Colony
    Construction = _property(Construction)  # type: Construction
    Explosion = _property(Explosion)  # type: Explosion
    FuelTank = _property(FuelTank)  # type: FuelTank
    Generator = _property(Generator)  # type: Generator
    Hitpoints = _property(Hitpoints)  # type: Hitpoints
    KineticOrbiter = _property(KineticOrbiter)  # type: KineticOrbiter
    Laser = _property(Laser)  # type: Laser
    LaserGun = _property(LaserGun)  # type: LaserGun
    LifeSupport = _property(LifeSupport)  # type: LifeSupport
    LinearThrusters = _property(LinearThrusters)  # type: LinearThrusters
    Observer = _property(Observer)  # type: Observer
    Planet = _property(Planet)  # type: Planet
    Projectile = _property(Projectile)  # type: Projectile
    Respawn = _property(Respawn)  # type: Respawn
    SelfDestruct = _property(SelfDestruct)  # type: SelfDestruct
    Sensor = _property(Sensor)  # type: Sensor
    Shields = _property(Shields)  # type: Shields
    Ship = _property(Ship)  # type: Ship
    Star = _property(Star)  # type: Star
    Station = _property(Station)  # type: Station
    Team = _property(Team)  # type: Team
    Torpedo = _property(Torpedo)  # type: Torpedo
    TorpedoGun = _property(TorpedoGun)  # type: TorpedoGun
    Transform = _property(Transform)  # type: Transform
    WarpDrive = _property(WarpDrive)  # type: WarpDrive
    Construction = _property(Construction)  # type: Construction

    def __init__(self, world, entity):
        # type: (EntityWorld, Entity) -> ()
        self.entity = entity
        self.world = world

    def _components(self, component_types):
        # type: (Sequence[Type[Component]]) -> Sequence[Type[Component]]
        return (self.world.component_for_entity(self.entity, t)
                is not None for t in component_types)

    def has_all(self, component_types):
        # type: (Entity, Sequence[Type[Component]]) -> bool
        """Returns ``True`` iff the entity has all the specified components."""
        return all(self._components(component_types))

    def has_any(self, component_types):
        # type: (Entity, Sequence[Type[Component]]) -> bool
        """Returns ``True`` iff the entity has at least one of the specified
        components. """
        return any(self._components(component_types))

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        pass


if __name__ == "__main__":
    w = EntityWorld()
    e0 = w.create_entity()
    e1 = w.create_entity()
    with Components(w, e0) as c:
        c.Team = Team(0)
        print c.Team.id
    with Components(w, e1) as c:
        c.Team = Team(1)
        print c.Team.id
    with Components(w, e0) as c:
        print c.Team.id
        del c.Team
    print w.entities
