from orb.components.subcomponents.appliance import Appliance
from orb.components.subcomponents.engine import Engine
from orb.esper import Component
from orb.core.config import LinearThrusters as Config
from panda3d.core import Vec3D


class LinearThrusters(Component):
    """Set of linear thrusters (including reverse thrusters)."""

    forward_factor = Config.forward_factor
    strafe_factor = Config.strafe_factor

    def __init__(self, thrust, resistance=Config.resistance):
        # type: (float, float) -> ()
        """Constructor.


        Thrust and energy are related by :math:`E = F x = m a x = .5 m v^2`,
        where `E` is energy, `F` is thrust and `x` is distance.

        Args:
            thrust: Thrust in Newton, at full throttle.
            resistance: Resistance, in Ohm.
        """
        self.appliance = Appliance(
            resistance, Config.min_current, Config.max_current)
        self.engine = Engine(Config.efficiency)

        self.thrust = thrust
        """Thrust magnitude at full throttle."""

        self.direction = Vec3D.zero().__copy__()
        """Last set thrust direction, in direction of movement."""

        self.time = 0
        """Time remaining to apply last input."""
