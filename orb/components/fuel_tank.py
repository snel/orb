from orb.esper import Component


class FuelTank(Component):
    """Fuel storage tank."""

    def __init__(self, capacity, initial_fuel=None):
        # type: (float) -> ()

        self.capacity = capacity
        """Fuel capacity, in kilograms."""

        self.fuel = initial_fuel if initial_fuel is not None else capacity
        """Fuel level, in kilograms."""
