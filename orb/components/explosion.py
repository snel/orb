from orb.esper import Component


class Explosion(Component):
    """Explosion."""

    def __init__(self, radius):
        # type: (float) -> ()
        """Constructor."""

        self.radius = radius
        """Explosion radius."""
