from orb.esper import Component
from orb.util import type_casting
from orb.util.serializable import serializable
from pypanda3d.bullet import BulletRigidBodyNode
from pypanda3d.core import NodePath, Vec3, LVecBase3d


# NodePaths cannot be serialised directly.  This reduces saved game file size.
@serializable(members=[])
class Body(Component):
    """A rigid body.

    Do not attempt to access Body properties after the object has been removed
    from the physics world. Panda3d will throw an assertion error:
        ``AssertionError: !is_empty() at line 230 of built/include/nodePath.I``.
    """

    def __init__(self, np):
        # type: (NodePath) -> ()
        """Constructor.

        Args:
            np: ``NodePath`` template with a ``BulletRigidBodyNode``.
        """
        self.np = np

    @property
    def bullet(self):
        # type: () -> BulletRigidBodyNode
        """``BulletRigidBodyNode`` template.
        Updates are synchronized to ``BulletRigidBodyNode`` in all bubbles, and
        updates in the owner bubble are synchronized back to this one.

        Note that this node object is not attached to any BulletWorld.
        Modifications will not directly affect physics in the game world.
        """
        return self.np.node()

    @property
    def mass(self):
        # type: () -> float
        """Mass, in kilograms."""
        return self.bullet.get_mass()

    @property
    def v(self):
        # type: () -> Vec3
        """Linear velocity vector, in m/s."""
        return self.bullet.get_linear_velocity()

    @v.setter
    def v(self, value):
        if isinstance(value, LVecBase3d):
            value = type_casting.cast_vec3f(value)

        self.bullet.set_linear_velocity(value)

    @property
    def w(self):
        # type: () -> Vec3
        """Angular velocity vector, in m/s."""
        return self.bullet.get_angular_velocity()

    @w.setter
    def w(self, value):
        if isinstance(value, LVecBase3d):
            value = type_casting.cast_vec3f(value)

        self.bullet.set_angular_velocity(value)

    @property
    def static(self):
        # type: () -> bool
        """Specifies whether this body is static."""
        return self.bullet.is_static()

    @property
    def kinematic(self):
        # type: () -> bool
        """Specifies whether to model dynamic or kinematic physics for this
        body. """
        return self.bullet.is_kinematic()

    @property
    def radius(self):
        """Approximate spherical bounding radius.
        Use with caution; only appears to work with BulletSphereShapes."""
        return self.bullet.get_bounds().get_radius()

    @property
    def shape_radius(self):
        """Approximate rectangular bounding radius.
        Use with caution; only appears to work with BulletSphereShapes."""
        return self.bullet.get_shape_bounds().get_radius()

    def apply_central_force(self, force):
        """Applies the force at the center of gravity.  This force will be
        applied continuously during the next frame. """
        if isinstance(force, LVecBase3d):
            force = type_casting.cast_vec3f(force)

        self.bullet.apply_central_force(force)

    def apply_torque(self, torque):
        """Applies the torque.  This torque will be
        applied continuously during the next frame. """
        if isinstance(torque, LVecBase3d):
            torque = type_casting.cast_vec3f(torque)

        self.bullet.apply_torque(torque)

    @property
    def shape(self):
        """Bullet physics shape object."""
        return self.bullet.get_shape(0)
