from typing import Dict

from orb.esper import Component, Entity
from pypanda3d.bullet import BulletWorld
from pypanda3d.core import NodePath, Vec3


class Bubble(Component):
    """Represents a scene graph, or perspective of the game world.
    It is kept synchronised with global game state.

    This component is no longer attached to any entities but treated as a
    singleton; see ``BubbleProcessor``.

    The bubble contains an ``origin_np`` and ``world_np``. Both of these are placed
    under the ``root`` node.  A player (or some other important reference point)
    is placed under ``origin_np``.  Everything else the player can see or collide
    with is placed under ``world_np``.

    The bubble also contains a map of entities to nodes, and a ``BulletWorld``
    that computes physics for the bubble's scene graph.
    """

    def __init__(self):
        # type: () -> ()

        self.root = NodePath('bubble')
        """Root node of the bubble."""

        self.world_np = self.root.attach_new_node('bubble_world')  # type: NodePath
        """The world as seen from the reference point. This may be translated
        periodically."""

        self.bullet_world = BulletWorld()
        """The physics simulator for the bubble."""

        # Disables uniform gravity in Bullet.
        self.bullet_world.setGravity(Vec3.zero())

        self.nps = {}  # type: Dict[Entity, NodePath]
        """Maps entities to their nodes in the scene graph.

        Includes child nodes of both origin_np and world_np.  It is assumed
        that all these NodePaths point at a BulletRigidBodyNode.
        """
