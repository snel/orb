from orb.esper import Component
from pypanda3d.core import NodePath


class Star(Component):
    """Star."""

    def __init__(self, name, radius, light_np):
        # type: (str, float, NodePath) -> ()
        self.name = name
        self.radius = radius
        self.light_np = light_np
