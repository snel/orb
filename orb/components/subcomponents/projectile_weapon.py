from orb.components.subcomponents.appliance import Appliance
from orb.components.subcomponents.countdown import Countdown
from orb.core.config import ProjectileWeapon as Config


class ProjectileWeapon(object):
    """Weapon that fires projectiles."""

    def __init__(self, min_current, cooldown, launch_speed, resistance=Config.resistance):
        # type: (float, float, float, float) -> ()
        """Constructor.

        Args:
            min_current: Speed projectiles are fired at.
            cooldown: Time required to recharge when supplied with
                ``min_current``, in seconds.
            launch_speed: Speed projectiles are fired at, in m/s.
            resistance: Resistance, in Ohm.
        """
        self.appliance = Appliance(
            resistance, min_current, Config.max_current)
        # self.capacitor = Capacitor(Config.capacitance)
        self.countdown = Countdown(cooldown)
        self.launch_speed = launch_speed  # type: float

