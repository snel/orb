from orb.util.serializable import serializable
from pypanda3d.core import NodePath


# NodePaths cannot be serialised directly.  This reduces saved game file size.
@serializable(members=[])
class Ghost(object):
    """Collision-detection sphere."""

    def __init__(self, np):
        # type: (NodePath) -> ()
        """Constructor.

        Args:
            np: ``NodePath`` template with a ``BulletGhostNode``.
        """
        self.np = np

