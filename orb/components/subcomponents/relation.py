class Relation(object):
    """Relation between two entities."""

    def __init__(self, actor, target):
        # type: () -> ()
        self.actor = actor
        """Entity that 'acts upon' the ``target``."""

        self.target = target
        """Entity that 'acted upon' by the ``actor``."""

    def __repr__(self):
        return ("Relation(actor={}, target={})".format(
                   self.actor, self.target))

