from orb.components.subcomponents import Resistor


class Appliance(Resistor):
    """Electrical appliance that draws current."""

    def __init__(self, resistance, min_current, max_current):
        # type: (float, float, float) -> ()
        """Constructor.

        Args:
            resistance: Resistance, in Ohms. Must be non-negative.
            min_current: Minimum current required for normal function,
                in Coulomb. Must be positive.
            max_current: Maximum current rating, in Coulomb.
                Must be positive.
        """
        Resistor.__init__(self, resistance)
        self.min_current = min_current
        self.max_current = max_current
        self.current_available = min_current

    def __repr__(self):
        return ("Appliance(resistance={}, min_current={}, max_current={}, "
                "current_available={})".format(
                    self.resistance, self.min_current, self.max_current,
                    self.current_available))
