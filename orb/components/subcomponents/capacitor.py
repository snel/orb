from resistor import Resistor


class Capacitor(Resistor):
    """Electrical capacitor that stores charge, like a battery."""

    def __init__(self, capacitance, max_voltage=float('inf'), esr=0):
        # type: (float, float, float, float) -> ()
        """Constructor.

        Args:
            capacitance: Capacitance, in Farad (Coulomb per Volt).
            max_voltage:  Maximum rated voltage. Exceeding this voltage may
                destroy the capacitor!  Not currently in use.
            # leakage: Current loss, in Amps. (Not the current leaking through
            #     the dielectric, between the terminals.)
            esr: Equivalent series resistance, in Ohms. This is the internal
                resistance of the capacitor terminals.
        """
        Resistor.__init__(self, esr)

        self.capacitance = capacitance
        self.charge = 0.

    @property
    def voltage(self):
        """Returns the voltage stored in the capacitor, given by `V = Q/C`."""
        return self.charge / self.capacitance

    @property
    def energy(self):
        """Returns the energy stored in the capacitor, given by
        `E = .5 * C * V**2`."""
        return .5 * self.capacitance * self.voltage ** 2

    def __repr__(self):
        return ("Capacitor(resistance={}, capacitance={}, leakage={}, "
                "charge={})".format(
                   self.resistance, self.capacitance, self.leakage, 
                   self.charge))
