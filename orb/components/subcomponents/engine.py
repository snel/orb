class Engine(object):
    """Engine that does work of some kind."""

    def __init__(self, efficiency):
        # type: (float) -> ()
        """Constructor.

        Args:
            efficiency: Ratio of energy conversion efficiency, between 0 and 1.
                For electrical energy supplied `W_0 = V*Q = V*I*t` the energy
                output will be `W_0*efficiency` Joules.
        """
        self.throttle = 0
        """Throttle setting ratio in [0, 1]."""

        self.efficiency = efficiency
        """Ratio of energy conversion efficiency in [0, 1]."""

    def __repr__(self):
        return ("Engine(throttle={}, efficiency={})".format(
                   self.throttle, self.efficiency))
