class Countdown(object):
    """Represents a time period."""

    def __init__(self, time):
        # type: (float) -> ()
        self.time_left = time

    def __repr__(self):
        return "Countdown(time_left={})".format(self.time_left)
