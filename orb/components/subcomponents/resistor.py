class Resistor(object):
    def __init__(self, resistance):
        # type: (float) -> ()
        self.resistance = resistance
        """Resistance, in Ohm."""

    def __repr__(self):
        return ("Resistor(resistance={})".format(
                   self.resistance))

