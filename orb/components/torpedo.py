from orb.components.subcomponents.countdown import Countdown
from orb.core.config import Torpedo as Config
from orb.esper import Component


class Torpedo(Component):
    """Slow-moving, remote controlled missile."""

    @staticmethod
    def new():
        self = object.__new__(Torpedo)
        Torpedo.__init__(self)
        return self

    def __init__(self):
        # type: () -> ()
        self.countdown = Countdown(Config.time_to_live)
