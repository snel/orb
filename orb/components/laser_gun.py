from orb.components.subcomponents.projectile_weapon import ProjectileWeapon
from orb.core.config import LaserGun as Config
from orb.esper import Component


class LaserGun(Component, ProjectileWeapon):
    """Gun that fires lasers."""

    def __init__(self, resistance=Config.resistance):
        # type: (float) -> ()
        ProjectileWeapon.__init__(
            self, min_current=Config.min_current, cooldown=Config.cooldown,
            launch_speed=Config.launch_speed, resistance=resistance)

    __init__.__doc__ = ProjectileWeapon.__doc__

__doc__ = LaserGun.__doc__
