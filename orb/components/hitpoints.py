from orb.esper import Component


class Hitpoints(Component):
    """Abstract representation of hull integrity and crew health for an
    destructible object."""

    def __init__(self, hp=1):
        # type: (float) -> ()

        self.hp = hp  # type: float
        """Hit points as a fraction."""
