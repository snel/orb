"""
Components are data associated with an entity.

Components that can be added/removed to/from existing entities during the game
should implement the new() static method. This method returns an instance ready
to be initialised from data received over the network.
"""

from attitude_thrusters import AttitudeThrusters
from avatar import Avatar
from orb.components.subcomponents.projectile_weapon import ProjectileWeapon
from blocks_sensor import BlocksSensor
from body import Body
from bubble import Bubble
from circuit import Circuit
from colony import Colony
from colonisable import Colonisable
from construction import Construction
from explosion import Explosion
from fuel_tank import FuelTank
from generator import Generator
from hitpoints import Hitpoints
from kinetic_orbiter import KineticOrbiter
from laser import Laser
from laser_gun import LaserGun
from life_support import LifeSupport
from linear_thrusters import LinearThrusters
from observer import Observer
from planet import Planet
from projectile import Projectile
from respawn import Respawn
from self_destruct import SelfDestruct
from sensor import Sensor
from shields import Shields
from ship import Ship
from star import Star
from station import Station
from team import Team
from torpedo import Torpedo
from torpedo_gun import TorpedoGun
from transform import Transform
from warp_drive import WarpDrive
