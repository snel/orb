from orb.esper import Component, Entity
from pypanda3d.core import QuatD


class KineticOrbiter(Component):
    """Kinetic orbiting body.

    Forces are modeled by us, movement and collisions by Bullet.  Should be
    attached to an entity with ``Transform`` and ``Body`` components.

    Note: unused at this time.
    """

    def __init__(self, barycenter, axis, theta, period, semi_major, semi_minor):
        # type: (Entity, QuatD, float, float, float, float) -> ()
        """Constructor.

        Args:
            barycenter: Entity at the orbit barycenter that this entity will
                orbit around
            axis: Quaternion perpendicular to the orbital plane.
                The quaternion also defines the "initial location",
                i.e. when ``theta=0``.  The orbital plane coincides with the
                semi-major and semi-minor axes.
            theta: Orbit angle in radians, relative to ``axis``.
            period: Orbital period.
            semi_major: Magnitude of semi-major axis.
            semi_minor: Magnitude of semi-minor axis.
        """
        self.barycenter = barycenter  # type: Entity
        self.axis = axis  # type: QuatD
        self.theta = theta  # type: float
        self.period = period  # type: float
        self.semi_major = semi_major  # type: float
        self.semi_minor = semi_minor  # type: float
