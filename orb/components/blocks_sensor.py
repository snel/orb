from orb.esper import Component


class BlocksSensor(Component):
    """Obstacle that blocks sensors penetrating it.

    For example, ships can hide behind planets from other players.
    """

    @staticmethod
    def new():
        self = object.__new__(BlocksSensor)
        BlocksSensor.__init__(self)
        return self

