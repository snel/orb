from orb.components.subcomponents.countdown import Countdown
from orb.core.config import Team as Config
from orb.esper import Component


class Respawn(Component):
    """Entity waiting to be respawned."""

    @staticmethod
    def new():
        self = object.__new__(Respawn)
        Respawn.__init__(self)
        return self

    def __init__(self):
        # type: () -> ()
        self.countdown = Countdown(Config.respawn_time)
