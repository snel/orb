from orb.components.subcomponents.relation import Relation
from orb.esper import Component


class Construction(Component):
    """Construction project.

    This component is added both to the entity under construction and the entity
    building it until it is completed.
    """

    @staticmethod
    def new():
        self = object.__new__(Construction)
        Construction.__init__(self, -1, -1)
        return self

    def __init__(self, builder, target):
        # type: () -> ()
        self.progress = 0.
        """Fraction of action progress, where 0 is incomplete and 1 is 
        complete. """

        self.relation = Relation(builder, target)
        """Relation between the builder and the entity under construction."""
