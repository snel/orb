from orb.esper import Component, Entity


class Station(Component):
    """Space station."""

    @staticmethod
    def new():
        self = object.__new__(Station)
        Station.__init__(self, -1)
        return self

    def __init__(self, home):
        # type: (Entity) -> ()
        self.home = home
