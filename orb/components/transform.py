import logging

from orb.util import  math_util
from orb.esper import Component
from orb.util import type_casting
from pypanda3d.core import Vec3D, QuatD, QuatF, Vec3F

log = logging.getLogger(__file__)


class Transform(Component):
    """Transform relative to the global reference frame.

    Includes a translation vector (in global coordinates) and a rotation
    quaternion.
    """

    def __init__(self, x, q=QuatD.ident_quat()):
        # type: (Vec3D, QuatD) -> ()
        """
        Constructor.

        Args:
            x: Translation vector, in metres.
            q: Rotation quaternion.
        """
        if isinstance(x, Vec3F):
            x = type_casting.cast_vec3d(x)

        if isinstance(q, QuatF):
            q = type_casting.cast_quatd(q)

        self._x = x.__copy__()  # type: Vec3D
        self._q = q.__copy__()  # type: QuatD

    @property
    def x(self):
        # type: () -> Vec3D
        """Translation vector, in metres."""
        return self._x

    @x.setter
    def x(self, value):
        # type: (Vec3D) -> ()
        value = type_casting.cast_vec3d(value)
        self._x.assign(value)

    @property
    def q(self):
        # type: () -> QuatD
        """Rotation quaternion, in degrees."""
        return self._q

    @q.setter
    def q(self, value):
        # type: (QuatD) -> ()
        value = type_casting.cast_quatd(value)
        self._q.assign(value)
        if not self._q.normalize():
            log.warn("zero-length quaternion", exc_info=1)
            self._q = QuatD.ident_quat().__copy__()

    @staticmethod
    def get_relative_vector_inv(v, v_transform):
        """Returns ``v`` relative to the inverse of this transform, given that it
        was relative to ``v_transform``."""
        return math_util.inverse_transform(v_transform.q, v)

    def get_relative_vector(self, v, v_transform=None):
        # type: (Vec3D) -> Vec3D
        """Returns ``v`` relative to this transform, given that it was
        relative to ``v_transform``."""
        return math_util.transform(self.q, v, v_transform)
