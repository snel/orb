from orb.components.subcomponents.countdown import Countdown
from orb.esper import Component
from orb.core.config import Laser as Config


class Laser(Component):
    """A laser pulse fired from a weapon."""

    @staticmethod
    def new():
        self = object.__new__(Laser)
        Laser.__init__(self)
        return self

    def __init__(self):
        # type: () -> ()
        self.countdown = Countdown(Config.time_to_live)


__doc__ = Laser.__doc__
