from orb.esper import Component


class Planet(Component):
    """Planet or moon."""

    def __init__(self, name, radius, texture, is_moon):
        # type: (str, float, str, bool) -> ()
        self.name = name
        self.radius = radius
        self.texture = texture
        self.is_moon = is_moon
