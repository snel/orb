from orb.components.subcomponents.appliance import Appliance
from orb.components.subcomponents.ghost import Ghost
from orb.core.config import Sensor as Config
from orb.esper import Component
from pypanda3d.core import NodePath


class Sensor(Component):
    """Ship sensor/radar."""

    def __init__(self, np, resistance=Config.resistance):
        # type: (NodePath, float) -> ()
        """Constructor.

        Args:
            np: ``NodePath`` template.
            resistance: Resistance, in Ohm.
        """
        self.appliance = Appliance(
            resistance, Config.min_current, Config.max_current)
        self.ghost = Ghost(np)

        # This must never be sent over the network directly.
        self.entities = set()
