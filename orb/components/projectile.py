from orb.esper import Component, Entity


class Projectile(Component):
    """Projectile fired from a weapon."""

    def __init__(self, fired_by, damage):
        # type: (Entity) -> ()

        self.fired_by = fired_by
        """The ship entity that fired the projectile."""

        self.damage = damage
        """The potential damage, in hitpoints."""
