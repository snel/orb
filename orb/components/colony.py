from orb.components.subcomponents.relation import Relation
from orb.esper import Component


class Colony(Component):
    """Colony established by placing a station in orbit."""

    @staticmethod
    def new():
        self = object.__new__(Colony)
        Colony.__init__(self, -1, -1)
        return self

    def __init__(self, station, world):
        # type: () -> ()
        self.relation = Relation(station, world)
        """Relation between the station in orbit and the colonised planet."""
