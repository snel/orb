from orb.components.subcomponents.projectile_weapon import ProjectileWeapon
from orb.core.config import TorpedoGun as Config
from orb.esper import Component


class TorpedoGun(Component, ProjectileWeapon):
    """Gun that fires torpedoes."""

    def __init__(self, resistance=Config.resistance):
        # type: (float) -> ()
        ProjectileWeapon.__init__(
            self, min_current=Config.min_current, cooldown=Config.cooldown,
            launch_speed=Config.launch_speed, resistance=resistance)

    __init__.__doc__ = ProjectileWeapon.__doc__
