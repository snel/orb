from orb.components.subcomponents.countdown import Countdown
from orb.esper import Component


class SelfDestruct(Component):
    """Entity waiting to be self-destructed."""

    @staticmethod
    def new():
        self = object.__new__(SelfDestruct)
        SelfDestruct.__init__(self, 0)
        return self

    def __init__(self, delay):
        # type: (float) -> ()
        """Constructor.

        Args:
            delay: Time delay, in seconds.
        """
        self.countdown = Countdown(delay)
