from six import viewitems
from typing import Callable, Type, Sequence

from orb import components
from orb.components import (
    AttitudeThrusters, Avatar, BlocksSensor, Body, Circuit,
    Colonisable, Colony, Construction, Explosion, FuelTank,
    Generator, Hitpoints, KineticOrbiter, Laser, LaserGun, LifeSupport,
    LinearThrusters, Observer, Planet, Projectile, Sensor, Shields,
    Ship, Star, Station, Team, Torpedo, TorpedoGun, Transform,
    WarpDrive, SelfDestruct)
from orb.components.respawn import Respawn
from orb.esper import Entity, Component, EntityWorld


components_lower = {k.lower(): v
                    for k, v in viewitems(components.__dict__)
                    if isinstance(v, type)}  # Avoids confusion with modules.


class ComponentMappers(object):
    """Convenience class for accessing components from entities.

    Example:
        >>> cm = ComponentMappers(world)
        >>> t = cm.Transform(42)
        >>> x0 = t.x if t else None  # Gets the position of entity 42.

    If performance is not critical and the entity is fixed, consider using
    :class:`Components` instead.
    """

    def __init__(self, world):
        # type: (EntityWorld) -> ()

        def m(component_type):
            # type: (Type[Component]) -> Callable[[Entity], Component]
            return lambda entity: world.component_for_entity(
                entity, component_type)

        def has_all(entity, component_types):
            # type: (Entity, Sequence[Type[Component]]) -> bool
            """Returns ``True`` iff the entity has all the specified
            components."""
            return all(m(t)(entity) is not None for t in component_types)

        def has_any(entity, component_types):
            # type: (Entity, Sequence[Type[Component]]) -> bool
            """Returns ``True`` iff the entity has at least one of the specified
            components."""
            return any(m(t)(entity) is not None for t in component_types)

        def is_only(entity, component_types, only_component_type):
            # type: (Entity, Sequence[Type[Component]], Type[Component]) -> bool
            """Returns ``True`` iff the only component the entity has from the
            set is the ``only_component_type``."""
            return all(m(t)(entity) is only_component_type
                       for t in component_types)

        def component_type_by_name(component_name, case_sensitive=True):
            """Returns a component type for the component type specified by
            name (str)."""
            if case_sensitive:
                return components.__dict__.get(component_name)
            else:
                return components_lower.get(component_name.lower())

        def component_by_name(component_name, case_sensitive=True):
            """Returns a component mapper for the component type specified by
            name (str)."""
            return m(component_type_by_name(component_name, case_sensitive))

        self.entity_world = world
        self.m = m
        self.has_all = has_all
        self.has_any = has_any
        self.is_only = is_only
        self.component_type_by_name = component_type_by_name
        self.component_by_name = component_by_name

        # We add them this way to enable type hints in PyCharm.
        self.AttitudeThrusters = m(AttitudeThrusters)  # type: Callable[[Entity], AttitudeThrusters]
        self.Avatar = m(Avatar)  # type: Callable[[Entity], Avatar]
        self.BlocksSensor = m(BlocksSensor)  # type: Callable[[Entity], BlocksSensor]
        self.Body = m(Body)  # type: Callable[[Entity], Body]
        self.Circuit = m(Circuit)  # type: Callable[[Entity], Circuit]
        self.Colonisable = m(Colonisable)  # type: Callable[[Entity], Colonisable]
        self.Colony = m(Colony)  # type: Callable[[Entity], Colony]
        self.Construction = m(Construction)  # type: Callable[[Entity], Construction]
        self.Explosion = m(Explosion)  # type: Callable[[Entity], Explosion]
        self.FuelTank = m(FuelTank)  # type: Callable[[Entity], FuelTank]
        self.Generator = m(Generator)  # type: Callable[[Entity], Generator]
        self.Hitpoints = m(Hitpoints)  # type: Callable[[Entity], Hitpoints]
        self.KineticOrbiter = m(KineticOrbiter)  # type: Callable[[Entity], KineticOrbiter]
        self.Laser = m(Laser)  # type: Callable[[Entity], Laser]
        self.LaserGun = m(LaserGun)  # type: Callable[[Entity], LaserGun]
        self.LifeSupport = m(LifeSupport)  # type: Callable[[Entity], LifeSupport]
        self.LinearThrusters = m(LinearThrusters)  # type: Callable[[Entity], LinearThrusters]
        self.Observer = m(Observer)  # type: Callable[[Entity], Observer]
        self.Planet = m(Planet)  # type: Callable[[Entity], Planet]
        self.Projectile = m(Projectile)  # type: Callable[[Entity], Projectile]
        self.Respawn = m(Respawn)  # type: Callable[[Entity], Respawn]
        self.SelfDestruct = m(SelfDestruct)  # type: Callable[[Entity], SelfDestruct]
        self.Sensor = m(Sensor)  # type: Callable[[Entity], Sensor]
        self.Shields = m(Shields)  # type: Callable[[Entity], Shields]
        self.Ship = m(Ship)  # type: Callable[[Entity], Ship]
        self.Star = m(Star)  # type: Callable[[Entity], Star]
        self.Station = m(Station)  # type: Callable[[Entity], Station]
        self.Team = m(Team)  # type: Callable[[Entity], Team]
        self.Torpedo = m(Torpedo)  # type: Callable[[Entity], Torpedo]
        self.TorpedoGun = m(TorpedoGun)  # type: Callable[[Entity], TorpedoGun]
        self.Transform = m(Transform)  # type: Callable[[Entity], Transform]
        self.WarpDrive = m(WarpDrive)  # type: Callable[[Entity], WarpDrive]
        self.Construction = m(Construction)  # type: Callable[[Entity], Construction]
