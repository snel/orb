from orb.esper import Component
from orb.core.config import Generator as Config


class Generator(Component):
    """An electric generator."""

    def __init__(self, voltage):
        # type: (float) -> ()

        self.voltage = voltage
        """Voltage generated, in Volts."""

        self.efficiency = Config.efficiency
        """Ratio of energy conversion efficiency, between 0 and 1.  For
        chemical energy `E_c` Joules the electrical energy generated will be:
            `E = E_c*efficiency = V*Q = V*I*t`.
        """

        self.max_current = Config.max_current
        """Maximum current the generator can produce."""
