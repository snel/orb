from orb.components.subcomponents.appliance import Appliance
from orb.components.subcomponents.capacitor import Capacitor
from orb.core.config import Shields as Config
from orb.esper import Component


class Shields(Component):
    """Shields (protective force fields)."""

    def __init__(self, effectiveness, resistance=Config.resistance):
        # type: (float, float) -> ()
        """Constructor.

        Args:
            effectiveness: Hitpoints absorbed per Coulomb charge.
            resistance: Resistance, in Ohm.
        """
        self.appliance = Appliance(
            resistance, Config.min_current, Config.max_current)
        self.capacitor = Capacitor(
            Config.capacitance, 0, 0)

        self.effectiveness = effectiveness
        """Hitpoints absorbed per Coulomb charge."""
