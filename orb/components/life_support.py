from orb.components.subcomponents.appliance import Appliance
from orb.core.config import LifeSupport as Config
from orb.esper import Component


class LifeSupport(Component):
    """Life support system. Requires power or the avatar will suffer damage."""

    def __init__(self, resistance=Config.resistance):
        # type: (float, float) -> ()
        """Constructor.

        Args:
            resistance: Resistance, in Ohm.
        """
        self.appliance = Appliance(
            resistance, Config.min_current, Config.max_current)
