from orb.esper import Component


class Colonisable(Component):
    """Colonisable planet or moon.  This means a station can be built in
    orbit."""

    @staticmethod
    def new():
        self = object.__new__(Colonisable)
        Colonisable.__init__(self)
        return self
