from orb.components.subcomponents.appliance import Appliance
from orb.components.subcomponents.countdown import Countdown
from orb.components.subcomponents.engine import Engine
from orb.core.config import WarpDrive as Config
from orb.esper import Component
from pypanda3d.core import Vec3


class WarpDrive(Component):
    """Warp drive."""

    def __init__(self, speed=Config.default_warp_speed,
                 resistance=Config.resistance):
        # type: (float, float) -> ()
        """Constructor.

        Args:
            speed: Linear speed in meters per second, at full throttle.
            resistance: Resistance, in Ohm.
        """
        self.appliance = Appliance(
            resistance, Config.min_current, Config.max_current)
        self.engine = Engine(Config.efficiency)
        self.countdown = Countdown(0)  # Config.cooldown

        self.speed = speed

        self.entry_velocity = Vec3(0, 0, 0)
        self.previous_speed = 0.
