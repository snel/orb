from orb.esper import Component


class Team(Component):
    """Player team."""

    @staticmethod
    def new():
        self = object.__new__(Team)
        Team.__init__(self, -1)
        return self

    def __init__(self, id):
        # type: (int) -> ()
        """Constructor.

        Arguments:
            id (int): Team id number.
        """
        self.id = id
