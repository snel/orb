from orb.esper import Component


class Avatar(Component):
    """A controllable avatar (e.g. ship, station or torpedo)."""

    @staticmethod
    def new():
        self = object.__new__(Avatar)
        Avatar.__init__(self)
        return self

