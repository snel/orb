from orb.esper import Component


class Circuit(Component):
    """Electrical circuit."""

    @staticmethod
    def new():
        self = object.__new__(Circuit)
        Circuit.__init__(self)
        return self
