from orb.components.subcomponents.appliance import Appliance
from orb.components.subcomponents.engine import Engine
from orb.esper import Component
from orb.core.config import AttitudeThrusters as Config
from pypanda3d.core import Vec3D


class AttitudeThrusters(Component):
    """A set of attitude thrusters."""

    def __init__(self, torque, resistance=Config.resistance):
        # type: (float, float) -> ()
        """Constructor.

        Torque and energy are related by ``E = torque*theta`` where `E` is energy
        and `theta` is the angle moved (in radians). So:
            `E/t = torque*theta/t  =>  torque = E / angular_acceleration'
        where `angular_acceleration = theta/t**2`.

        See:
            http://hyperphysics.phy-astr.gsu.edu/hbase/rke.html

        Args:
            torque: Torque magnitude in Newton meters, at full throttle.
            resistance: Resistance, in Ohm.
        """
        self.appliance = Appliance(
            resistance, Config.min_current, Config.max_current)
        self.engine = Engine(Config.efficiency)

        self.torque = torque
        """Torque magnitude at full throttle."""

        self.direction = Vec3D.zero().__copy__()
        """Last set turning direction."""

        self.time = 0
        """Time remaining to apply last input."""

        self.stabilise = False
        """Set to ``True`` to enter a special mode where angular velocity is 
        reduced to 0. """
