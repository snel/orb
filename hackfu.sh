mkdir ../orb-hackfu

mkdir deleteme
mv ../orb-hackfu/* deleteme
rm -rf deleteme

cp -r ./* ../orb-hackfu

rm -r ../orb-hackfu/logs
rm -r ../orb-hackfu/saved_games
rm -r ../orb-hackfu/doc-build
rm -r ../orb-hackfu/doc-source
rm ../orb-hackfu/*.pyc
rm ../orb-hackfu/**/*.pyc
rm ../orb-hackfu/TODO.txt
rm ../orb-hackfu/README.md
rm ../orb-hackfu/Makefile
rm ../orb-hackfu/hackfu.sh
rm ../orb-hackfu/mkdocs.sh

cp doc-build/latex/Orb.pdf ../orb-hackfu/orb_manual.pdf
cp -r doc-build/html ../orb-hackfu/html_docs

