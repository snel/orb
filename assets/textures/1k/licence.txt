1k planetary textures:
From [Panda3d samples](https://github.com/panda3d/panda3d/tree/master/samples/solar-system/models).
Provided under the [BSD licence](https://github.com/panda3d/panda3d/blob/master/LICENSE).
